//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartDispatching.DB.DataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class FormanProblem
    {
        public int Id { get; set; }
        public Nullable<int> ProblemId { get; set; }
        public Nullable<int> FormanId { get; set; }
    
        public virtual Forman Forman { get; set; }
        public virtual Problem Problem { get; set; }
    }
}
