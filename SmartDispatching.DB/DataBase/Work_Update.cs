//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartDispatching.DB.DataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Work_Update
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Work_Update()
        {
            this.OrderWorkUpdates = new HashSet<OrderWorkUpdate>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> StatusId { get; set; }
    
        public virtual OrderStatu OrderStatu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderWorkUpdate> OrderWorkUpdates { get; set; }
    }
}
