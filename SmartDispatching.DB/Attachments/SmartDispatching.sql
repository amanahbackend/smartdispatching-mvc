USE [master]
GO
/****** Object:  Database [aspnet-SmartDispatching-20170404095844]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE DATABASE [aspnet-SmartDispatching-20170404095844]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'aspnet-SmartDispatching-20170404095844.mdf', FILENAME = N'C:\Users\ahmon\Documents\Visual Studio 2015\Projects\ElGhanim\smartdispatching\SmartDispatching\App_Data\aspnet-SmartDispatching-20170404095844.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'aspnet-SmartDispatching-20170404095844_log.ldf', FILENAME = N'C:\Users\ahmon\Documents\Visual Studio 2015\Projects\ElGhanim\smartdispatching\SmartDispatching\App_Data\aspnet-SmartDispatching-20170404095844_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [aspnet-SmartDispatching-20170404095844].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET ARITHABORT OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET  ENABLE_BROKER 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET  MULTI_USER 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET DB_CHAINING OFF 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET DELAYED_DURABILITY = DISABLED 
GO
USE [aspnet-SmartDispatching-20170404095844]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Address]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PACI] [nvarchar](50) NULL,
	[AreaId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[StreetId] [int] NULL,
	[GovId] [int] NULL,
	[BlockId] [int] NULL,
	[Functional_Location] [nvarchar](100) NOT NULL,
	[House_Kasima] [nvarchar](50) NULL,
	[Floor] [nvarchar](50) NULL,
	[AppartmentNo] [nvarchar](50) NULL,
	[AddressNote] [nvarchar](100) NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Area]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Area](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[RoleId] [int] NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssignedOrders]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignedOrders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[TechnicianId] [int] NULL,
	[FormanId] [int] NULL,
	[DispatcherId] [int] NULL,
 CONSTRAINT [PK_AssignedOrders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Block]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Block](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Block] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cancellation_Reasons]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cancellation_Reasons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Cancellation_Reasons] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompanyCode]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyCode](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CompanyCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contract]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contract](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_Contract] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContractType]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ContractType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[PhoneOne] [nvarchar](50) NOT NULL,
	[PhoneTwo] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Division]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Division](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Forman]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forman](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DispatcherId] [nvarchar](128) NOT NULL,
	[SuperDispatcherId] [nvarchar](128) NOT NULL,
	[PhoneNo] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Forman] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormanArea]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormanArea](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[FormanId] [int] NULL,
 CONSTRAINT [PK_FormanArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormanProblem]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FormanProblem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProblemId] [int] NULL,
	[FormanId] [int] NULL,
 CONSTRAINT [PK_FormanProblem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Governorate]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Governorate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Governorate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[CompanyCodeId] [int] NOT NULL,
	[DivisionId] [int] NOT NULL,
	[PriorityId] [int] NULL,
	[StatusId] [int] NOT NULL,
	[ProblemId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[Notes_ICAgent] [nvarchar](150) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderPriority]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderPriority](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Problem]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Problem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Problem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Street_Jaddah]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Street_Jaddah](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Street_Jaddah] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SuperDispatcher]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuperDispatcher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SuperId] [nvarchar](128) NULL,
	[DispatcherId] [nvarchar](128) NULL,
 CONSTRAINT [PK_SuperDispatcher] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Technician]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Technician](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[FormanId] [int] NOT NULL,
	[DispatcherId] [int] NOT NULL,
	[SuperDispatcherId] [int] NOT NULL,
	[AvailabilityId] [int] NULL,
 CONSTRAINT [PK_Technician] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TechnicianArea]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TechnicianArea](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NULL,
	[TechnicianId] [int] NULL,
 CONSTRAINT [PK_TechnicianArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TechnicianProblem]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TechnicianProblem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProblemId] [int] NULL,
	[TechnicianId] [int] NULL,
 CONSTRAINT [PK_TechnicianProblem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TV_Availability]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TV_Availability](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_TV_Availability] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Work_Update]    Script Date: 4/9/2017 7:57:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Work_Update](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Work_Update] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201704042011381_init', N'SmartDispatching.Migrations.Configuration', 0x1F8B0800000000000400DD5C5B6FE3B6127E3FC0F90F829E7A0E522B97B38B6D60EF2275929EA09B0BD6D9A26F0B5AA21D6225CA95A83441D15FD687FEA4FE850E25EAC68B2EB6623B4581622D92DF0C871FC9E17098BFFEF873FCE129F0AD471CC524A413FB6874685B98BAA147E87262276CF1ED3BFBC3FB7FFF6B7CE1054FD64F79BD135E0F5AD278623F30B63A759CD87DC0018A470171A3300E176CE4868183BCD0393E3CFCCE393A723040D8806559E34F096524C0E90FF8390DA98B572C41FE75E8613F16DFA16496A25A3728C0F10AB97862CF0214B17302BF98FB005A8EB226B675E61304EACCB0BFB02D4469C81003654F3FC778C6A2902E67D08420FFFE7985A1DE02F931169D382DAB77EDCFE131EF8F5336CCA1DC246661D013F0E84418C8919BAF6566BB302098F0024CCD9E79AF53334EEC2B0FA79F3E853E184016783AF5235E79625F1722CEE2D50D66A3BCE12883BC8C00EED730FA3AAA221E589DDB1D14843A1E1DF2FF0EAC69E2B324C2138A131621FFC0BA4BE63E717FC4CFF7E1574C272747F3C5C9BB376F9177F2F67FF8E44DB5A7D057A857FB009FEEA2708523D00D2F8AFEDB96536FE7C80D8B6695369955804BC03ADBBA464F1F315DB2079835C7EF6CEB923C612FFF22C8F59912984AD0884509FCBC497C1FCD7D5C943B8D32F9FF1BA41EBF793B88D41BF44896E9D04BF261E24430AF3E613F2D8D1FC82A9B5EB5F1FE22AA5D4661C07FD7F995957E998549E4F2CE84C62AF7285A6256D76EEC94E4ED44690E353CAD73D4FDA736D754A5B7B62AEFD03A332117B1EDD990EBFBB2723B33EE6CB582C14BA9C52DD24438C38E3592200E2CB96249A2A3AE24A2D0B97FF29A781120E20FB0287690024EC98244012E7AF97D081444B4B7CE77288E614DF0FE8FE28706D5E19F03A83EC36E120155670C05AB179776F710527C9304733E03B6276BB0A1B9FF35BC442E0BA30BCA5B6D8CF73174BF8609BBA0DE3962F833737340FEF39E04DD010651E7CC75711C5F0299B1370DC1E7CE01AF283B39EE0DC757A95D3B25531F9140EF9548EBE997BC6AE999E86B28DE89A19ACE436952F563B824B49BAA7955B3AA598D565545B5BEAA72B06E9A8A9A6645D30AAD7A66B506F3F9D2111ADEE94B61F7DFEBDB6CF336AD051533CE6085C43F608A2358C6BC3BC4188E6839025DD68D5D380BE9F071A12FBE37A5927E427E32B4A8B56643BA080C3F1B52D8FD9F0DA99AF0F99178DC2BE97014CA2B037CA7FAFA5356FB9C9334DBF674A87573DBC2B7B30698A6CB591C872E496781260826421875FDC187B3DAE319596FE49808740C884EF896075FA06FB64CAA5B7A8E7DCCB075E66641C2298A5DE4A966840E793D14CB77548D62656CA4AEDC7F1599C0741CF146881F826298A98432755A10EA9215F25BAD24B5ECB885F1BE1732E49273BCC2940B6CB54417E1FA500857A090230D4A9B85C64E8571CD443478ADA6316F7361CB715722145BE1648BEF6CE0A5F0DF5E8498CD16DB02399B4DD2450163586F1704156795AE04900F2EFB4650E9C46420A870A9B642D0BAC57640D0BA495E1D41B3236AD7F197CEABFB46CFFA4179FBDB7AA3B976C0CD9A3DF68C9A99EF096D18B4C0914ACFF3392FC44F4C7338033DC5F92C16AEAE4C110E3EC3AC1EB229FD5DAD1FEA3483C8246A022C89D6022A2E0415206542F5502E8FE5356A27BC881EB079DCAD1156ACFD126C85032A76F562B452D17C7D2A93B3D3E9A3E859C10685E49D0E0B151C0D21E4C5ABDEF10E4631C56555C374F185FB78C3958E89C16830508BE76A3052DE99C1AD9453B3DD4A3A87AC8F4BB6919524F7C960A5BC33835B4970B4DD481AA7A0875BB09189EA5BF840932D8F7414BB4D513676B2A429F161EC18B2ABC6D768B5227459C9B6125FAC59966A35FD76D63FFD28C8301C37D6642115DA16925818A125964A4134687A49A2989D2386E688C779A65EA054D3EEAD86E53F1759DD3ED541CCF781BC36FFB7B859355CE3D7365CD523114097D0CD80BB35692C5D43027D738BA7C0211F459AF0FD34F493809ABD2C73EBEC12AFDA3EFBA2228C1D497FC58B524CA6F8BA75FB771A1D75660C39528527B3FE6899214C36CFFDD0AAD54DBEA919250F5555514CE1AB9D8D9EC9A5E93F62B2CBD87FC05A115E6686893C952A80F8D413A392EAA08055CABAA3D6B351AA98F592EE8852CA4915522AEAA16535B1A4A664B5602D3C8345F535BA4B505349AAE86A6977644D5249155A53BC06B64667B9AC3BAA26EFA40AAC29EE8E5D26A1C82BE91EEF61C683CC669B5876E0DD6C173360BCCCB238CC2658B9D7AF02553EF7C41237F70A98F8BE9794329EFA36A35416ECD88C52060CF31A54BB16AF2F418D77F966CCDA5D776D996FBAEB37E3F523EE8BD24339F9C9550AE9C509503AE98DC5A9ABFDB18D720CCBAAD8566E4620D573CC7030E21546B35FFCA94F305FD0F30AD78892058E5996DF611F1F1E1D4B4F75F6E7D98C13C79EAF39B59ADECED4C76C0BA95AF41145EE038AD4C4890D9E9694A04A4CFA8A7AF86962FF96B63A4DC31BFC5FE9E703EB2AFE4CC92F0914DC4709B67E5713418749B56F3E73EDE9C388EE56BDFAF94BD6F4C0BA8D60C69C5A87922DD719E1FA73895EDA644D37D066ED4714AF7742D5DE256851A509B1FE33843961833C41C8B5FC26404FFFE9AB9AF699C146889AA70443E10D6242D3538175B08CCF043CF8C9D26702FD3AAB7F36B08E6AC6270384F607931F0C745F86F2963BDC6A3407A36D2C49A99D5B13AE37CABEDCF5DEA4E4656F34D1D5DCEB1E701BE457AFC18C57969A3CD8EEA8C93C1E0C7B97D47EF174E37DC9302E733F769B58BCCD5CE2865BA27F540AF11E24BD699278769F28BC6DAE9982B97B9E6DD92F1D78CFC82652BB769FF4BB6DB299C2BC7B4EB65EA9BD7BC6B55DED9F3B665AE72D74E789BA6ACE91E15246170B6E4BC4CD02E770C29F874082CCA3CCDE4FEA33BF9AB2565B049655CC42CD2967B26065E22872951ACD62FBF5556CF88D9D15759AC51A12359B648BF5BF51B6A8D32CDB90FEB88B14626D02A22EADBB651D6BCA897A4D29C3B59EB464A8B7F9AC8D37ECAF29437810A3D4668FE18EF8F524040F629221A74E8F0460F5BA17F6CECA5F6084FD3B26CB1282FF3D468ADDDAAE59D4B9A28B30DFBC258DF22A5284E61A33E4C1967A1631B2402E83621E634E1F80A7713B7ED331C7DE15BD4DD82A61D0651CCCFD5AC08B3B014DF2D32CE7BACEE3DB55FAB74C86E802A849786CFE967E9F10DF2BF4BED4C4840C10DCBB10115D3E968C477697CF05D24D483B0209F3154ED13D0E563E80C5B774861EF13ABA01FD3EE225729FCB08A009A47D20EA661F9F13B48C50100B8CB23DFC040E7BC1D3FBBF01000819CC88540000, N'6.1.3-40302')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'1', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'5', N'Dispatcher')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2', N'IT')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'3', N'Manager')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'4', N'Supervisor')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'011d9bed-5087-400a-98d0-407e606d96f9', N'1')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'50ed756d-2b8e-4fa3-8304-4a9caadc2319', N'2')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ee26291f-3d5f-4fd1-b632-63ee2e4edf22', N'2')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1037baf4-f109-4909-8cee-05bbd6bb0ceb', N'3')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9a6e3027-8048-4629-99fd-bbf987de636f', N'4')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b109d203-91f9-4283-b51e-47b347201c15', N'4')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'34af51be-8710-48bf-92e2-6be1b3ebc8bc', N'5')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'36c3a0d3-810e-4102-903f-737e72bc57f2', N'5')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'011d9bed-5087-400a-98d0-407e606d96f9', N'Admin@Admin.com', 0, N'AEnFYk/STGTNECMhCLhVleyrSELkPd32YvO1MW4QMjeITV41mQTZCC8zLKs9TQ+owQ==', N'67685fa2-0d65-45f3-aa6a-71f768606a1d', NULL, 0, 0, NULL, 1, 0, N'Admin', 1)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'1037baf4-f109-4909-8cee-05bbd6bb0ceb', N'manager@manager.com', 0, N'ACYbknuR/eldLSMEzyA3SycbBRBMuFkCFiL8Omi0T3id+2sDdWsa10vhsCZjVhvU4g==', N'24d3b2a6-e452-46d6-bcc1-b3c47f75a980', NULL, 0, 0, NULL, 1, 0, N'Manager', 3)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'34af51be-8710-48bf-92e2-6be1b3ebc8bc', N'DispatcherOne@DispatcherOne.com', 0, N'ALO0L3dcv9qscPrMrTdBplrzTymfZMy2CW1ZyfL7xaRwDXSSJXMth57CXsHBDZPnvw==', N'1aea9067-5564-48f8-b60b-4141edec0994', NULL, 0, 0, NULL, 1, 0, N'DispatcherOne', 5)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'36c3a0d3-810e-4102-903f-737e72bc57f2', N'DispatcherTwo@DispatcherTwo.com', 0, N'AJDG77I1vUfYbFArcjYKeYnBH1bOdtKZwDLoSFQSNrE+qt1xQjy1N13DYOitsXaX/w==', N'e0a5ce5f-4763-43fd-9b7f-33491e2da2ea', NULL, 0, 0, NULL, 1, 0, N'DispatcherTwo', 5)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'50ed756d-2b8e-4fa3-8304-4a9caadc2319', N'testit@testit.com', 0, N'ANxOqGBfXLDWXlGn28tFmdFzsUrguKzoEzPCBlSLb8+zUGNzesFYaLUNDiuJkSi3pw==', N'822307e5-1616-4328-88b5-1964c7872ecd', NULL, 0, 0, NULL, 1, 0, N'testit', 2)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'9a6e3027-8048-4629-99fd-bbf987de636f', N'supervisorTwo@supervisorTwo.com', 0, N'ACBWS4s5ezKSTrQJr+jNINoV0JwD4y2V/S0FBxgVSbdOtPJPFUSD9fNklXpS+fDyqg==', N'cdab4ec7-e3da-4c6b-b48f-74a8e1536330', NULL, 0, 0, NULL, 1, 0, N'supervisorTwo', 4)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'b109d203-91f9-4283-b51e-47b347201c15', N'supervisorOne@supervisorOne.com', 0, N'AD9VQ6SJFlza2HJJuEkRHgWmiJs46MSzVI/JrxeLSCG+bfPBPF8Od8xG/2/J3uH6wg==', N'f81f603d-3513-4e50-b1a0-9b865277efc3', NULL, 0, 0, NULL, 1, 0, N'supervisorOne', 4)
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [RoleId]) VALUES (N'ee26291f-3d5f-4fd1-b632-63ee2e4edf22', N'it@it.com', 0, N'AI3Zx5ludu3MP5y6XBJqwNr2BRJi/LmamnP+ZDFteBQarKJgCX4a+71O/vc5VBKm4g==', N'4c6f97d6-12f9-4b1a-9cf9-3e5f01929c0c', NULL, 0, 0, NULL, 1, 0, N'itest', 2)
SET IDENTITY_INSERT [dbo].[Cancellation_Reasons] ON 

INSERT [dbo].[Cancellation_Reasons] ([Id], [Name]) VALUES (1, N'Customer not available')
INSERT [dbo].[Cancellation_Reasons] ([Id], [Name]) VALUES (2, N'Customer request')
INSERT [dbo].[Cancellation_Reasons] ([Id], [Name]) VALUES (3, N'Customer refuse to pay')
SET IDENTITY_INSERT [dbo].[Cancellation_Reasons] OFF
SET IDENTITY_INSERT [dbo].[CompanyCode] ON 

INSERT [dbo].[CompanyCode] ([Id], [Name]) VALUES (1, N'EN01')
INSERT [dbo].[CompanyCode] ([Id], [Name]) VALUES (2, N'EN02')
INSERT [dbo].[CompanyCode] ([Id], [Name]) VALUES (3, N'EN03')
SET IDENTITY_INSERT [dbo].[CompanyCode] OFF
SET IDENTITY_INSERT [dbo].[ContractType] ON 

INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (1, N'Pearl Contract', N'TYP9')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (2, N'Bronze Contract', N'TYP8')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (3, N'Platinum Contract', N'TYP7')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (4, N'Silver Contract', N'TYP6')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (5, N'Basic Contract', N'TYP5')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (6, N'Diamond Contract', N'TYP4')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (7, N'Golden Contract', N'TYP3')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (8, N'Extend Warranty Cont', N'TYP2')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (9, N'Warranty Contract', N'TYP1')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (10, N'Elevator Contracts', N'TY11')
INSERT [dbo].[ContractType] ([Id], [Name], [Code]) VALUES (11, N'Special Contracts', N'TY10')
SET IDENTITY_INSERT [dbo].[ContractType] OFF
SET IDENTITY_INSERT [dbo].[Division] ON 

INSERT [dbo].[Division] ([Id], [Name]) VALUES (1, N'Non-Residential')
INSERT [dbo].[Division] ([Id], [Name]) VALUES (2, N'Residential')
SET IDENTITY_INSERT [dbo].[Division] OFF
SET IDENTITY_INSERT [dbo].[Forman] ON 

INSERT [dbo].[Forman] ([Id], [Name], [DispatcherId], [SuperDispatcherId], [PhoneNo]) VALUES (1, N'Forman1', N'34af51be-8710-48bf-92e2-6be1b3ebc8bc', N'b109d203-91f9-4283-b51e-47b347201c15', N'00 965 25252525')
SET IDENTITY_INSERT [dbo].[Forman] OFF
SET IDENTITY_INSERT [dbo].[OrderPriority] ON 

INSERT [dbo].[OrderPriority] ([Id], [Name]) VALUES (1, N'Normal')
INSERT [dbo].[OrderPriority] ([Id], [Name]) VALUES (2, N'Urgent')
INSERT [dbo].[OrderPriority] ([Id], [Name]) VALUES (3, N'Emergency')
SET IDENTITY_INSERT [dbo].[OrderPriority] OFF
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (1, N'Dispatched')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (2, N'Open')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (3, N'In progress')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (4, N'Complete')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (5, N'Cancelled')
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
SET IDENTITY_INSERT [dbo].[OrderType] ON 

INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (1, N'Contract')
INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (2, N'Enhancement')
INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (3, N'Preventive')
INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (4, N'MC potential')
INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (5, N'Cash Compressor')
INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (6, N'Cash')
INSERT [dbo].[OrderType] ([Id], [Name]) VALUES (7, N'SRAC extended warranty')
SET IDENTITY_INSERT [dbo].[OrderType] OFF
SET IDENTITY_INSERT [dbo].[Problem] ON 

INSERT [dbo].[Problem] ([Id], [Name]) VALUES (1, N'CAC residential')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (2, N'CAC commercial')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (3, N'SRAC installation')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (4, N'Chillers')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (5, N'CHS')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (6, N'Technology')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (7, N'Plumbing')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (8, N'Electrical')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (9, N'Fire fighting')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (10, N'Non Residential Std')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (11, N'Security Audio visual')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (12, N'Stationed operator')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (13, N'Subcontractor')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (14, N'Video data network')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (15, N'Water cooler')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (16, N'Cleaning')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (17, N'Forklift')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (18, N'Residential Swimming pool')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (19, N'Residential plumbing services')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (20, N'Residential electrical services')
INSERT [dbo].[Problem] ([Id], [Name]) VALUES (21, N'Service for government contracts')
SET IDENTITY_INSERT [dbo].[Problem] OFF
SET IDENTITY_INSERT [dbo].[SuperDispatcher] ON 

INSERT [dbo].[SuperDispatcher] ([Id], [SuperId], [DispatcherId]) VALUES (1, N'b109d203-91f9-4283-b51e-47b347201c15', N'34af51be-8710-48bf-92e2-6be1b3ebc8bc')
INSERT [dbo].[SuperDispatcher] ([Id], [SuperId], [DispatcherId]) VALUES (2, N'b109d203-91f9-4283-b51e-47b347201c15', N'36c3a0d3-810e-4102-903f-737e72bc57f2')
SET IDENTITY_INSERT [dbo].[SuperDispatcher] OFF
SET IDENTITY_INSERT [dbo].[TV_Availability] ON 

INSERT [dbo].[TV_Availability] ([Id], [Name]) VALUES (1, N'Available')
INSERT [dbo].[TV_Availability] ([Id], [Name]) VALUES (2, N'Not Available')
SET IDENTITY_INSERT [dbo].[TV_Availability] OFF
SET IDENTITY_INSERT [dbo].[Work_Update] ON 

INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (1, N'Pending For Material')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (2, N'Customer Not Available')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (3, N'Address Not Clear')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (4, N'No Work Done')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (5, N'Black Listed Customer')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (6, N'No Need')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (7, N'Duplicate')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (8, N'Progress - Quotation')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (9, N'Progress  -Gas Leak Problem')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (10, N'Progress - Water Leak')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (11, N'Progress - Motor Problem')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (12, N'Progress - Belt Problem')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (13, N'Progress - Elevator Program')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (14, N'Progress – Elevator Travel cabin')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (15, N'Change Compressor')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (16, N'Motor Fixed')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (17, N'Completed - Motor Replace')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (18, N'Completed - Belt Replaced')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (19, N'Completed - Unit Washed/Cleaned')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (20, N'Completed – Gas Leak Repaired')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (21, N'Completed - Compressor Changed')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (22, N'Completed - Water Leak Re')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (23, N'Cancelled - E.C.S')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (24, N'Cancelled - Customer can’t pay')
INSERT [dbo].[Work_Update] ([Id], [Name]) VALUES (25, N'Cancelled - Customer has not paid')
SET IDENTITY_INSERT [dbo].[Work_Update] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 4/9/2017 7:57:37 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Area]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Block] FOREIGN KEY([BlockId])
REFERENCES [dbo].[Block] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Block]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Customer]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Governorate] FOREIGN KEY([GovId])
REFERENCES [dbo].[Governorate] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Governorate]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Street_Jaddah] FOREIGN KEY([StreetId])
REFERENCES [dbo].[Street_Jaddah] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Street_Jaddah]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AssignedOrders]  WITH CHECK ADD  CONSTRAINT [FK_AssignedOrders_Forman] FOREIGN KEY([FormanId])
REFERENCES [dbo].[Forman] ([Id])
GO
ALTER TABLE [dbo].[AssignedOrders] CHECK CONSTRAINT [FK_AssignedOrders_Forman]
GO
ALTER TABLE [dbo].[AssignedOrders]  WITH CHECK ADD  CONSTRAINT [FK_AssignedOrders_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[AssignedOrders] CHECK CONSTRAINT [FK_AssignedOrders_Order]
GO
ALTER TABLE [dbo].[AssignedOrders]  WITH CHECK ADD  CONSTRAINT [FK_AssignedOrders_Technician] FOREIGN KEY([TechnicianId])
REFERENCES [dbo].[Technician] ([Id])
GO
ALTER TABLE [dbo].[AssignedOrders] CHECK CONSTRAINT [FK_AssignedOrders_Technician]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_ContractType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[ContractType] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_ContractType]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Customer]
GO
ALTER TABLE [dbo].[FormanArea]  WITH CHECK ADD  CONSTRAINT [FK_FormanArea_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[FormanArea] CHECK CONSTRAINT [FK_FormanArea_Area]
GO
ALTER TABLE [dbo].[FormanArea]  WITH CHECK ADD  CONSTRAINT [FK_FormanArea_Forman] FOREIGN KEY([FormanId])
REFERENCES [dbo].[Forman] ([Id])
GO
ALTER TABLE [dbo].[FormanArea] CHECK CONSTRAINT [FK_FormanArea_Forman]
GO
ALTER TABLE [dbo].[FormanProblem]  WITH CHECK ADD  CONSTRAINT [FK_FormanProblem_Forman] FOREIGN KEY([FormanId])
REFERENCES [dbo].[Forman] ([Id])
GO
ALTER TABLE [dbo].[FormanProblem] CHECK CONSTRAINT [FK_FormanProblem_Forman]
GO
ALTER TABLE [dbo].[FormanProblem]  WITH CHECK ADD  CONSTRAINT [FK_FormanProblem_Problem] FOREIGN KEY([ProblemId])
REFERENCES [dbo].[Problem] ([Id])
GO
ALTER TABLE [dbo].[FormanProblem] CHECK CONSTRAINT [FK_FormanProblem_Problem]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_CompanyCode] FOREIGN KEY([CompanyCodeId])
REFERENCES [dbo].[CompanyCode] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_CompanyCode]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Division] FOREIGN KEY([DivisionId])
REFERENCES [dbo].[Division] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Division]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderPriority] FOREIGN KEY([PriorityId])
REFERENCES [dbo].[OrderPriority] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderPriority]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderStatus]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[OrderType] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderType]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Problem] FOREIGN KEY([ProblemId])
REFERENCES [dbo].[Problem] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Problem]
GO
ALTER TABLE [dbo].[Technician]  WITH CHECK ADD  CONSTRAINT [FK_Technician_Forman] FOREIGN KEY([FormanId])
REFERENCES [dbo].[Forman] ([Id])
GO
ALTER TABLE [dbo].[Technician] CHECK CONSTRAINT [FK_Technician_Forman]
GO
ALTER TABLE [dbo].[Technician]  WITH CHECK ADD  CONSTRAINT [FK_Technician_TV_Availability] FOREIGN KEY([AvailabilityId])
REFERENCES [dbo].[TV_Availability] ([Id])
GO
ALTER TABLE [dbo].[Technician] CHECK CONSTRAINT [FK_Technician_TV_Availability]
GO
ALTER TABLE [dbo].[TechnicianArea]  WITH CHECK ADD  CONSTRAINT [FK_TechnicianArea_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[TechnicianArea] CHECK CONSTRAINT [FK_TechnicianArea_Area]
GO
ALTER TABLE [dbo].[TechnicianArea]  WITH CHECK ADD  CONSTRAINT [FK_TechnicianArea_Technician] FOREIGN KEY([TechnicianId])
REFERENCES [dbo].[Technician] ([Id])
GO
ALTER TABLE [dbo].[TechnicianArea] CHECK CONSTRAINT [FK_TechnicianArea_Technician]
GO
ALTER TABLE [dbo].[TechnicianProblem]  WITH CHECK ADD  CONSTRAINT [FK_TechnicianProblem_Problem] FOREIGN KEY([ProblemId])
REFERENCES [dbo].[Problem] ([Id])
GO
ALTER TABLE [dbo].[TechnicianProblem] CHECK CONSTRAINT [FK_TechnicianProblem_Problem]
GO
ALTER TABLE [dbo].[TechnicianProblem]  WITH CHECK ADD  CONSTRAINT [FK_TechnicianProblem_Technician] FOREIGN KEY([TechnicianId])
REFERENCES [dbo].[Technician] ([Id])
GO
ALTER TABLE [dbo].[TechnicianProblem] CHECK CONSTRAINT [FK_TechnicianProblem_Technician]
GO
USE [master]
GO
ALTER DATABASE [aspnet-SmartDispatching-20170404095844] SET  READ_WRITE 
GO
