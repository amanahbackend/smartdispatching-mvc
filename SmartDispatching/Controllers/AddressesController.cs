﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class AddressesController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: Addresses
        public ActionResult Index()
		{
			//.Include(a => a.Block)
				//.Include(a => a.Street_Jaddah)


			var addresses = db.Addresses.Include(a => a.Area).Include(a => a.Customer).Include(a => a.Governorate);
            return View(addresses.ToList());
        }

        // GET: Addresses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // GET: Addresses/Create
        public ActionResult Create()
        {
            ViewBag.AreaId = new SelectList(db.Areas, "Id", "Name");
            ViewBag.BlockId = new SelectList(db.Blocks, "Id", "Name");
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name");
            ViewBag.GovId = new SelectList(db.Governorates, "Id", "Name");
            ViewBag.StreetId = new SelectList(db.Street_Jaddah, "Id", "Name");
            return View();
        }

        // POST: Addresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PACI,AreaId,CustomerId,StreetId,GovId,BlockId,Functional_Location,House_Kasima,Floor,AppartmentNo,AddressNote")] Address address)
        {
            if (ModelState.IsValid)
            {
                db.Addresses.Add(address);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AreaId = new SelectList(db.Areas, "Id", "Name", address.AreaId);
            ViewBag.BlockId = new SelectList(db.Blocks, "Id", "Name", address.BlockId);
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", address.CustomerId);
            ViewBag.GovId = new SelectList(db.Governorates, "Id", "Name", address.GovId);
            ViewBag.StreetId = new SelectList(db.Street_Jaddah, "Id", "Name", address.StreetId);
            return View(address);
        }

        // GET: Addresses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaId = new SelectList(db.Areas, "Id", "Name", address.AreaId);
            ViewBag.BlockId = new SelectList(db.Blocks, "Id", "Name", address.BlockId);
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", address.CustomerId);
            ViewBag.GovId = new SelectList(db.Governorates, "Id", "Name", address.GovId);
            ViewBag.StreetId = new SelectList(db.Street_Jaddah, "Id", "Name", address.StreetId);
            return View(address);
        }

        // POST: Addresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PACI,AreaId,CustomerId,StreetId,GovId,BlockId,Functional_Location,House_Kasima,Floor,AppartmentNo,AddressNote")] Address address)
        {
            if (ModelState.IsValid)
            {
                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaId = new SelectList(db.Areas, "Id", "Name", address.AreaId);
            ViewBag.BlockId = new SelectList(db.Blocks, "Id", "Name", address.BlockId);
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", address.CustomerId);
            ViewBag.GovId = new SelectList(db.Governorates, "Id", "Name", address.GovId);
            ViewBag.StreetId = new SelectList(db.Street_Jaddah, "Id", "Name", address.StreetId);
            return View(address);
        }

        // GET: Addresses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // POST: Addresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Address address = db.Addresses.Find(id);
            db.Addresses.Remove(address);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
