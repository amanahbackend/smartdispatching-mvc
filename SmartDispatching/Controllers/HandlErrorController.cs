﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SmartDispatching.Helper;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SmartDispatching.Controllers
{
    public class HandlErrorController : Controller
    {
		protected override void OnException(ExceptionContext filterContext)
		{
			ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

			var exception = filterContext.Exception;
			var httpException = exception as HttpException;
			var request = Request;
			Response.Clear();
			Server.ClearError();
			Response.TrySkipIisCustomErrors = true;
			var routeData = new RouteData();
			routeData.Values["controller"] = "Error";
			routeData.Values["action"] = "Index";
			routeData.Values["exception"] = exception;
			Response.StatusCode = 500;

			if (httpException != null)
			{
				Response.StatusCode = httpException.GetHttpCode();

				switch (Response.StatusCode)
				{
					case 500:
						routeData.Values["action"] = "HttpError500";
						break;
					case 404:
						routeData.Values["action"] = "HttpError404";
						break;
					default:
						routeData.Values["action"] = "Index";
						break;
				}
			}
			var errorService = exception.TargetSite;
			DashboardHelper.LogExceptions(request, exception, user);
			filterContext.ExceptionHandled = true;
		}
	}
}