﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

namespace SmartDispatching.Controllers
{
	public class TechniciansController : HandlErrorController
	{
		private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: Technicians
		public ActionResult Index()
		{
			var technicians = db.Technicians.Include(t => t.Forman).Include(t => t.TV_Availability).Where(x=>x.SuperDispatcherId == user.Id && x.IsDelete != true);

			var users = db.AspNetUsers.ToList();
			foreach (var item in technicians)
			{
				foreach (var user in users)
				{
					if (user.Id == item.DispatcherId)
					{
						item.DispatcherId = user.UserName;
					}
					if (user.Id == item.SuperDispatcherId)
					{
						item.SuperDispatcherId = user.UserName;
					}
				}
			}
			return View(technicians);
		}

		// GET: Technicians/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Technician technician = db.Technicians.Find(id);
			if (technician == null)
			{
				return HttpNotFound();
			}
			technician.DispatcherId = db.AspNetUsers.Where(x => x.Id == technician.DispatcherId).FirstOrDefault().UserName;
			technician.SuperDispatcherId = db.AspNetUsers.Where(x => x.Id == technician.SuperDispatcherId).FirstOrDefault().UserName;
			return View(technician);
		}

		// GET: Technicians/Create
		public ActionResult Create()
		{
			var targetDispatchers = db.SuperDispatchers.Where(x => x.SuperId == user.Id).ToList();
			List<AspNetUser> dispatchers = new List<AspNetUser>();
			//var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();

			for (int i = 0; i < targetDispatchers.Count; i++)
			{
				var dispatcherId = targetDispatchers[i].DispatcherId;
				var dispatcher = db.AspNetUsers.Where(x => x.Id == dispatcherId).FirstOrDefault();
				dispatchers.Add(dispatcher);
			}
			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName");
			var formans = db.Formen.Where(x => x.SuperDispatcherId == user.Id);
			ViewBag.FormanId = new SelectList(formans, "Id", "Name");

			var ghanimVehicles  = GetVehicles();
			if (ghanimVehicles != null)
			{
				if (ghanimVehicles.status.statusdescription == "SUCCESS")
				{
					ViewBag.CarId = new SelectList(ghanimVehicles.response.vehiclesreg, "VID", "Reg");
				}
				else
				{
					ViewBag.CarId = new SelectList(null, "Id", "Reg");
				}
			}
			else
			{
				ViewBag.CarId = new SelectList(null, "Id", "Reg");
			}

			//ViewBag.CarId = new SelectList(db.Cars, "Id", "Reg");

			//ViewBag.AvailabilityId = new SelectList(db.TV_Availability, "Id", "Name");
			return View();
		}

		// POST: Technicians/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create( Technician technician)
		{
			if (ModelState.IsValid)
			{
				var isFound = db.Technicians.Where(x => x.Name == technician.Name && x.IsDelete != true).FirstOrDefault();
				if (isFound == null)
				{
					technician.SuperDispatcherId = user.Id;
					technician.AvailabilityId = 1;
					db.Technicians.Add(technician);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				AddErrors("Technician name must be unique.");
			}

			//ViewBag.AvailabilityId = new SelectList(db.TV_Availability, "Id", "Name", technician.AvailabilityId);
			var targetDispatchers = db.SuperDispatchers.Where(x => x.SuperId == user.Id).ToList();
			List<AspNetUser> dispatchers = new List<AspNetUser>();
			//var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();

			for (int i = 0; i < targetDispatchers.Count; i++)
			{
				var dispatcherId = targetDispatchers[i].DispatcherId;
				var dispatcher = db.AspNetUsers.Where(x => x.Id == dispatcherId).FirstOrDefault();
				dispatchers.Add(dispatcher);
			}
			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName",technician.DispatcherId);
			var formans = db.Formen.Where(x => x.SuperDispatcherId == user.Id);
			ViewBag.FormanId = new SelectList(formans, "Id", "Name", technician.FormanId);
			return View(technician);
		}

		private VehicleModel GetVehicles()
		{
			try
			{
				var URL = ConfigurationManager.AppSettings["VechiclesAPI"];
				WebRequest request = WebRequest.Create(URL);
				var postData = "{input: { }}";
				var data = Encoding.ASCII.GetBytes(postData);

				request.Method = "POST";
				request.ContentType = "application/json";
				request.ContentLength = data.Length;
				using (var stream = request.GetRequestStream())
				{
					stream.Write(data, 0, data.Length);
				}
				//WebResponse response = request.GetResponse();
				//Stream dataStream = response.GetResponseStream();
				//// Open the stream using a StreamReader for easy access.  
				//StreamReader reader = new StreamReader(dataStream);
				//// Read the content.  
				//string responseFromServer = reader.ReadToEnd();
				var response = (HttpWebResponse)request.GetResponse();

				var responseFromServer = new StreamReader(response.GetResponseStream()).ReadToEnd();
				JavaScriptSerializer oJS = new JavaScriptSerializer();
				VehicleModel oRootObject = new VehicleModel();
				oRootObject = oJS.Deserialize<VehicleModel>(responseFromServer);

				return oRootObject;
			}
			catch(Exception ex)
			{
				return null;
			}
		}

		// GET: Technicians/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Technician technician = db.Technicians.Find(id);
			if (technician == null)
			{
				return HttpNotFound();
			}
			ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", technician.FormanId);
			var ghanimVehicles = GetVehicles();
			if (ghanimVehicles != null)
			{
				if (ghanimVehicles.status.statusdescription == "SUCCESS")
				{
					ViewBag.CarId = new SelectList(ghanimVehicles.response.vehiclesreg, "VID", "Reg", technician.CarId);
				}
				else
				{
					ViewBag.CarId = new SelectList(ghanimVehicles.response.vehiclesreg, "Id", "Reg", technician.CarId);
				}
			}
			else
			{
				ViewBag.CarId = new SelectList(null, "Id", "Reg",technician.CarId);
			}
			//ViewBag.CarId = new SelectList(db.Cars, "Id", "Reg", technician.CarId);
			ViewBag.AvailabilityId = new SelectList(db.TV_Availability, "Id", "Name", technician.AvailabilityId);
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			var superDispatchers = db.AspNetUsers.Where(x => x.RoleId == 4).ToList();
			List<SelectListItem> supers = new List<SelectListItem>();
			foreach (var item in superDispatchers)
			{
				supers.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == technician.SuperDispatcherId ? true : false)
				});
			}
			List<SelectListItem> dispatchersDrop = new List<SelectListItem>();
			foreach (var item in dispatchers)
			{
				dispatchersDrop.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == technician.DispatcherId ? true : false)
				});
			}
			ViewBag.DispatcherId = dispatchersDrop;
			ViewBag.SuperDispatcherId = supers;

			return View(technician);
		}

		// POST: Technicians/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Technician technician)
		{
			if (ModelState.IsValid)
			{
				db.Entry(technician).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", technician.FormanId);
			var ghanimVehicles = GetVehicles();
			if (ghanimVehicles != null)
			{
				if (ghanimVehicles.status.statusdescription == "SUCCESS")
				{
					ViewBag.CarId = new SelectList(ghanimVehicles.response.vehiclesreg, "VID", "Reg", technician.CarId);
				}
				else
				{
					ViewBag.CarId = new SelectList(null, "Id", "Reg", technician.CarId);
				}
			}
			else
			{
				ViewBag.CarId = new SelectList(null, "Id", "Reg", technician.CarId);
			}
			//ViewBag.CarId = new SelectList(db.Cars, "Id", "Reg", technician.CarId);
			ViewBag.AvailabilityId = new SelectList(db.TV_Availability, "Id", "Name", technician.AvailabilityId);
			return View(technician);
		}

		// GET: Technicians/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Technician technician = db.Technicians.Find(id);
			if (technician == null)
			{
				return HttpNotFound();
			}
			ViewBag.IsNotAllowed = TempData["ErrorMessage"] as string;

			return View(technician);
		}

		// POST: Technicians/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			//db.Technicians.Remove(technician);
			var assigned = db.AssignedOrders.Where(x => x.TechnicianId == id && x.IsClosed != true).ToList();
			if (assigned != null && assigned.Count  ==  0)
			{
				Technician technician = db.Technicians.Find(id);
				technician.IsDelete = true;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			else
			{
				TempData["ErrorMessage"] = "Please unassign the orders before delete this user.";
				return RedirectToAction("Delete", new { id = id });
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		[HttpPost]
		public JsonResult GetDispatchers(string id)
		{
			var dispatchersId = db.SuperDispatchers.Where(x => x.SuperId == id).ToList();
			List<AspNetUser> dispatchers = new List<AspNetUser>();

			for (int i = 0; i < dispatchersId.Count; i++)
			{
				var dispatcherId = dispatchersId[i].DispatcherId;
				var tempUser = db.AspNetUsers.Where(x => x.Id == dispatcherId).FirstOrDefault();
				dispatchers.Add(tempUser);
			}
			return Json(new SelectList(dispatchers, "Id", "UserName"), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetFormans(string id)
		{
			var formans = db.Formen.Where(x => x.DispatcherId == id).ToList();
			return Json(new SelectList(formans, "Id", "Name"), JsonRequestBehavior.AllowGet);
		}

		private void AddErrors(string result)
		{
			ModelState.AddModelError("", result);
		}
	}
}
