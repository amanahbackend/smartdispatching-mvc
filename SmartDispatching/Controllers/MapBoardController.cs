﻿using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

namespace SmartDispatching.Controllers
{
	[Authorize]
	public class MapBoardController : HandlErrorController
	{
		private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: MapBoard
		public ActionResult Index()
		{			
			List<OrderBoardViewModel> unAssignedOrderForBoard = new List<OrderBoardViewModel>();
			var unAssignedOrders = db.dispatcherUnAssigned(user.Id).ToList();
			for (int i = 0; i < unAssignedOrders.Count; i++)
			{
				OrderBoardViewModel tempOrder = new OrderBoardViewModel()
				{
					OrderNo = unAssignedOrders[i].OrderNo,
					CreatedDate = unAssignedOrders[i].CreatedDate,
					StatusId = unAssignedOrders[i].StatusId,
					DateTo = unAssignedOrders[i].DateTo,
					DateFrom = unAssignedOrders[i].DateFrom,
					Id = unAssignedOrders[i].orderId,
					CustomerNo = unAssignedOrders[i].CustomerNo,
					CustomerName = unAssignedOrders[i].CustomerName,
					CustomerPhone = unAssignedOrders[i].PhoneOne,
					Block = unAssignedOrders[i].Block,
					PACI = unAssignedOrders[i].PACI,
					Type = unAssignedOrders[i].Type,
					TypeId = unAssignedOrders[i].TypeId,
					Problem = unAssignedOrders[i].Problem,
					ProblemId = unAssignedOrders[i].ProblemId,
					Area = unAssignedOrders[i].Area,
					Street = unAssignedOrders[i].StreetId.ToString(),
					Governorate = unAssignedOrders[i].GovName			
				};
				if (unAssignedOrders[i].Lat != null && unAssignedOrders[i].Long!= null)
				{
					tempOrder.Lat = (decimal)unAssignedOrders[i].Lat;
					tempOrder.Long = (decimal)unAssignedOrders[i].Long;
				}
				unAssignedOrderForBoard.Add(tempOrder);
			}
			ViewBag.UnAssignedOrders = unAssignedOrderForBoard;

			ViewBag.Status = db.OrderStatus.ToList();
			ViewBag.Problems = db.Problems.ToList();
			ViewBag.Technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();
			ViewBag.Dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			var availability = db.TV_Availability.ToList();
			ViewBag.Availability = availability;

			return View();
		}

		// GET: MapBoard/Details/5
		public ActionResult Details(int id)
		{
			return View();
		}

		// GET: MapBoard/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: MapBoard/Create
		[HttpPost]
		public ActionResult Create(FormCollection collection)
		{
			try
			{
				// TODO: Add insert logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		// GET: MapBoard/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: MapBoard/Edit/5
		[HttpPost]
		public ActionResult Edit(int id, FormCollection collection)
		{
			try
			{
				// TODO: Add update logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		// GET: MapBoard/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: MapBoard/Delete/5
		[HttpPost]
		public ActionResult Delete(int id, FormCollection collection)
		{
			try
			{
				// TODO: Add delete logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		//////////////////////////////////////////////////////////////////////////////
		[HttpPost]
		public JsonResult GetOrders(DateTime dateFrom, DateTime dateTo, List<string> status, List<string> problems, List<string> technicians, List<string> dispatchers)
		{

			var nw = DateTime.Now;
			var userValues = db.MapFillters.Where(x => x.UserId == user.Id);
			db.MapFillters.RemoveRange(userValues);
			db.SaveChanges();


			if (status != null && status.Count > 0)
			{
				for (int i = 0; i < status.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Status",
						UserId = user.Id,
						value = status[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempStatus = db.OrderStatus.ToList();
				for (int i = 0; i < tempStatus.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Status",
						UserId = user.Id,
						value = tempStatus[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (problems != null && problems.Count > 0)
			{
				for (int i = 0; i < problems.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Problem",
						UserId = user.Id,
						value = problems[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempProblems = db.Problems.ToList();
				for (int i = 0; i < tempProblems.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Problem",
						UserId = user.Id,
						value = tempProblems[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}


			if (technicians != null && technicians.Count > 0)
			{
				for (int i = 0; i < technicians.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Technician",
						UserId = user.Id,
						value = technicians[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				//var tempTechnicians = db.Technicians.Where(x => x.DispatcherId == user.Id).ToList();
				//for (int i = 0; i < tempTechnicians.Count; i++)
				//{
				//	MapFillter rf = new MapFillter()
				//	{
				//		CreatedDate = nw,
				//		Type = "Technician",
				//		UserId = user.Id,
				//		value = tempTechnicians[i].Name
				//	};
				//	db.MapFillters.Add(rf);
				//	db.SaveChanges();
				//}
			}

			if (dispatchers != null && dispatchers.Count > 0)
			{
				for (int i = 0; i < dispatchers.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Dispatcher",
						UserId = user.Id,
						value = dispatchers[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempDispatchers = db.AspNetUsers.ToList();
				for (int i = 0; i < tempDispatchers.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Dispatcher",
						UserId = user.Id,
						value = tempDispatchers[i].UserName
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			MapFillter rfDateTo = new MapFillter()
			{
				CreatedDate = nw,
				Type = "DateTo",
				UserId = user.Id,
				value = dateTo.ToString()
			};
			db.MapFillters.Add(rfDateTo);
			db.SaveChanges();
			MapFillter rfDateFrom = new MapFillter()
			{
				CreatedDate = nw,
				Type = "DateFrom",
				UserId = user.Id,
				value = dateFrom.ToString()
			};
			db.MapFillters.Add(rfDateFrom);
			db.SaveChanges();

			var outOrders = db.getMapFillterData(user.Id).ToList();
			List<OrderBoardViewModel> mapData = new List<OrderBoardViewModel>();

			for (int i = 0; i < outOrders.Count; i++)
			{
				OrderBoardViewModel tempResult = new OrderBoardViewModel()
				{
					OrderNo = outOrders[i].OrderNo,
					CreatedDate = outOrders[i].CreatedDate,
					DateTo = outOrders[i].DateTo,
					DateFrom = outOrders[i].DateFrom,
					Id = outOrders[i].orderId,
					Status = outOrders[i].orderStatus,
					Problem = outOrders[i].problemName,
					ProblemId = outOrders[i].ProblemId,
					ProblemCode = outOrders[i].problemCode,
					TypeId = outOrders[i].TypeId,
					Type = outOrders[i].orderType,
					Area = outOrders[i].areaName
				};
				if (outOrders[i].Lat !=null && outOrders[i].Long != null)
				{
					tempResult.Lat =(decimal)outOrders[i].Lat;
					tempResult.Long = (decimal)outOrders[i].Long;
				}
				mapData.Add(tempResult);
			}
			var json = new JavaScriptSerializer().Serialize(mapData);

			return Json(json, JsonRequestBehavior.AllowGet);
			/*
			List<int> orderIds = new List<int>();
			List<int> orderStatusIds = new List<int>();
			List<int> orderProblemIds = new List<int>();
			List<int> orderTechnicianIds = new List<int>();
			List<int> orderDispatcherIds = new List<int>();

			if (status.Count > 0)
			{
				var statusIds = db.OrderStatus.Where(t => status.Contains(t.Name)).ToList();
				if (statusIds != null && statusIds.Count > 0)
				{
					for (int i = 0; i < statusIds.Count; i++)
					{
						var statusObj = statusIds[i].Orders.ToList();
						for (int j = 0; j < statusObj.Count; j++)
						{
							var order = statusObj[j];
							orderStatusIds.Add(order.Id);
						}
					}
				}
			}
			if (problems.Count > 0)
			{
				var problemIds = db.Problems.Where(t => problems.Contains(t.Name)).ToList();
				if (problemIds != null && problemIds.Count > 0)
				{
					for (int i = 0; i < problemIds.Count; i++)
					{
						var problemObj = problemIds[i].Orders.ToList();
						for (int j = 0; j < problemObj.Count; j++)
						{
							var order = problemObj[j];
							orderProblemIds.Add(order.Id);
						}
					}
				}
			}
			if (technicians.Count > 0)
			{
				var technicianIds = db.Technicians.Where(t => technicians.Contains(t.Name) && t.IsDelete != true).ToList();
				if (technicianIds != null && technicianIds.Count > 0)
				{
					for (int i = 0; i < technicianIds.Count; i++)
					{
						var techId = technicianIds[i].Id;
						var orders = db.AssignedOrders.Where(x => x.TechnicianId == techId).ToList();
						if (orders != null && orders.Count > 0)
						{
							for (int j = 0; j < orders.Count; j++)
							{
								int orderId = Convert.ToInt32(orders[j].OrderId);
								orderTechnicianIds.Add(orderId);
							}
						}
					}
				}
			}
			if (dispatchers.Count > 0)
			{
				var dispatcherIds = db.AspNetUsers.Where(t => dispatchers.Contains(t.UserName)).ToList();
				if (dispatcherIds != null && dispatcherIds.Count > 0)
				{
					for (int i = 0; i < dispatcherIds.Count; i++)
					{
						var dispatcherId = dispatcherIds[i].Id;
						var orders = db.AssignedOrders.Where(x => x.DispatcherId == dispatcherId).ToList();
						if (orders != null && orders.Count > 0)
						{
							for (int j = 0; j < orders.Count; j++)
							{
								int orderId = Convert.ToInt32(orders[j].OrderId);
								orderDispatcherIds.Add(orderId);
							}
						}
					}
				}
			}

			var intersection = new List<int>();
			if (orderProblemIds.Count > 0 && orderStatusIds.Count > 0 && orderDispatcherIds.Count > 0 && orderTechnicianIds.Count > 0)
			{
				var list1 = orderProblemIds.Intersect(orderStatusIds);
				var list2 = orderDispatcherIds.Intersect(orderTechnicianIds);
				intersection = list1.Intersect(list2).ToList();
			}
			else
			{
				var listOfLists = new List<List<int>>();
				var listValues = new List<int>();
				var skipper = 0;
				if (orderTechnicianIds.Count > 0)
				{
					listOfLists.Add(orderTechnicianIds);
					foreach (var item in orderTechnicianIds)
					{
						listValues.Add(item);
					}
					skipper += 1;
				}
				if (orderDispatcherIds.Count > 0)
				{
					listOfLists.Add(orderDispatcherIds);
					foreach (var item in orderDispatcherIds)
					{
						listValues.Add(item);
					}
					skipper += 1;
				}
				if (orderStatusIds.Count > 0)
				{
					listOfLists.Add(orderStatusIds);
					foreach (var item in orderStatusIds)
					{
						listValues.Add(item);
					}
					skipper += 1;
				}
				if (orderProblemIds.Count > 0)
				{
					listOfLists.Add(orderProblemIds);
					foreach (var item in orderProblemIds)
					{
						listValues.Add(item);
					}
					skipper += 1;
				}
				List<int> outList = new List<int>();
				if (listOfLists.Count > 0)
				{
					if (skipper == 4)
					{
						foreach (var item in listValues)
						{
							var itemCount = listValues.Where(x => x == item).ToList().Count;
							var isFound = outList.Where(x => x == item).ToList().Count;
							if (itemCount == 4 && isFound == 0)
							{
								outList.Add(item);
							}
						}
					}
					else if (skipper == 3)
					{
						foreach (var item in listValues)
						{
							var itemCount = listValues.Where(x => x == item).ToList().Count;
							var isFound = outList.Where(x => x == item).ToList().Count;
							if (itemCount == 3 && isFound == 0)
							{
								outList.Add(item);
							}
						}
					}
					else if (skipper == 2)
					{
						foreach (var item in listValues)
						{
							var itemCount = listValues.Where(x => x == item).ToList().Count;
							var isFound = outList.Where(x => x == item).ToList().Count;
							if (itemCount == 2 && isFound == 0)
							{
								outList.Add(item);
							}
						}
					}
					else if (skipper == 1)
					{
						foreach (var item in listValues)
						{
							var itemCount = listValues.Where(x => x == item).ToList().Count;
							var isFound = outList.Where(x => x == item).ToList().Count;
							if (itemCount == 1 && isFound == 0)
							{
								outList.Add(item);
							}
						}
					}

					//intersection = listOfLists.Skip(skipper).Aggregate(new HashSet<int>(listOfLists.First()), (h, e) => { h.IntersectWith(e); return h; }).ToList();
					intersection = outList;
				}
			}

			//var listOfLists = new List<List<int>>() { orderTechnicianIds, orderDispatcherIds, orderStatusIds, orderProblemIds };
			//var intersection = orderDispatcherIds.Intersect(orderTechnicianIds).Intersect(orderProblemIds).Intersect(orderStatusIds).ToList();


			var outOrders = db.Orders.Where(e => intersection.Contains(e.Id)).ToList();
			if (dateTo == dateFrom)
			{
				outOrders = outOrders.Where(x => DateTime.Parse(x.CreatedDate.ToString()).Date == dateFrom).ToList();
			}
			else
			{
				outOrders = outOrders.Where(x => DateTime.Parse(x.CreatedDate.ToString()).Date <= dateTo && DateTime.Parse(x.CreatedDate.ToString()).Date >= dateFrom).ToList();
			}

			List<OrderMapModel> toMap = new List<OrderMapModel>();
			for (int i = 0; i < outOrders.Count; i++)
			{
				var orderOutTemp = new OrderMapModel()
				{
					PACI = outOrders[i].Address.PACI,
					CreatedDate = outOrders[i].CreatedDate.ToString(),
					OrderNo = outOrders[i].OrderNo,
					Lat = outOrders[i].Address.Lat,
					Long = outOrders[i].Address.Long
				};
				if (outOrders[i].Address.Area != null)
				{
					orderOutTemp.Area = outOrders[i].Address.Area.Name;
				}
				if (outOrders[i].Problem != null)
				{
					orderOutTemp.Problem = outOrders[i].Problem.Name;
					orderOutTemp.ProblemCode = outOrders[i].Problem.Code;
				}
				if (outOrders[i].OrderStatu != null)
				{
					orderOutTemp.Status = outOrders[i].OrderStatu.Name;
				}

				if (outOrders[i].OrderType != null)
				{
					orderOutTemp.OrderType = outOrders[i].OrderType.Name;
				}
				toMap.Add(orderOutTemp);
			}*/

			//var json = new JavaScriptSerializer().Serialize(toMap);


		//	return Json(null, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult Reports(List<string> status, List<string> problems, List<string> technicians, List<string> dispatchers, List<string> types, List<string> divisions, List<string> company, DateTime dateFrom, DateTime dateTo, bool isTrip)
		{
			var nw = DateTime.Now;
			var userValues = db.MapFillters.Where(x => x.UserId == user.Id);
			db.MapFillters.RemoveRange(userValues);
			db.SaveChanges();

			if (status != null && status.Count > 0)
			{
				for (int i = 0; i < status.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Status",
						UserId = user.Id,
						value = status[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempStatus = db.OrderStatus.ToList();
				for (int i = 0; i < tempStatus.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Status",
						UserId = user.Id,
						value = tempStatus[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (problems != null && problems.Count > 0)
			{
				for (int i = 0; i < problems.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Problem",
						UserId = user.Id,
						value = problems[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempProblems = db.Problems.ToList();
				for (int i = 0; i < tempProblems.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Problem",
						UserId = user.Id,
						value = tempProblems[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}


			if (technicians != null && technicians.Count > 0)
			{
				for (int i = 0; i < technicians.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Technician",
						UserId = user.Id,
						value = technicians[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				//var tempTechnicians = db.Technicians.Where(x => x.DispatcherId == user.Id).ToList();
				//for (int i = 0; i < tempTechnicians.Count; i++)
				//{
				//	MapFillter rf = new MapFillter()
				//	{
				//		CreatedDate = nw,
				//		Type = "Technician",
				//		UserId = user.Id,
				//		value = tempTechnicians[i].Name
				//	};
				//	db.MapFillters.Add(rf);
				//	db.SaveChanges();
				//}
			}

			if (dispatchers != null && dispatchers.Count > 0)
			{
				for (int i = 0; i < dispatchers.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Dispatcher",
						UserId = user.Id,
						value = dispatchers[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempDispatchers = db.AspNetUsers.ToList();
				for (int i = 0; i < tempDispatchers.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Dispatcher",
						UserId = user.Id,
						value = tempDispatchers[i].UserName
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}


			if (types != null && types.Count > 0)
			{
				for (int i = 0; i < types.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Type",
						UserId = user.Id,
						value = types[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempTypes = db.OrderTypes.ToList();
				for (int i = 0; i < tempTypes.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Type",
						UserId = user.Id,
						value = tempTypes[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (divisions != null && divisions.Count > 0)
			{
				for (int i = 0; i < divisions.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Division",
						UserId = user.Id,
						value = divisions[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempDivisions = db.Divisions.ToList();
				for (int i = 0; i < tempDivisions.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Division",
						UserId = user.Id,
						value = tempDivisions[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (company != null && company.Count > 0)
			{
				for (int i = 0; i < company.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Company",
						UserId = user.Id,
						value = company[i]
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempCompany = db.CompanyCodes.ToList();
				for (int i = 0; i < tempCompany.Count; i++)
				{
					MapFillter rf = new MapFillter()
					{
						CreatedDate = nw,
						Type = "Company",
						UserId = user.Id,
						value = tempCompany[i].Name
					};
					db.MapFillters.Add(rf);
					db.SaveChanges();
				}
			}

			MapFillter rfDateTo = new MapFillter()
			{
				CreatedDate = nw,
				Type = "DateTo",
				UserId = user.Id,
				value = dateTo.ToString()
			};
			db.MapFillters.Add(rfDateTo);
			db.SaveChanges();
			MapFillter rfDateFrom = new MapFillter()
			{
				CreatedDate = nw,
				Type = "DateFrom",
				UserId = user.Id,
				value = dateFrom.ToString()
			};
			db.MapFillters.Add(rfDateFrom);
			db.SaveChanges();

			if (isTrip == true)
			{
				MapFillter rf = new MapFillter()
				{
					CreatedDate = nw,
					Type = "IsTrip",
					UserId = user.Id,
					value = isTrip.ToString()
				};
				db.MapFillters.Add(rf);
				db.SaveChanges();
			}
			else
			{
				MapFillter rf = new MapFillter()
				{
					CreatedDate = nw,
					Type = "IsTrip",
					UserId = user.Id,
					value = "False"
				};
				db.MapFillters.Add(rf);
				db.SaveChanges();
			}

			return Json(null, JsonRequestBehavior.AllowGet);

		}

		[HttpGet]
		public JsonResult GetCars()
		{
			var technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();

			List<CarTech> VIDs = new List<CarTech>();

			if (technicians.Count > 0)
			{
				for (int i = 0; i < technicians.Count; i++)
				{
					if (technicians[i].CarId != null)
					{
						var temp = new CarTech()
						{
							TechName = technicians[i].Name,
							VID = technicians[i].CarId
						};
						VIDs.Add(temp);
					}
				}
			}

			var json = new JavaScriptSerializer().Serialize(VIDs);

			return Json(json, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public string GetTechnicianName(string vid)
		{
			//	var car = db.Cars.Where(x => x.VID == vid).FirstOrDefault();
			if (vid != "")
			{
				return db.Technicians.Where(x => x.CarId == vid && x.IsDelete != true).FirstOrDefault().Name;
			}
			return null;
		}

		[HttpGet]
		public JsonResult GetDispatcherOrders()
		{
			var currentDispatcherOrders = db.getDispatcherOrdersForToday(user.Id).ToList();
			List<OrderBoardViewModel> currerntDispatcherOutOrders = new List<OrderBoardViewModel>();

			for (int i = 0; i < currentDispatcherOrders.Count; i++)
			{
				OrderBoardViewModel tempResult = new OrderBoardViewModel()
				{
					OrderNo = currentDispatcherOrders[i].OrderNo,
					CreatedDateString = currentDispatcherOrders[i].CreatedDate.ToString(),
					DateTo = currentDispatcherOrders[i].DateTo,
					DateFrom = currentDispatcherOrders[i].DateFrom,
					Id = currentDispatcherOrders[i].Id,
					Status = currentDispatcherOrders[i].statusName,
					Problem = currentDispatcherOrders[i].problemName,
					ProblemId = currentDispatcherOrders[i].ProblemId,
					ProblemCode = currentDispatcherOrders[i].problemCode,
					TypeId = currentDispatcherOrders[i].TypeId,
					Type = currentDispatcherOrders[i].typeName,
					Area = currentDispatcherOrders[i].areaName,
					Lat = (decimal)currentDispatcherOrders[i].Lat,
					Long = (decimal)currentDispatcherOrders[i].Long
				};
				currerntDispatcherOutOrders.Add(tempResult);
			}
			var json = new JavaScriptSerializer().Serialize(currerntDispatcherOutOrders);

			return Json(json, JsonRequestBehavior.AllowGet);
		}

	}
}
