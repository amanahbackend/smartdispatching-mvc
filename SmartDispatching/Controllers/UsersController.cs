﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
	[Authorize]
	public class UsersController : HandlErrorController
	{
		private ApplicationUserManager _userManager;
		private ApplicationDbContext db = new ApplicationDbContext();
		ApplicationUser currentUser = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}

		// GET: Users
		public ActionResult Index()
		{
			return View(db.Users.ToList());
		}

		// GET: Users/Details/5
		public ActionResult Details(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var applicationUser = db.Users.Find(id);
			var roleId = applicationUser.Roles.Where(x => x.UserId == applicationUser.Id).FirstOrDefault().RoleId;
			var role = db.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
			RegisterViewModel user = new RegisterViewModel()
			{
				UserName = applicationUser.UserName,
				Email = applicationUser.Email,
				RoleId = role
			};
			if (applicationUser == null)
			{
				return HttpNotFound();
			}
			return View(user);
		}

		// GET: Users/Create
		public ActionResult Create()
		{
			Dispatching dbcontext = new Dispatching();
			ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name");
			ViewBag.DivisionId = new SelectList(dbcontext.Divisions, "Id", "Name");
			return View();
		}

		//
		// POST: /Account/Register
		//[HttpPost]
		//[AllowAnonymous]
		//[ValidateAntiForgeryToken]
		//public async Task<ActionResult> Register(RegisterViewModel model)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
		//		var result = await UserManager.CreateAsync(user, model.Password);
		//		if (result.Succeeded)
		//		{
		//			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
		//			string roleName = roleManager.Roles.Where(x => x.Id == model.RoleId).FirstOrDefault().Name;
		//			await UserManager.AddToRoleAsync(user.Id, roleName);
		//			await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
		//			// For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
		//			// Send an email with this link
		//			// string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
		//			// var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
		//			// await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

		//			return RedirectToAction("Index", "Home");
		//		}
		//		AddErrors(result);
		//	}

		//	// If we got this far, something failed, redisplay form
		//	return View(model);
		//}

		// POST: Users/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(RegisterViewModel model)
		{
			var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
			var result = await UserManager.CreateAsync(user, model.Password);
			if (result.Succeeded)
			{
				var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
				string roleName = roleManager.Roles.Where(x => x.Id == model.RoleId).FirstOrDefault().Name;
				await UserManager.AddToRoleAsync(user.Id, roleName);

				Dispatching context = new Dispatching();
				var newUser = context.AspNetUsers.Where(x => x.UserName == model.UserName && x.Email == model.Email).FirstOrDefault();
				newUser.RoleId = Convert.ToInt32(model.RoleId);
				context.SaveChanges();

				if (newUser.RoleId == 4)
				{
					SuperDispatcher tempNew = new SuperDispatcher()
					{
						SuperId = newUser.Id,
						DivisionId = Convert.ToInt32(model.DivisionId)
					};
					context.SuperDispatchers.Add(tempNew);
					context.SaveChanges();
				}
				else if (newUser.RoleId == 7)
				{
					var foreman = context.Formen.Where(x => x.Name == newUser.UserName).FirstOrDefault();
					if (foreman != null)
					{
						foreman.ForemanUserId = newUser.Id;
						context.SaveChanges();
					}
					else
					{
						//Forman tempNew = new Forman()
						//{
						//	Name = newUser.UserName,
						//	ForemanUserId = newUser.Id
						//};
						//context.Formen.Add(tempNew);
						//context.SaveChanges();
					}
				}
				return View("Index", db.Users.ToList());
			}
			//db.Users.Add(applicationUser);
			//            db.SaveChanges();
			AddErrors(result);

			Dispatching dbcontext = new Dispatching();
			ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", model.RoleId);
			if (!string.IsNullOrEmpty(model.DivisionId))
			{
				ViewBag.DivisionId = new SelectList(dbcontext.Divisions, "Id", "Name", model.DivisionId);
			}
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> CreateForeman(Forman model)
		{

			string email = model.Name + "@ghanim.com";
			string password = "Test@12";
			string roleId = "7";
			var user = new ApplicationUser { UserName = model.Name, Email = email };
			var result = await UserManager.CreateAsync(user, password);
			Dispatching context = new Dispatching();

			if (result.Succeeded)
			{
				var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
				string roleName = roleManager.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
				await UserManager.AddToRoleAsync(user.Id, roleName);

				var newUser = context.AspNetUsers.Where(x => x.UserName == model.Name && x.Email == email).FirstOrDefault();
				newUser.RoleId = Convert.ToInt32(roleId);
				context.SaveChanges();

				if (newUser.RoleId == 7)
				{
					var foreman = context.Formen.Where(x => x.Name == newUser.UserName).FirstOrDefault();
					if (foreman != null)
					{
						foreman.ForemanUserId = newUser.Id;
						context.SaveChanges();
					}
					else
					{
						var isFound = context.Formen.Where(x => x.Name == model.Name).FirstOrDefault();
						if (isFound == null)
						{
							Forman newForman = new Forman()
							{
								Name = model.Name,
								PhoneNo = "00 965 " + model.PhoneNo,
								DispatcherId = model.DispatcherId,
								SuperDispatcherId = currentUser.Id,
								ForemanUserId = newUser.Id
							};
							context.Formen.Add(newForman);
							context.SaveChanges();

							//	string message = usersController.CreateForeman(registerForeman);
							return RedirectToAction("Index", "Formans");
						}
						AddErrors("Forman name must be unique.");
					}
				}
			}

			var dispatchers = context.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			var superDispatchers = context.AspNetUsers.Where(x => x.RoleId == 4).ToList();
			List<SelectListItem> supers = new List<SelectListItem>();
			foreach (var item in superDispatchers)
			{
				supers.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == model.SuperDispatcherId ? true : false)
				});
			}
			List<SelectListItem> dispatchersDrop = new List<SelectListItem>();
			foreach (var item in dispatchers)
			{
				dispatchersDrop.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == model.DispatcherId ? true : false)
				});
			}
			ViewBag.DispatcherId = dispatchersDrop;
			ViewBag.SuperDispatcherId = supers;
			
			return RedirectToAction("Create", "Formans", model);

			//var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
			//var result = UserManager.Create(user, model.Password);
			//if (result.Succeeded)
			//{
			//	var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
			//	string roleName = roleManager.Roles.Where(x => x.Id == model.RoleId).FirstOrDefault().Name;
			//	UserManager.AddToRoleAsync(user.Id, roleName);

			//	Dispatching context = new Dispatching();
			//	var newUser = context.AspNetUsers.Where(x => x.UserName == model.UserName && x.Email == model.Email).FirstOrDefault();
			//	newUser.RoleId = Convert.ToInt32(model.RoleId);
			//	context.SaveChanges();

			//	if (newUser.RoleId == 4)
			//	{
			//		SuperDispatcher tempNew = new SuperDispatcher()
			//		{
			//			SuperId = newUser.Id,
			//			DivisionId = Convert.ToInt32(model.DivisionId)
			//		};
			//		context.SuperDispatchers.Add(tempNew);
			//		context.SaveChanges();
			//	}
			//	else if (newUser.RoleId == 7)
			//	{
			//		var foreman = context.Formen.Where(x => x.Name == newUser.UserName).FirstOrDefault();
			//		if (foreman != null)
			//		{
			//			foreman.ForemanUserId = newUser.Id;
			//			context.SaveChanges();
			//		}
			//		else
			//		{
			//			//Forman tempNew = new Forman()
			//			//{
			//			//	Name = newUser.UserName,
			//			//	ForemanUserId = newUser.Id
			//			//};
			//			//context.Formen.Add(tempNew);
			//			//context.SaveChanges();
			//		}
			//	}
			//	return "Success";
			//}
			////db.Users.Add(applicationUser);
			////            db.SaveChanges();
			//AddErrors(result);

			//Dispatching dbcontext = new Dispatching();
			//ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", model.RoleId);
			//if (!string.IsNullOrEmpty(model.DivisionId))
			//{
			//	ViewBag.DivisionId = new SelectList(dbcontext.Divisions, "Id", "Name", model.DivisionId);
			//}
			//return "Failed";
		}

		private void AddErrors(string result)
		{
			ModelState.AddModelError("", result);
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}
		// GET: Users/Edit/5
		public ActionResult Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser applicationUser = db.Users.Find(id);
			var roleId = applicationUser.Roles.Where(x => x.UserId == applicationUser.Id).FirstOrDefault().RoleId;
			var role = db.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
			RegisterViewModel user = new RegisterViewModel()
			{
				UserName = applicationUser.UserName,
				Email = applicationUser.Email,
				RoleId = role
			};
			SelectListItem selected = new SelectListItem()
			{
				Selected = true,
				Text = role,
				Value = roleId
			};
			if (applicationUser == null)
			{
				return HttpNotFound();
			}
			ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", selected);
			return View(user);
		}

		// POST: Users/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(ApplicationUser applicationUser)
		{
			if (ModelState.IsValid)
			{
				Dispatching dbcontext = new Dispatching();
				var user = dbcontext.AspNetUsers.Where(x => x.Id == applicationUser.Id).FirstOrDefault();
				user.UserName = applicationUser.UserName;
				user.Email = applicationUser.Email;
				dbcontext.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(applicationUser);
		}

		// GET: Users/Delete/5
		public ActionResult Delete(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser applicationUser = db.Users.Find(id);
			if (applicationUser == null)
			{
				return HttpNotFound();
			}
			return View(applicationUser);
		}

		// POST: Users/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(string id)
		{
			ApplicationUser applicationUser = db.Users.Find(id);
			db.Users.Remove(applicationUser);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
