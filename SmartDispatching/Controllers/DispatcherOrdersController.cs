﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class DispatcherOrdersController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: DispatcherOrders
        public ActionResult Index()
        {
			//         var dispatcherOrders = db.DispatcherOrders.Include(d => d.AspNetUser).Include(d => d.Order);

			//var users = db.AspNetUsers.ToList();
			//foreach (var item in dispatcherOrders)
			//{
			//	foreach (var user in users)
			//	{
			//		if (user.Id == item.DispatcherId)
			//		{
			//			item.DispatcherId = user.UserName;
			//		}
			//	}
			//}

			//return View(dispatcherOrders.ToList());
			return View();
        }

        // GET: DispatcherOrders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatcherOrder dispatcherOrder = db.DispatcherOrders.Find(id);
            if (dispatcherOrder == null)
            {
                return HttpNotFound();
            }
            return View(dispatcherOrder);
        }

        // GET: DispatcherOrders/Create
        public ActionResult Create()
        {
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5);

			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName");
			ViewBag.OrderId = new SelectList(db.Orders, "Id", "Id");
			return View();
        }

        // POST: DispatcherOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DispatcherId,OrderId")] DispatcherOrder dispatcherOrder)
        {
            if (ModelState.IsValid)
            {
                db.DispatcherOrders.Add(dispatcherOrder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5);

			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName", dispatcherOrder.DispatcherId);
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Id", dispatcherOrder.OrderId);
            return View(dispatcherOrder);
        }

        // GET: DispatcherOrders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatcherOrder dispatcherOrder = db.DispatcherOrders.Find(id);
            if (dispatcherOrder == null)
            {
                return HttpNotFound();
            }
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5);

			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName", dispatcherOrder.DispatcherId);
			ViewBag.OrderId = new SelectList(db.Orders, "Id", "Id", dispatcherOrder.OrderId);
			return View(dispatcherOrder);
        }

        // POST: DispatcherOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DispatcherId,OrderId")] DispatcherOrder dispatcherOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dispatcherOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
			}
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5);
			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName", dispatcherOrder.DispatcherId);
			ViewBag.OrderId = new SelectList(db.Orders, "Id", "Id", dispatcherOrder.OrderId);
			return View(dispatcherOrder);
        }

        // GET: DispatcherOrders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatcherOrder dispatcherOrder = db.DispatcherOrders.Find(id);
            if (dispatcherOrder == null)
            {
                return HttpNotFound();
            }
            return View(dispatcherOrder);
        }

        // POST: DispatcherOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DispatcherOrder dispatcherOrder = db.DispatcherOrders.Find(id);
            db.DispatcherOrders.Remove(dispatcherOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
