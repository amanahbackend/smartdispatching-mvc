﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
	public class DispatcherDatasController : HandlErrorController
	{
		private Dispatching db = new Dispatching();

		// GET: DispatcherDatas
		public ActionResult Index()
		{
			var dispatcherDatas = db.DispatcherDatas.Include(d => d.SAPArea).Include(d => d.AspNetUser).Include(d => d.Division).Include(d => d.Problem);
			return View(dispatcherDatas.ToList());
		}

		// GET: DispatcherDatas/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherData dispatcherData = db.DispatcherDatas.Find(id);
			if (dispatcherData == null)
			{
				return HttpNotFound();
			}
			return View(dispatcherData);
		}

		// GET: DispatcherDatas/Create
		public ActionResult Create()
		{
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();

			ViewBag.AreaId = new MultiSelectList(db.SAPAreas, "Id", "Name");
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName");
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name");
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name");
			return View();
		}

		// POST: DispatcherDatas/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		public JsonResult Create(string dispatcherId, int problemId, List<int> areaIds)
		{
			//if (ModelState.IsValid)
			//{
			var divisionId = db.DispatcherDivisions.Where(x => x.DispatcherId == dispatcherId).FirstOrDefault().DivisionId;

			DispatcherData tempDispatcher = new DispatcherData();
			List<DispatcherData> addThis = new List<DispatcherData>();
			if (areaIds.Count > 0)
			{
				for (int i = 0; i < areaIds.Count; i++)
				{
					var id = areaIds[i];
					tempDispatcher = db.DispatcherDatas.Where(x => x.AreaId == id && x.ProblemId == problemId && x.DivisionId == divisionId).FirstOrDefault();
					if (tempDispatcher == null)
					{
						addThis.Add(new DispatcherData()
						{
							DispatcherId = dispatcherId,
							ProblemId = problemId,
							AreaId = areaIds[i],
							DivisionId = Convert.ToInt32(divisionId)
						});
					}
				}

				db.DispatcherDatas.AddRange(addThis);
				db.SaveChanges();
				return Json(new { IsCreated = true, Content = "Success" });
			}
			return Json(new { IsCreated = false, Content = "Failed" });

			//	if (isFound == null)
			//	{
			//		dispatcherData.DivisionId = Convert.ToInt32(divisionId);
			//		db.DispatcherDatas.Add(dispatcherData);
			//		db.SaveChanges();
			//		return RedirectToAction("Index");
			//	}
			//	ModelState.AddModelError("", "Can not assign that  area and problem to this dispatcher");
			//}
			//var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();

			//ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", dispatcherData.AreaId);
			//ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherData.DispatcherId);
			//ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", dispatcherData.ProblemId);
			//	return View(); //dispatcherData
		}

		// GET: DispatcherDatas/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherData dispatcherData = db.DispatcherDatas.Find(id);
			if (dispatcherData == null)
			{
				return HttpNotFound();
			}
			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", dispatcherData.AreaId);
			ViewBag.DispatcherId = new SelectList(db.AspNetUsers, "Id", "Email", dispatcherData.DispatcherId);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", dispatcherData.DivisionId);
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", dispatcherData.ProblemId);
			return View(dispatcherData);
		}

		// POST: DispatcherDatas/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,DispatcherId,AreaId,DivisionId,ProblemId")] DispatcherData dispatcherData)
		{
			if (ModelState.IsValid)
			{
				db.Entry(dispatcherData).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", dispatcherData.AreaId);
			ViewBag.DispatcherId = new SelectList(db.AspNetUsers, "Id", "Email", dispatcherData.DispatcherId);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", dispatcherData.DivisionId);
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", dispatcherData.ProblemId);
			return View(dispatcherData);
		}

		// GET: DispatcherDatas/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherData dispatcherData = db.DispatcherDatas.Find(id);
			if (dispatcherData == null)
			{
				return HttpNotFound();
			}
			return View(dispatcherData);
		}

		// POST: DispatcherDatas/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			DispatcherData dispatcherData = db.DispatcherDatas.Find(id);
			db.DispatcherDatas.Remove(dispatcherData);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
