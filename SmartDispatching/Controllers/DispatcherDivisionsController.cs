﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class DispatcherDivisionsController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: DispatcherDivisions
        public ActionResult Index()
        {
            var dispatcherDivisions = db.DispatcherDivisions.Include(d => d.AspNetUser).Include(d => d.Division);
            return View(dispatcherDivisions.ToList());
        }

        // GET: DispatcherDivisions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatcherDivision dispatcherDivision = db.DispatcherDivisions.Find(id);
            if (dispatcherDivision == null)
            {
                return HttpNotFound();
            }
            return View(dispatcherDivision);
        }

        // GET: DispatcherDivisions/Create
        public ActionResult Create()
        {
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName");
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name");
            return View();
        }

        // POST: DispatcherDivisions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DispatcherId,DivisionId")] DispatcherDivision dispatcherDivision)
        {
            if (ModelState.IsValid)
            {
                db.DispatcherDivisions.Add(dispatcherDivision);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherDivision.DispatcherId);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", dispatcherDivision.DivisionId);
            return View(dispatcherDivision);
        }

        // GET: DispatcherDivisions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatcherDivision dispatcherDivision = db.DispatcherDivisions.Find(id);
            if (dispatcherDivision == null)
            {
                return HttpNotFound();
            }
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherDivision.DispatcherId);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", dispatcherDivision.DivisionId);
            return View(dispatcherDivision);
        }

        // POST: DispatcherDivisions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DispatcherId,DivisionId")] DispatcherDivision dispatcherDivision)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dispatcherDivision).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherDivision.DispatcherId);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", dispatcherDivision.DivisionId);
            return View(dispatcherDivision);
        }

        // GET: DispatcherDivisions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatcherDivision dispatcherDivision = db.DispatcherDivisions.Find(id);
            if (dispatcherDivision == null)
            {
                return HttpNotFound();
            }
            return View(dispatcherDivision);
        }

        // POST: DispatcherDivisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DispatcherDivision dispatcherDivision = db.DispatcherDivisions.Find(id);
            db.DispatcherDivisions.Remove(dispatcherDivision);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
