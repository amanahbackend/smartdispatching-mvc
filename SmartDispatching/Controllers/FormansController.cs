﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace SmartDispatching.Controllers
{
	public class FormansController : HandlErrorController
	{
		private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: Formans
		public ActionResult Index()
		{
			var formans = db.Formen.Where(x => x.SuperDispatcherId == user.Id).ToList();

			var users = db.AspNetUsers.ToList();
			foreach (var item in formans)
			{
				foreach (var user in users)
				{
					if (user.Id == item.DispatcherId)
					{
						item.DispatcherId = user.UserName;
					}
					if (user.Id == item.SuperDispatcherId)
					{
						item.SuperDispatcherId = user.UserName;
					}
				}
			}
			return View(formans);
		}

		// GET: Formans/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Forman forman = db.Formen.Find(id);
			if (forman == null)
			{
				return HttpNotFound();
			}
			forman.DispatcherId = db.AspNetUsers.Where(x => x.Id == forman.DispatcherId).FirstOrDefault().UserName;
			forman.SuperDispatcherId = db.AspNetUsers.Where(x => x.Id == forman.SuperDispatcherId).FirstOrDefault().UserName;
			return View(forman);
		}

		// GET: Formans/Create
		public ActionResult Create()
		{
			var targetDispatchers = db.SuperDispatchers.Where(x => x.SuperId == user.Id).ToList();
			List<AspNetUser> dispatchers = new List<AspNetUser>();
			//var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();

			for (int i = 0; i < targetDispatchers.Count; i++)
			{
				var dispatcherId = targetDispatchers[i].DispatcherId;
				var dispatcher = db.AspNetUsers.Where(x => x.Id == dispatcherId).FirstOrDefault();
				dispatchers.Add(dispatcher);
			}
			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName");
			return View();
		}

		// POST: Formans/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(FormanViewModel forman)
		{
			if (ModelState.IsValid)
			{
				var isFound = db.Formen.Where(x => x.Name == forman.Name).FirstOrDefault();
				if (isFound == null)
				{
					Forman newForman = new Forman()
					{
						Name = forman.Name,
						PhoneNo = "00 965 " + forman.PhoneNo,
						DispatcherId = forman.DispatcherId,
						SuperDispatcherId = user.Id
					};
					db.Formen.Add(newForman);
					db.SaveChanges();

					UsersController usersController = new UsersController();
					RegisterViewModel registerForeman = new RegisterViewModel()
					{
						UserName = newForman.Name,
						Email = newForman.Name + "@ghanim.com",
						Password = "Test@12",
						ConfirmPassword = "Test@12",
						RoleId = "7"
					};
				//	string message = usersController.CreateForeman(registerForeman);

					return RedirectToAction("Index");
				}
				AddErrors("Forman name must be unique.");
			}

			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			var superDispatchers = db.AspNetUsers.Where(x => x.RoleId == 4).ToList();
			List<SelectListItem> supers = new List<SelectListItem>();
			foreach (var item in superDispatchers)
			{
				supers.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == forman.SuperDispatcherId ? true : false)
				});
			}
			List<SelectListItem> dispatchersDrop = new List<SelectListItem>();
			foreach (var item in dispatchers)
			{
				dispatchersDrop.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == forman.DispatcherId ? true : false)
				});
			}
			ViewBag.DispatcherId = dispatchersDrop;
			ViewBag.SuperDispatcherId = supers;

			return View(forman);
		}

		// GET: Formans/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			Forman forman = db.Formen.Find(id);

			if (forman == null)
			{
				return HttpNotFound();
			}
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			var superDispatchers = db.AspNetUsers.Where(x => x.RoleId == 4).ToList();
			List<SelectListItem> supers = new List<SelectListItem>();
			foreach (var item in superDispatchers)
			{
				supers.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == forman.SuperDispatcherId ? true : false)
				});
			}
			List<SelectListItem> dispatchersDrop = new List<SelectListItem>();
			foreach (var item in dispatchers)
			{
				dispatchersDrop.Add(new SelectListItem()
				{
					Value = item.Id,
					Text = item.UserName,
					Selected = (item.Id == forman.DispatcherId ? true : false)
				});
			}
			ViewBag.DispatcherId = dispatchersDrop;
			ViewBag.SuperDispatcherId = supers;

			FormanViewModel formanEdit = new FormanViewModel()
			{
				Id = forman.Id,
				Name = forman.Name,
				PhoneNo = forman.PhoneNo.Replace("00 965 ", ""),
				DispatcherId = forman.DispatcherId,
				SuperDispatcherId = forman.SuperDispatcherId
			};

			return View(formanEdit);
		}

		// POST: Formans/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(FormanViewModel forman)
		{
			if (ModelState.IsValid)
			{
				var savedForman = db.Formen.Where(x => x.Id == forman.Id).FirstOrDefault();
				savedForman.Name = forman.Name;
				savedForman.PhoneNo = forman.PhoneNo;
				savedForman.DispatcherId = forman.DispatcherId;
				savedForman.SuperDispatcherId = forman.SuperDispatcherId;

				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(forman);
		}

		// GET: Formans/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Forman forman = db.Formen.Find(id);
			if (forman == null)
			{
				return HttpNotFound();
			}
			return View(forman);
		}

		// POST: Formans/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			Forman forman = db.Formen.Find(id);
			db.Formen.Remove(forman);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		[HttpPost]
		public JsonResult GetDispatchers(string id)
		{
			var dispatchersId = db.SuperDispatchers.Where(x => x.SuperId == id).ToList();
			List<AspNetUser> dispatchers = new List<AspNetUser>();

			for (int i = 0; i < dispatchersId.Count; i++)
			{
				var dispatcherId = dispatchersId[i].DispatcherId;
				var tempUser = db.AspNetUsers.Where(x => x.Id == dispatcherId).FirstOrDefault();
				dispatchers.Add(tempUser);
			}
			return Json(new SelectList(dispatchers, "Id", "UserName"), JsonRequestBehavior.AllowGet);
		}

		private void AddErrors(string result)
		{
			ModelState.AddModelError("", result);
		}
	}
}
