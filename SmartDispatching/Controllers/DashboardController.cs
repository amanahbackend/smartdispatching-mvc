﻿using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using SmartDispatching.Helper;
using System.Data.Entity.Core.Objects;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace SmartDispatching.Controllers
{
	[Authorize]
	public class DashboardController : HandlErrorController
	{
		private Dispatching db = new Dispatching();

		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: Dashboard
		public ActionResult Index()
		{
			var workUpdates = db.Work_Update.ToList();
			List<Work_Update> outListUpdate = new List<Work_Update>();
			for (int i = 0; i < workUpdates.Count; i++)
			{
				Work_Update tempWU = new Work_Update()
				{
					Id = workUpdates[i].Id,
					Name = workUpdates[i].Name,
					StatusId = workUpdates[i].StatusId
				};
				outListUpdate.Add(tempWU);
			}
			ViewBag.WorkUpdateId = outListUpdate;
			ViewBag.Statuses = db.OrderStatus.ToList();
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5 && x.Id != user.Id).ToList();
			List<DisparcheViewModel> dispatcherModel = new List<DisparcheViewModel>();
			foreach (var item in dispatchers)
			{
				DisparcheViewModel temp = new DisparcheViewModel()
				{
					id = item.Id,
					Name = item.UserName
				};
				dispatcherModel.Add(temp);
			}
			ViewBag.Dispatchers = dispatcherModel;

			//var unAssignedOrders = db.AssignedOrders.Where(x => x.DispatcherId == user.Id && x.FormanId == null && x.IsClosed == false).ToList();
			List<OrderBoardViewModel> unAssignedOrderForBoard = new List<OrderBoardViewModel>();
			var unAssignedOrders = db.dispatcherUnAssigned(user.Id).ToList();
			for (int i = 0; i < unAssignedOrders.Count; i++)
			{
				OrderBoardViewModel tempOrder = new OrderBoardViewModel()
				{
					OrderNo = unAssignedOrders[i].OrderNo,
					CreatedDate = unAssignedOrders[i].CreatedDate,
					StatusId = unAssignedOrders[i].StatusId,
					DateTo = unAssignedOrders[i].DateTo,
					DateFrom = unAssignedOrders[i].DateFrom,
					Id = unAssignedOrders[i].orderId,
					CustomerNo = unAssignedOrders[i].CustomerNo,
					CustomerName = unAssignedOrders[i].CustomerName,
					CustomerPhone = unAssignedOrders[i].PhoneOne,
					Block = unAssignedOrders[i].Block,
					PACI = unAssignedOrders[i].PACI,
					Type = unAssignedOrders[i].Type,
					TypeId = unAssignedOrders[i].TypeId,
					Problem = unAssignedOrders[i].Problem,
					ProblemId = unAssignedOrders[i].ProblemId,
					Area = unAssignedOrders[i].Area,
					Street = unAssignedOrders[i].StreetId.ToString(),
					Governorate = unAssignedOrders[i].GovName
				};
				unAssignedOrderForBoard.Add(tempOrder);
			}
			ViewBag.UnAssignedOrders = unAssignedOrderForBoard;

			var formans = db.Formen.Where(x => x.DispatcherId == user.Id).ToList();
			var technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();
			var availability = db.TV_Availability.ToList();
			ViewBag.Technicians = technicians;
			ViewBag.Availability = availability;
			List<FormanBoardViewModel> outFormans = new List<FormanBoardViewModel>();

			foreach (var item in formans)
			{
				var tempTechnicians = technicians.Where(x => x.FormanId == item.Id).ToList();
				List<TechnicianBoardViewModel> tech = new List<TechnicianBoardViewModel>();
				foreach (var techItem in tempTechnicians)
				{
					List<OrderBoardViewModel> orders = new List<OrderBoardViewModel>();
					var tempout = db.getAssignedOrdersForTechs(techItem.Id.ToString()).ToList();
					for (int i = 0; i < tempout.Count; i++)
					{
						OrderBoardViewModel orderDetail = new OrderBoardViewModel()
						{
							Id = tempout[i].orderId,
							CreatedDate = tempout[i].CreatedDate,
							DateFrom = tempout[i].DateFrom,
							DateTo = tempout[i].DateTo,
							OrderNo = tempout[i].OrderNo,
							CustomerName = tempout[i].CustomerName,
							Area = tempout[i].Area,
							Block = tempout[i].Block,
							Problem = tempout[i].Problem,
							ProblemId = tempout[i].ProblemId,
							Type = tempout[i].Type,
							TypeId = tempout[i].TypeId
						};
						orders.Add(orderDetail);
					}
					//foreach (var order in techItem.AssignedOrders)
					//{
					//	var tempOrder = db.Orders.Where(x => x.Id == order.OrderId).FirstOrDefault();
					//	var orderDates = db.AssignedOrders.Where(x => x.OrderId == order.OrderId && x.IsClosed == false).FirstOrDefault();
					//	var customer = db.Customers.Where(x => x.Id == order.Order.CustomerId).FirstOrDefault();
					//	var address = db.Addresses.Where(x => x.Id == order.Order.AddressId).FirstOrDefault();
					//	string block = "";
					//	if (address.BlockId != null)
					//	{
					//		var tempBlock = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
					//		if (tempBlock != null)
					//		{
					//			block = tempBlock.Name;
					//		}
					//	}
					//	if (orderDates != null)
					//	{
					//		OrderBoardViewModel orderDetail = new OrderBoardViewModel()
					//		{
					//			Id = tempOrder.Id,
					//			CreatedDate = tempOrder.CreatedDate,
					//			DateFrom = orderDates.DateFrom,
					//			DateTo = orderDates.DateTo,
					//			OrderNo = tempOrder.OrderNo,
					//			CustomerName = customer.Name,
					//			Area = address.Area.Name,
					//			Block = block
					//		};
					//		//if (orderDates.ActionId == 1)
					//		//{
					//		//	orderDetail.ActionName = "No Action";
					//		//}
					//		//else if (orderDates.ActionId == 2)
					//		//{
					//		//	orderDetail.ActionName = "Received";
					//		//}
					//		//else if (orderDates.ActionId == 3)
					//		//{
					//		//	orderDetail.ActionName = "Started";
					//		//}
					//		//else if (orderDates.ActionId == 4)
					//		//{
					//		//	orderDetail.ActionName = "Finished";
					//		//}
					//		//else
					//		//{
					//		//	orderDetail.ActionName = "No Action";
					//		//}

					//		if (tempOrder.Problem != null)
					//		{
					//			orderDetail.Problem = tempOrder.Problem.Name;
					//			orderDetail.ProblemId = tempOrder.ProblemId;
					//		}
					//		if (tempOrder.OrderType != null)
					//		{
					//			orderDetail.Type = tempOrder.OrderType.Name;
					//			orderDetail.TypeId = tempOrder.TypeId;
					//		}

					//		orders.Add(orderDetail);
					//	}

					//}

					int counts = 0;
					var compeletedOrders = db.AssignedOrders.Where(x => x.TechnicianId == techItem.Id && x.IsClosed == true).ToList();
					for (int i = 0; i < compeletedOrders.Count(); i++)
					{
						if (compeletedOrders[i].ActionDate != null)
						{
							if (DateTime.Parse(compeletedOrders[i].ActionDate.ToString()).Date == DateTime.Now.Date)
							{
								counts++;
							}
						}
					}
					TechnicianBoardViewModel tempTech = new TechnicianBoardViewModel()
					{
						Id = techItem.Id,
						Name = techItem.Name,
						OrderCounts = techItem.AssignedOrders.Count,
						Orders = orders,
						CompeletedCount = counts
					};
					var availabilityObj = db.TV_Availability.Where(x => x.Id == techItem.AvailabilityId).FirstOrDefault();
					if (availabilityObj != null)
					{
						tempTech.Availability = availabilityObj.Name;
					}
					tech.Add(tempTech);
				}

				FormanBoardViewModel temp = new FormanBoardViewModel()
				{
					AllOrderCount = tempTechnicians.Count,
					Name = item.Name,
					Id = item.Id,
					Technicians = tech
				};
				outFormans.Add(temp);
			}

            ViewBag.CompanyCodeId = new SelectList(db.CompanyCodes, "Id", "Name");
            ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name");
            ViewBag.PriorityId = new SelectList(db.OrderPriorities, "Id", "Name");
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name");
            ViewBag.OrderTypeId = new SelectList(db.OrderTypes, "Id", "Name");
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name");
            //ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", tempOrder.CustomerId);

            //ViewBag.ContractTypeId = new SelectList(db.ContractTypes, "Id", "Name", );
            //ViewBag.CancellationReason = new SelectList(db.Cancellation_Reasons, "Id", "Name");

            ViewBag.Governorates = db.Governorates.ToList();
            ViewBag.Areas = db.Areas.ToList();
            ViewBag.Blocks = db.Blocks.ToList();
            ViewBag.Streets = db.Street_Jaddah.ToList();


            ViewBag.Formans = JsonConvert.SerializeObject(outFormans, Formatting.Indented,
					new JsonSerializerSettings
					{
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore
					});

			return View();
		}

		#region create_and_upload_order
		[HttpPost]
		public JsonResult CreateOrder(OrderViewModel OrderData)
		{
			if (OrderData != null)
			{
				var isFound = db.Orders.Where(x => x.OrderNo == OrderData.OrderNo).FirstOrDefault();
				if (isFound == null)
				{
					Customer tempCustomer = new Customer()
					{
						CustomerNo = OrderData.CustomerNo,
						Name = OrderData.Name,
						PhoneOne = OrderData.PhoneOne,
						PhoneTwo = OrderData.PhoneTwo
					};
					db.Customers.Add(tempCustomer);
					db.SaveChanges();

					var customerId = db.Customers.Where(x => x.PhoneOne == OrderData.PhoneOne && x.Name == OrderData.Name).FirstOrDefault().Id;
					if (OrderData.ContractTypeId > 0 && OrderData.CreatedDate != null && OrderData.ExpirationDate != null)
					{
						Contract tempContract = new Contract()
						{
							ContractNo = OrderData.ContractNo,
							TypeId = OrderData.ContractTypeId,
							CreatedDate = OrderData.CreatedDate,
							ExpirationDate = OrderData.ExpirationDate,
							CustomerId = customerId
						};
						db.Contracts.Add(tempContract);
						db.SaveChanges();
					}

					int? areaId = null;
					if (OrderData.AreaId > 0)
					{
						areaId = db.Areas.Where(x => x.OriginalId == OrderData.AreaId).FirstOrDefault().Id;
					}

					Address tempAddress = new Address()
					{
						AddressNote = OrderData.AddressNote,
						AppartmentNo = OrderData.AppartmentNo,
						AreaId = areaId,
						BlockId = OrderData.BlockId,
						GovId = OrderData.GovId,
						Functional_Location = OrderData.Functional_Location,
						PACI = OrderData.PACI,
						StreetId = OrderData.StreetId,
						House_Kasima = OrderData.House_Kasima,
						Floor = OrderData.Floor,
						CustomerId = customerId
					};
					//if (tempAddress.PACI != null)
					//{
					//	PACIModel tempPaci = GetLocationByPACI(tempAddress.PACI);
					//	tempAddress.Lat = tempPaci.features[0].attributes.lat;
					//	tempAddress.Long = tempPaci.features[0].attributes.lon;
					//}
					db.Addresses.Add(tempAddress);
					db.SaveChanges();

                    var addressId = db.Addresses.Where(x => x.CustomerId == customerId).FirstOrDefault().Id;
					var createdDate = DateTime.Now;
					Order tempOrder = new Order()
					{
						OrderNo = OrderData.OrderNo,
						CustomerId = customerId,
						DivisionId = OrderData.DivisionId,
						CompanyCodeId = OrderData.CompanyCodeId,
						Notes_ICAgent = OrderData.Notes_ICAgent,
						PriorityId = OrderData.PriorityId,
						ProblemId = OrderData.ProblemId,
						CreatedDate = createdDate,
						StatusId = OrderData.StatusId,
						TypeId = OrderData.OrderTypeId,
                        AddressId = addressId
                    };
					db.Orders.Add(tempOrder);
					db.SaveChanges();
					//	var orderId = db.Orders.Where(x => x.CustomerId == customerId && x.CreatedDate == createdDate).FirstOrDefault();

					AutomationAssignOrderToDispatcher(tempOrder, tempAddress, tempOrder.Id);
					return Json(new { IsCreated = true, Content = "Success" });
				}
				else
				{
					return Json(new { IsCreated = false, Content = "Order No is created before" });
				}
			}
			return Json(new { IsCreated = false, Content = "There is an issue with server now, please try again later" });
		}

		[HttpPost]
		public JsonResult UploadOrderFile(List<OrderViewModel> Orders)
		{
			if (Orders != null)
			{
				for (int i = 0; i < Orders.Count; i++)
				{
					try
					{
						var OrderData = Orders[i];
						DateTime? cDate = null;
						if (OrderData.OrderDateString != null)
						{
							cDate = DateTime.ParseExact(OrderData.OrderDateString, "dd.MM.yyyy HH:mm:ss", null);
						}
						// arabic =	Encoding.GetEncoding(OrderData.AddressNote);
						RowData tempRow = new RowData()
						{
							CustomerNo = OrderData.CustomerNo,
							CustomerName = OrderData.Name,
							PhoneOne = OrderData.PhoneOne,
							PhoneTwo = OrderData.PhoneTwo,
							ContractNo = OrderData.ContractNo,
							ContractType = OrderData.ContractTypeCode,
							ContractTypeDescription = OrderData.ContractTypeName,
							ContractDate = OrderData.CreatedDate,
							ContactExpiraion = OrderData.ExpirationDate,
							FunctionalLocation = OrderData.Functional_Location,
							PACI = OrderData.PACI,
							Governorate = OrderData.GovId,
							AreaCode = OrderData.AreaIdFile,
							AreaDescription = OrderData.AreaDescription,
							Block = OrderData.BlockName,
							House = OrderData.House_Kasima,
							Floor = OrderData.Floor,
							AppartmentNo = OrderData.AppartmentCode,
							AddressNote = OrderData.AddressNote,
							OrderNo = OrderData.OrderNo,
							OrderType = OrderData.OrderTypeCode,
							OrderTypeDescription = OrderData.OrderTypeName,
							CompanyCode = OrderData.CompanyCodeName,
							OrderDate = cDate,
							Division = OrderData.DivisionCode,
							DivisionDescription = OrderData.DivisionName,
							OrderPriority = OrderData.PriorityId,
							OrderPriorityDescription = OrderData.OrderPriorityDescription,
							Problem = OrderData.ProblemCode,
							ProblemDescription = OrderData.ProblemDescription,
							OrderNoteAgent = OrderData.Notes_ICAgent,
							OrderStatus = "Open"
						};

						if (OrderData.StreetId != null)
						{
							tempRow.Street = OrderData.StreetId.ToString();
						}


						db.RowDatas.Add(tempRow);
						db.SaveChanges();

						var isFound = db.Orders.Where(x => x.OrderNo == OrderData.OrderNo).FirstOrDefault();
						if (isFound == null)
						{
							if (OrderData.Name != null && OrderData.PhoneOne != null && OrderData.AreaIdFile != 0)
							{
								Customer tempCustomer = new Customer()
								{
									CustomerNo = OrderData.CustomerNo,
									Name = OrderData.Name,
									PhoneOne = OrderData.PhoneOne,
									PhoneTwo = OrderData.PhoneTwo
								};
								db.Customers.Add(tempCustomer);
								db.SaveChanges();

								var customerId = db.Customers.Where(x => x.PhoneOne == OrderData.PhoneOne && x.Name == OrderData.Name).FirstOrDefault().Id;
								if (OrderData.ContractTypeName != null && OrderData.CreatedDate != null && OrderData.ExpirationDate != null)
								{
									var tCode = OrderData.ContractTypeCode;
									var contractTypeId = db.ContractTypes.Where(x => x.Code == tCode).FirstOrDefault();
									Contract tempContract = new Contract()
									{
										ContractNo = OrderData.ContractNo,
										TypeId = contractTypeId.Id,
										CustomerId = customerId
									};

									if (OrderData.CreatedDate != null)
									{
										string sdate = OrderData.CreatedDate.ToString();
										string[] ssize = sdate.Split(new char[0]);
										if (ssize[0] != "1/1/0001")
										{
											tempContract.CreatedDate = OrderData.CreatedDate;
										}
									}

									if (OrderData.ExpirationDate != null)
									{
										string sdate = OrderData.ExpirationDate.ToString();
										string[] ssize = sdate.Split(new char[0]);

										if (ssize[0] != "1/1/0001")
										{
											tempContract.ExpirationDate = OrderData.ExpirationDate;
										}
									}
									try
									{
										db.Contracts.Add(tempContract);
										db.SaveChanges();
									}
									catch (Exception e)
									{
										Console.WriteLine(e.Message);
									}
								}

								Address tempAddress = new Address()
								{
									AddressNote = OrderData.AddressNote,
									AppartmentNo = OrderData.AppartmentNo,
									BlockId = OrderData.BlockId,
									GovId = OrderData.GovId,
									Functional_Location = OrderData.Functional_Location,
									PACI = OrderData.PACI,
									StreetId = OrderData.StreetId,
									House_Kasima = OrderData.House_Kasima,
									Floor = OrderData.Floor,
									CustomerId = customerId
								};
								if (tempAddress.PACI != null && tempAddress.PACI != "0" && tempAddress.PACI != "null")
								{
									PACIModel tempPaci = GetLocationByPACI(tempAddress.PACI);
									if (tempPaci != null && tempPaci.features != null && tempPaci.features.Count > 0)
									{
										tempAddress.Lat = tempPaci.features[0].attributes.lat;
										tempAddress.Long = tempPaci.features[0].attributes.lon;
									}
								}
								int? areaID;
								if (OrderData.AreaIdFile != null)
								{
									var areaTemp = db.Areas.Where(x => x.OriginalId == OrderData.AreaIdFile).FirstOrDefault();
									if (areaTemp != null)
									{
										areaID = areaTemp.Id;
										tempAddress.AreaId = areaID;
									}
								}
								db.Addresses.Add(tempAddress);
								db.SaveChanges();

								var createdDate = DateTime.Now;
								int? problemId = null;
								if (OrderData.ProblemCode != null)
								{
									var problem = db.Problems.Where(x => x.Code == OrderData.ProblemCode).FirstOrDefault();
									if (problem != null)
									{
										problemId = problem.Id;
									}
								}
								int? orderTypeId = null;
								if (OrderData.OrderTypeCode != null)
								{
									var temp = db.OrderTypes.Where(x => x.Code == OrderData.OrderTypeCode).FirstOrDefault();
									if (temp != null)
									{
										orderTypeId = temp.Id;
									}
								}
								int? companyCodeId = null;
								if (OrderData.CompanyCodeName != null)
								{
									companyCodeId = db.CompanyCodes.Where(x => x.Name == OrderData.CompanyCodeName).FirstOrDefault().Id;
								}
								int? divisionId = null;
								if (OrderData.DivisionName != null)
								{
									divisionId = db.Divisions.Where(x => x.Name == OrderData.DivisionName).FirstOrDefault().Id;
								}

								int? priorityId = null;
								if (OrderData.PriorityId != null)
								{
									var id = Convert.ToInt32(OrderData.PriorityId);
									var temp = db.OrderPriorities.Where(x => x.Id == id).FirstOrDefault();
									if (temp != null)
									{
										priorityId = temp.Id;
									}
								}

								Order tempOrder = new Order()
								{
									OrderNo = OrderData.OrderNo,
									CustomerId = customerId,
									Notes_ICAgent = OrderData.Notes_ICAgent,
									ProblemId = problemId,
									StatusId = 1,
									AddressId = tempAddress.Id
								};
								if (OrderData.OrderDateString != null)
								{
									tempOrder.CreatedDate = cDate;
								}

								if (priorityId != null)
								{
									tempOrder.PriorityId = priorityId;
								}

								if (divisionId != null)
								{
									tempOrder.DivisionId = Convert.ToInt32(divisionId);
								}
								if (companyCodeId != null)
								{
									tempOrder.CompanyCodeId = Convert.ToInt32(companyCodeId);
								}
								if (orderTypeId != null)
								{
									tempOrder.TypeId = Convert.ToInt32(orderTypeId);
								}

								db.Orders.Add(tempOrder);
								db.SaveChanges();

								AutomationAssignOrderToDispatcher(tempOrder, tempAddress, tempOrder.Id);
							}

							//	var orderId = db.Orders.Where(x => x.CustomerId == customerId && x.CreatedDate == createdDate).FirstOrDefault();

							//return Json(new { IsCreated = true, Content = "Success" });
						}
						//else
						//{
						//	return Json(new { IsCreated = false, Content = "Order No is created before" });
						//}
					}
					catch
					{
						return null;
					}
				}

				return Json(new { IsCreated = true, Content = "Success" });
			}
			return Json(new { IsCreated = false, Content = "There is an issue with server now, please try again later" });
		}
		#endregion

		#region paci_methods
		private PACIModel GetLocationByPACI(string PACI)
		{
			try
			{
				string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
	Request.ApplicationPath.TrimEnd('/') + "/";
				var URL = baseUrl + "/proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/Hosted/PACIGeocoder/FeatureServer/0/query?where=civilid+%3D+%27" + PACI + "%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&gdbVersion=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=&resultOffset=&resultRecordCount=&f=pjson";
				WebRequest request = WebRequest.Create(URL);
				request.Timeout = 3000;
				WebResponse response = request.GetResponse();
				Stream dataStream = response.GetResponseStream();
				// Open the stream using a StreamReader for easy access.  
				StreamReader reader = new StreamReader(dataStream);
				// Read the content.  
				string responseFromServer = reader.ReadToEnd();
				JavaScriptSerializer oJS = new JavaScriptSerializer();
				PACIModel oRootObject = new PACIModel();
				oRootObject = oJS.Deserialize<PACIModel>(responseFromServer);

				return oRootObject;
			}
			catch
			{
				return null;
			}
		}
		private List<DropPACI> GetAreas(int? GovNo)
		{
			try
			{
				if (GovNo != null)
				{
					string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
					var URL = baseUrl + "proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/AddressSearch/MapServer/2/query?where=Gov_no+%3D+" + GovNo + "+&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&f=pjson";
					WebRequest request = WebRequest.Create(URL);
					request.Timeout = 3000;
					WebResponse response = request.GetResponse();
					Stream dataStream = response.GetResponseStream();
					// Open the stream using a StreamReader for easy access.  
					StreamReader reader = new StreamReader(dataStream);
					// Read the content.  
					string responseFromServer = reader.ReadToEnd();
					JavaScriptSerializer oJS = new JavaScriptSerializer();
					//AreaPaciModel oRootObject = new AreaPaciModel();
					var oAreas = oJS.Deserialize<AreaPACIResultModel>(responseFromServer);
					List<DropPACI> outAreas = new List<DropPACI>();
					for (int i = 0; i < oAreas.features.Count; i++)
					{
						DropPACI temp = new DropPACI()
						{
							Id = oAreas.features[i].attributes.Nhood_No,
							Name = oAreas.features[i].attributes.NeighborhoodEnglish
						};
						outAreas.Add(temp);
					}
					return outAreas;
				}
				else
				{
					return null;
				}
			}
			catch
			{
				return null;
			}
		}
		private List<DropPACI> GetBlocks(int? NhoodNo)
		{
			if (NhoodNo != null)
			{
				string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
				var URL = baseUrl + "proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/AddressSearch/MapServer/1/query?where=Nhood_No+%3D+" + NhoodNo + "&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&f=pjson";
				WebRequest request = WebRequest.Create(URL);
				request.Timeout = 3000;
				WebResponse response = request.GetResponse();
				Stream dataStream = response.GetResponseStream();
				// Open the stream using a StreamReader for easy access.  
				StreamReader reader = new StreamReader(dataStream);
				// Read the content.  
				string responseFromServer = reader.ReadToEnd();
				JavaScriptSerializer oJS = new JavaScriptSerializer();
				//AreaPaciModel oRootObject = new AreaPaciModel();
				var oAreas = oJS.Deserialize<BlockPACIResultModel>(responseFromServer);
				List<DropPACI> outAreas = new List<DropPACI>();
				for (int i = 0; i < oAreas.features.Count; i++)
				{
					DropPACI temp = new DropPACI()
					{
						Id = oAreas.features[i].attributes.BlockID,
						Name = oAreas.features[i].attributes.BlockEnglish
					};
					outAreas.Add(temp);
				}
				return outAreas;
			}
			return null;
		}
		private List<DropPACI> GetStreets(int? govId, int? areaId, string blockName)
		{
			if (govId != null && areaId != null && blockName != null && blockName != "" && blockName != null)
			{
				string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
				var URL = baseUrl + "proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/AddressSearch/MapServer/0/query?where=%28Gov_no+%3D+" + govId + "+and+Nhood_No+%3D+" + areaId + "+and+BlockEnglish+%3D+%27" + blockName + "%27+%29&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&f=pjson";
				WebRequest request = WebRequest.Create(URL);
				request.Timeout = 3000;
				WebResponse response = request.GetResponse();
				Stream dataStream = response.GetResponseStream();
				// Open the stream using a StreamReader for easy access.  
				StreamReader reader = new StreamReader(dataStream);
				// Read the content.  
				string responseFromServer = reader.ReadToEnd();
				JavaScriptSerializer oJS = new JavaScriptSerializer();
				//AreaPaciModel oRootObject = new AreaPaciModel();
				var oAreas = oJS.Deserialize<StreetPACIResultModel>(responseFromServer);
				List<DropPACI> outAreas = new List<DropPACI>();
				for (int i = 0; i < oAreas.features.Count; i++)
				{
					DropPACI temp = new DropPACI()
					{
						Id = oAreas.features[i].attributes.OBJECTID,
						Name = oAreas.features[i].attributes.StreetEnglish
					};
					outAreas.Add(temp);
				}
				return outAreas;
			}
			return null;
		}
		#endregion

		#region order_actions
		[HttpPost]
		public PartialViewResult EditOrder(int orderId)
		{
			var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();

			if (order != null)
			{
				if (order.AssignedOrders.Count > 0)
				{
					var orderTemp = order.AssignedOrders.FirstOrDefault();
					if (orderTemp.TechnicianId != null && order.StatusId == 1)
					{
						order.StatusId = 2;
						db.SaveChanges();
					}
				}
			}

			var sapData = db.RowDatas.Where(x => x.OrderNo == order.OrderNo).FirstOrDefault();

			var address = db.Addresses.Where(x => x.Id == order.AddressId).FirstOrDefault();
			var contract = db.Contracts.Where(x => x.CustomerId == order.CustomerId).FirstOrDefault();
			var tempOrder = new OrderViewModel();

			int? originalId = null;
			if (address.AreaId > 0)
			{
				originalId = db.Areas.Where(x => x.Id == address.AreaId).FirstOrDefault().OriginalId;
			}

			if (contract == null)
			{
				//DateTime dt = DateTime.ParseExact(, "mm/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
				var createdDate = Convert.ToDateTime(order.CreatedDate).ToString("dd/MMMM/yyyy HH:mm");
				//string createdDate = dt.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

				//string[] createdDate = order.CreatedDate.ToString().Split(new char[0]);
				tempOrder = new OrderViewModel()
				{
					AddressNote = address.AddressNote,
					//Floor = address.Floor,
					//AppartmentNo = address.AppartmentNo,
					Functional_Location = address.Functional_Location,
					//GovId = address.GovId,
					//	House_Kasima = address.House_Kasima,
					//	BlockId = address.BlockId,
					//AreaId = Convert.ToInt32(originalId),
					//PACI = address.PACI,
					//StreetId = address.StreetId,
					OrderNo = order.OrderNo,
					CompanyCodeId = order.CompanyCodeId,
					OrderType = order.OrderType,
					ProblemId = order.ProblemId,
					PriorityId = order.PriorityId,
					StatusId = order.StatusId,
					DivisionId = order.DivisionId,
					Notes_ICAgent = order.Notes_ICAgent,
					Notes_Dispatcher = order.Notes_Dispatcher,
					CustomerNo = order.Customer.CustomerNo,
					Name = order.Customer.Name,
					PhoneOne = order.Customer.PhoneOne,
					PhoneTwo = order.Customer.PhoneTwo,
					StringCreatedDate = createdDate
				};
				if (order.OrderType != null)
				{
					tempOrder.EditOrderTypeId = order.OrderType.Id;
				}
				if (order.OrderPriority != null)
				{
					tempOrder.PriorityId = order.OrderPriority.Id;
				}
			}
			else
			{
				//DateTime dt = DateTime.ParseExact(order.CreatedDate.ToString(), "mm/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

				//string createdDate = dt.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);

				var createdDate = Convert.ToDateTime(order.CreatedDate).ToString("dd/MMMM/yyyy HH:mm");

				tempOrder = new OrderViewModel()
				{
					AddressNote = address.AddressNote,
					//Floor = address.Floor,
					//AppartmentNo = address.AppartmentNo,
					Functional_Location = address.Functional_Location,
					//	GovId = address.GovId,
					//House_Kasima = address.House_Kasima,
					//BlockId = address.BlockId,
					//	AreaId = Convert.ToInt32(originalId),
					//		PACI = address.PACI,
					//	StreetId = address.StreetId,
					OrderNo = order.OrderNo,
					CompanyCodeId = order.CompanyCodeId,
					OrderType = order.OrderType,
					ProblemId = order.ProblemId,
					PriorityId = order.PriorityId,
					StatusId = order.StatusId,
					DivisionId = order.DivisionId,
					Notes_ICAgent = order.Notes_ICAgent,
					Notes_Dispatcher = order.Notes_Dispatcher,
					Name = order.Customer.Name,
					CustomerNo = order.Customer.CustomerNo,
					PhoneOne = order.Customer.PhoneOne,
					PhoneTwo = order.Customer.PhoneTwo,
					ContractTypeId = Convert.ToInt32(contract.TypeId),
					CreatedDateString = Convert.ToDateTime(contract.CreatedDate).ToString("dd/MMMM/yyyy"),
					ExpirationDateString = Convert.ToDateTime(contract.ExpirationDate).ToString("dd/MMMM/yyyy"),
					ContractNo = contract.ContractNo,
					StringCreatedDate = createdDate
				};
				if (order.OrderType != null)
				{
					tempOrder.EditOrderTypeId = order.OrderType.Id;
				}
				if (order.OrderPriority != null)
				{
					tempOrder.PriorityId = order.OrderPriority.Id;
				}
			}

			tempOrder.SAPPACI = sapData.PACI;
			tempOrder.SAPGov = sapData.Governorate;
			tempOrder.SAPArea = sapData.AreaDescription;
			//if (sapData.Block != null)
			//{
			//	tempOrder.SAPBlock = sapData.Block.ToString();
			//}
			tempOrder.SAPBlock = sapData.Block;
			tempOrder.SAPStreet = sapData.Street;
			tempOrder.SAPBuilding = sapData.House;
			tempOrder.SAPFloor = sapData.Floor;
			tempOrder.SAPAppartment = sapData.AppartmentNo;

			if (address.PACI == null)
			{
				if (tempOrder.SAPPACI != null)
				{
					PACIModel tempPaci = GetLocationByPACI(tempOrder.SAPPACI);
					if (tempPaci != null && tempPaci.features != null && tempPaci.features.Count > 0)
					{
						tempOrder.PACI = tempPaci.features[0].attributes.civilid;
						tempOrder.GovName = tempPaci.features[0].attributes.governorateenglish;
						tempOrder.AreaDescription = tempPaci.features[0].attributes.neighborhoodenglish;
						tempOrder.BlockName = tempPaci.features[0].attributes.blockenglish;
						tempOrder.StreetName = tempPaci.features[0].attributes.streetenglish;
						tempOrder.AppartmentNo = tempPaci.features[0].attributes.houseenglish;
						tempOrder.Floor = tempPaci.features[0].attributes.floor_no;
						tempOrder.House_Kasima = tempPaci.features[0].attributes.houseenglish;
						ViewBag.GovName = tempOrder.GovName;
						ViewBag.AreaDescription = tempOrder.AreaDescription;
						ViewBag.AreaDescriptionId = tempPaci.features[0].attributes.neighborhoodid;
						ViewBag.BlockName = tempOrder.BlockName;
						ViewBag.StreetName = tempOrder.StreetName;
					}
					else
					{
						tempOrder.PACI = address.PACI;
						tempOrder.GovId = address.GovId;
						//if (tempOrder.GovId == null)
						//{

						//}


						if (address.Area != null && address.Area.OriginalId != null)
						{
							tempOrder.AreaId = Convert.ToInt32(address.Area.OriginalId);
						}
						//else
						//{

						//}

						tempOrder.BlockId = address.BlockId;
						var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
						if (block != null)
						{
							tempOrder.BlockName = block.Name;
						}
						//else
						//{

						//}
						tempOrder.StreetId = address.StreetId;

						tempOrder.AppartmentNo = address.AppartmentNo;
						tempOrder.Floor = address.Floor;
						tempOrder.House_Kasima = address.House_Kasima;
						tempOrder.Lat = address.Lat;
						tempOrder.Long = address.Long;
					}
				}
				else
				{
					tempOrder.PACI = address.PACI;
					tempOrder.GovId = address.GovId;
					if (address.Area != null && address.Area.OriginalId != null)
					{
						tempOrder.AreaId = Convert.ToInt32(address.Area.OriginalId);
					}
					tempOrder.BlockId = address.BlockId;
					var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
					if (block != null)
					{
						tempOrder.BlockName = block.Name;
					}
					tempOrder.StreetId = address.StreetId;
					tempOrder.AppartmentNo = address.AppartmentNo;
					tempOrder.Floor = address.Floor;
					tempOrder.House_Kasima = address.House_Kasima;
					tempOrder.Lat = address.Lat;
					tempOrder.Long = address.Long;

					List<SelectListItem> areaList = new List<SelectListItem>();
					var tArea = db.Areas.Where(x => x.OriginalId == tempOrder.AreaId).FirstOrDefault();
					if (tArea != null)
					{
						areaList.Add(new SelectListItem()
						{
							Value = tArea.Id.ToString(),
							Text = tArea.Name,
							Selected = true
						});
						ViewBag.AreaId = areaList;
					}


					List<SelectListItem> blockList = new List<SelectListItem>();
					var tBlock = db.Blocks.Where(x => x.Id == tempOrder.BlockId).FirstOrDefault();
					if (tBlock != null)
					{
						blockList.Add(new SelectListItem()
						{
							Value = tBlock.Id.ToString(),
							Text = tBlock.Name,
							Selected = true
						});
						ViewBag.BlockId = blockList;
					}

					List<SelectListItem> streetList = new List<SelectListItem>();
					var tStreet = db.Street_Jaddah.Where(x => x.Id == tempOrder.StreetId).FirstOrDefault();
					if (tStreet != null)
					{
						streetList.Add(new SelectListItem()
						{
							Value = tStreet.Id.ToString(),
							Text = tStreet.Name,
							Selected = true
						});
						ViewBag.StreetId = streetList;
					}
				}
			}
			else
			{
				//PACIModel tempPaci = GetLocationByPACI(address.PACI);
				//if (tempPaci != null && tempPaci.features != null && tempPaci.features.Count > 0)
				//{
				//	tempOrder.PACI = tempPaci.features[0].attributes.civilid;
				//	tempOrder.GovName = tempPaci.features[0].attributes.governorateenglish;
				//	tempOrder.AreaDescription = tempPaci.features[0].attributes.neighborhoodenglish;
				//	tempOrder.StreetName = tempPaci.features[0].attributes.streetenglish;
				//	tempOrder.AppartmentNo = tempPaci.features[0].attributes.houseenglish;
				//	tempOrder.Floor = tempPaci.features[0].attributes.floor_no;
				//	tempOrder.House_Kasima = tempPaci.features[0].attributes.houseenglish;
				//	ViewBag.GovName = tempOrder.GovName;
				//	ViewBag.AreaDescription = tempOrder.AreaDescription;
				//	ViewBag.AreaDescriptionId = tempPaci.features[0].attributes.neighborhoodid;
				//	ViewBag.BlockName = tempOrder.BlockName;
				//	ViewBag.StreetName = tempOrder.StreetName;
				//}

				tempOrder.PACI = address.PACI;
				tempOrder.GovId = address.GovId;
				if (address.Area != null && address.Area.OriginalId != null)
				{
					tempOrder.AreaId = Convert.ToInt32(address.Area.OriginalId);
				}
				tempOrder.BlockId = address.BlockId;
				var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
				if (block != null)
				{
					tempOrder.BlockName = block.Name;
				}
				tempOrder.StreetId = address.StreetId;
				tempOrder.AppartmentNo = address.AppartmentNo;
				tempOrder.Floor = address.Floor;
				tempOrder.House_Kasima = address.House_Kasima;
				tempOrder.Lat = address.Lat;
				tempOrder.Long = address.Long;
			}

			if (tempOrder.GovId == 7)
			{
				ViewBag.GovId = new SelectList(db.Governorates.Where(x => x.Id == 7), "Id", "Name", tempOrder.GovId);
			}
			else
			{
				if (tempOrder.GovId != null)
				{
					ViewBag.GovId = new SelectList(db.Governorates.Where(x => x.Id != 7), "Id", "Name", tempOrder.GovId);
				}
				else
				{
					ViewBag.GovId = new SelectList(db.Governorates.Where(x => x.Id != 7), "Id", "Name");
				}
			}

			try
			{
				if (tempOrder.AreaId != 0)
				{
					var tArea = db.Areas.Where(x => x.OriginalId == tempOrder.AreaId).FirstOrDefault();
					var areas = new List<DropPACI>();
					if (tempOrder.GovId != 7)
					{
						areas = GetAreas(tempOrder.GovId);
					}
					List<SelectListItem> areaList = new List<SelectListItem>();
					if (tArea == null)
					{
						areaList.Add(new SelectListItem()
						{
							Value = "",
							Text = "-- Select One --",
							Selected = true
						});
					}
					if (areas != null && areas.Count > 0)
					{
						for (int i = 0; i < areas.Count; i++)
						{
							if (tArea != null)
							{
								areaList.Add(new SelectListItem()
								{
									Value = areas[i].Id.ToString(),
									Text = areas[i].Name,
									Selected = (areas[i].Name == tArea.Name ? true : false)
								});
							}
							else
							{
								areaList.Add(new SelectListItem()
								{
									Value = areas[i].Id.ToString(),
									Text = areas[i].Name
								});
							}
						}
						ViewBag.AreaId = areaList;
					}
				}
				else
				{
					List<SelectListItem> areaList = new List<SelectListItem>();
					if (tempOrder.GovId != null && tempOrder.GovId != 0)
					{
						var areas = GetAreas(tempOrder.GovId);
						areaList.Add(new SelectListItem()
						{
							Value = "",
							Text = "-- Select One --",
							Selected = true
						});
						for (int i = 0; i < areas.Count; i++)
						{
							areaList.Add(new SelectListItem()
							{
								Value = areas[i].Id.ToString(),
								Text = areas[i].Name
							});
						}
					}
					else
					{
						areaList.Add(new SelectListItem()
						{
							Value = "999999999",
							Text = "Not Found In PACI",
							Selected = true
						});
					}
					ViewBag.AreaId = areaList;
				}


				if (tempOrder.BlockId > 0 && !string.IsNullOrEmpty(tempOrder.BlockName))
				{
					var tempBlocks = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
					List<SelectListItem> blockList = new List<SelectListItem>();
					if (tempBlocks.Name == "ByPass PACI down")
					{
						blockList.Add(new SelectListItem()
						{
							Value = "999999999",
							Text = tempBlocks.Name,
							Selected = true
						});
					}
					else
					{
						int blockNo = Convert.ToInt32(tempBlocks.BlockNo);
						blockList.Add(new SelectListItem()
						{
							Value = tempBlocks.BlockNo,
							Text = tempBlocks.Name,
							Selected = true
						});
					}

					var blocks = GetBlocks(tempOrder.AreaId);

					if (blocks != null && blocks.Count > 0)
					{
						if (tempBlocks == null)
						{
							blockList.Add(new SelectListItem()
							{
								Value = "",
								Text = "-- Select One --",
								Selected = true
							});
						}
						for (int i = 0; i < blocks.Count; i++)
						{
							blockList.Add(new SelectListItem()
							{
								Value = blocks[i].Id.ToString(),
								Text = blocks[i].Name
							});
						}
					}
					ViewBag.BlockId = blockList;
				}
				else
				{
					var blocks = GetBlocks(tempOrder.AreaId);
					List<SelectListItem> blockList = new List<SelectListItem>();

					if (blocks != null && blocks.Count > 0)
					{

						blockList.Add(new SelectListItem()
						{
							Value = "",
							Text = "-- Select One --",
							Selected = true
						});

						for (int i = 0; i < blocks.Count; i++)
						{
							blockList.Add(new SelectListItem()
							{
								Value = blocks[i].Id.ToString(),
								Text = blocks[i].Name
							});
						}
						ViewBag.BlockId = blockList;
					}
					else
					{
						blockList.Add(new SelectListItem()
						{
							Value = "999999999",
							Text = "Not Found In PACI",
							Selected = true
						});
						ViewBag.BlockId = blockList;
					}
				}

				if (tempOrder.StreetId != null)
				{
					var street = db.Street_Jaddah.Where(x => x.Id == tempOrder.StreetId).ToList();
					if (street.Count() > 0)
					{
						if (street[0].Name != "Not Found In PACI")
						{
							var streets = GetStreets(tempOrder.GovId, tempOrder.AreaId, tempOrder.BlockName);

							if (streets != null && streets.Count > 0)
							{
								List<SelectListItem> streetsList = new List<SelectListItem>();
								for (int i = 0; i < streets.Count; i++)
								{
									if (street.Count > 0)
									{
										var tStreet = streets.Where(x => x.Name == street.FirstOrDefault().Name).FirstOrDefault().Id;
										streetsList.Add(new SelectListItem()
										{
											Value = streets[i].Id.ToString(),
											Text = streets[i].Name,
											Selected = (streets[i].Id == tStreet ? true : false)
										});
									}
									else
									{
										streetsList.Add(new SelectListItem()
										{
											Value = streets[i].Id.ToString(),
											Text = streets[i].Name
										});
									}
								}
								ViewBag.StreetId = streetsList;
							}
							else
							{
								List<SelectListItem> streetsList = new List<SelectListItem>();
								streetsList.Add(new SelectListItem()
								{
									Value = "",
									Text = "-- Select One --"
								});
								ViewBag.StreetId = streetsList;
							}
						}
						else
						{
							List<SelectListItem> streetsList = new List<SelectListItem>();
							streetsList.Add(new SelectListItem()
							{
								Value = tempOrder.StreetId.ToString(),
								Text = street[0].Name,
								Selected = true
							});
							ViewBag.StreetId = streetsList;
						}
					}
					else
					{
						List<SelectListItem> streetsList = new List<SelectListItem>();
						streetsList.Add(new SelectListItem()
						{
							Value = "999999999",
							Text = "Not Found In PACI",
							Selected = true
						});
						ViewBag.StreetId = streetsList;
					}
				}
				else
				{
					var streets = GetStreets(tempOrder.GovId, tempOrder.AreaId, tempOrder.BlockName);
					List<SelectListItem> streetsList = new List<SelectListItem>();
					if (streets != null && streets.Count > 0)
					{
						streetsList.Add(new SelectListItem()
						{
							Value = "",
							Text = "-- Select One --",
							Selected = true
						});
						for (int i = 0; i < streets.Count; i++)
						{
							streetsList.Add(new SelectListItem()
							{
								Value = streets[i].Id.ToString(),
								Text = streets[i].Name
							});
						}
						ViewBag.StreetId = streetsList;
					}
					else
					{
						streetsList.Add(new SelectListItem()
						{
							Value = "999999999",
							Text = "Not Found In PACI",
							Selected = true
						});
						ViewBag.StreetId = streetsList;
					}
				}
			}
			catch (Exception ex)
			{
				if (tempOrder.AreaId != 0)
				{
					List<SelectListItem> areaList = new List<SelectListItem>();
					var tArea = db.Areas.Where(x => x.OriginalId == tempOrder.AreaId).FirstOrDefault();
					areaList.Add(new SelectListItem()
					{
						Value = tArea.Id.ToString(),
						Text = tArea.Name,
						Selected = true
					});
					ViewBag.AreaId = areaList;
				}
				else
				{
					List<SelectListItem> areaList = new List<SelectListItem>();
					areaList.Add(new SelectListItem()
					{
						Value = "111111111",
						Text = "ByPass PACI down",
						Selected = true
					});
					ViewBag.AreaId = areaList;
				}

				if (tempOrder.BlockId > 0 && !string.IsNullOrEmpty(tempOrder.BlockName))
				{
					var tempBlocks = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
					if (tempBlocks != null)
					{
						List<SelectListItem> blockList = new List<SelectListItem>();
						blockList.Add(new SelectListItem()
						{
							Value = tempBlocks.Id.ToString(),
							Text = tempBlocks.Name,
							Selected = true
						});
						ViewBag.BlockId = blockList;
					}
				}
				else
				{
					List<SelectListItem> blockList = new List<SelectListItem>();
					blockList.Add(new SelectListItem()
					{
						Value = "111111111",
						Text = "ByPass PACI down",
						Selected = true
					});
					ViewBag.BlockId = blockList;
				}

				if (tempOrder.StreetId != null)
				{
					List<SelectListItem> streetsList = new List<SelectListItem>();

					var tStreet = db.Street_Jaddah.Where(x => x.Id == tempOrder.StreetId).FirstOrDefault();
					streetsList.Add(new SelectListItem()
					{
						Value = tStreet.Id.ToString(),
						Text = tStreet.Name,
						Selected = true
					});
					ViewBag.StreetId = streetsList;
				}
				else
				{
					List<SelectListItem> streetsList = new List<SelectListItem>();
					streetsList.Add(new SelectListItem()
					{
						Value = "111111111",
						Text = "ByPass PACI down",
						Selected = true
					});
					ViewBag.StreetId = streetsList;
				}

				List<SelectListItem> gocList = new List<SelectListItem>();
				gocList.Add(new SelectListItem()
				{
					Value = "7",
					Text = "ByPass PACI down",
					Selected = true
				});
				ViewBag.GovId = gocList;
			}
			ViewBag.CompanyCodeId = new SelectList(db.CompanyCodes, "Id", "Name", tempOrder.CompanyCodeId);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", tempOrder.DivisionId);
			ViewBag.PriorityId = new SelectList(db.OrderPriorities, "Id", "Name", tempOrder.PriorityId);
			ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", tempOrder.StatusId);
			ViewBag.EditOrderTypeId = new SelectList(db.OrderTypes, "Id", "Name", tempOrder.EditOrderTypeId);
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", tempOrder.ProblemId);
			ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", tempOrder.CustomerId);

			ViewBag.ContractTypeId = new SelectList(db.ContractTypes, "Id", "Name", tempOrder.ContractTypeId);
			ViewBag.CancellationReason = new SelectList(db.Cancellation_Reasons, "Id", "Name");
			ViewBag.OrderId = orderId;

			if ((tempOrder.Lat == null && tempOrder.Long == null) || (tempOrder.Lat == 0 && tempOrder.Long == 0))
			{
				ViewBag.isLocation = false;
			}
			else
			{
				ViewBag.isLocation = true;
			}

			return PartialView("OrderEditPartial", tempOrder);
		}

		[HttpPost]
		public JsonResult ConfirmEditOrder(OrderViewModel OrderData)
		{
			if (OrderData != null)
			{
				var order = db.Orders.Where(x => x.OrderNo == OrderData.OrderNo).FirstOrDefault();
				order.TypeId = OrderData.OrderTypeId;
				order.PriorityId = OrderData.PriorityId;
				order.ProblemId = OrderData.ProblemId;
				order.StatusId = OrderData.StatusId;
				order.Notes_Dispatcher = OrderData.Notes_Dispatcher;
				order.Cancellation_Reason = OrderData.Cancellation_Reason;
				db.SaveChanges();

				var tempOrderStatus = db.OrderStatus.Where(x => x.Id == OrderData.StatusId).FirstOrDefault().Name;
				var tempRow = db.RowDatas.Where(x => x.OrderNo == order.OrderNo).FirstOrDefault();
				tempRow.OrderStatus = tempOrderStatus;
				db.SaveChanges();


				if (OrderData.StatusId == 5 || OrderData.StatusId == 4)
				{
					var assigened = db.AssignedOrders.Where(x => x.OrderId == order.Id).FirstOrDefault();
					assigened.IsClosed = true;
					db.SaveChanges();
				}

				var customer = db.Customers.Where(x => x.Id == order.CustomerId).FirstOrDefault();
				customer.PhoneTwo = OrderData.PhoneTwo;
				db.SaveChanges();

				var address = db.Addresses.Where(x => x.Id == order.AddressId).FirstOrDefault();
				//int? areaId = null;
				if (OrderData.AreaId > 0)
				{
					var id = OrderData.AreaId;
					var areaTemp = db.Areas.Where(x => x.OriginalId == id).FirstOrDefault();
					if (areaTemp != null)
					{
						address.AreaId = areaTemp.Id;
					}
					else
					{
						Area temp = new Area()
						{
							OriginalId = OrderData.AreaId,
							Name = OrderData.AreaDescription
						};
						db.Areas.Add(temp);
						db.SaveChanges();
						address.AreaId = temp.Id;
					}
				}
				if (OrderData.GovId != null)
				{
					var id = OrderData.GovId;
					var govTemp = db.Governorates.Where(x => x.Id == id).FirstOrDefault();
					if (govTemp != null)
					{
						address.GovId = govTemp.Id;
					}
					else
					{
						Governorate temp = new Governorate()
						{
							Name = OrderData.GovName
						};
						db.Governorates.Add(temp);
						db.SaveChanges();
						address.GovId = temp.Id;
					}
				}

				if (OrderData.BlockId > 0)
				{
					var id = OrderData.BlockId;
					var blockTemp = db.Blocks.Where(x => x.Name == id.ToString()).FirstOrDefault();
					if (blockTemp != null)
					{
						address.BlockId = blockTemp.Id;
					}
					else
					{
						Block temp = new Block()
						{
							Name = OrderData.BlockName,
							BlockNo = OrderData.BlockName
						};
						db.Blocks.Add(temp);
						db.SaveChanges();
						address.BlockId = temp.Id;
					}
				}

				if (OrderData.StreetId != null)
				{
					var name = OrderData.StreetName;
					var streetTemp = db.Street_Jaddah.Where(x => x.Name == name).FirstOrDefault();
					if (streetTemp != null)
					{
						address.StreetId = streetTemp.Id;
					}
					else
					{
						Street_Jaddah temp = new Street_Jaddah()
						{
							Name = OrderData.StreetName
						};
						db.Street_Jaddah.Add(temp);
						db.SaveChanges();
						address.StreetId = temp.Id;
					}
				}
				if (OrderData.Lat != 0 && OrderData.Long != 0)
				{
					address.Lat = OrderData.Lat;
					address.Long = OrderData.Long;
				}

				address.PACI = OrderData.PACI;
				address.House_Kasima = OrderData.House_Kasima;
				address.Floor = OrderData.Floor;
				address.AppartmentNo = OrderData.AppartmentNo;
				address.AddressNote = OrderData.AddressNote;
				db.SaveChanges();

				return Json(new { IsCreated = true, Content = "Success" });
			}
			return Json(new { IsCreated = false, Content = "Error" });
		}

		[HttpPost]
		public bool AddWorkUpdate(int orderId, int tripNo, string workNotes, int workUpdate, int workStatus)
		{
			var orderAssign = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();

			var tech = "";
			if (orderAssign.TechnicianId != null)
			{
				tech = db.Technicians.Where(x => x.Id == orderAssign.TechnicianId && x.IsDelete != true).FirstOrDefault().Name;
			}

			var workUName = db.Work_Update.Where(x => x.Id == workUpdate).FirstOrDefault();

			OrderWorkUpdate temp = new OrderWorkUpdate()
			{
				TechnicianName = tech,
				DispatcherName = user.UserName,
				OrderId = orderId,
				TripNo = tripNo,
				WorkUpdateId = workUpdate,
				Notes = workNotes,
				WorkUpdateName = workUName.Name,
				StatusName = workUName.OrderStatu.Name,
				CreatedDate = DateTime.Now
			};
			db.OrderWorkUpdates.Add(temp);
			db.SaveChanges();

			var tempOrder = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
			if (tempOrder.StatusId != 4 || tempOrder.StatusId != 5)
			{
				tempOrder.StatusId = workStatus;
				db.SaveChanges();
			}

			if (workStatus == 5 || workStatus == 4)
			{
				var assigened = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
				assigened.IsClosed = true;
				assigened.ActionDate = DateTime.Now;
				db.SaveChanges();
			}

			return true;
		}

		[HttpPost]
		public JsonResult GetHistoryWorkUpdate(int orderId)
		{
			var orderWU = db.OrderWorkUpdates.Where(x => x.OrderId == orderId).ToList();
			if (orderWU != null)
			{
				List<OrderWUModel> result = new List<OrderWUModel>();
				for (int i = 0; i < orderWU.Count; i++)
				{
					OrderWUModel temp = new OrderWUModel()
					{
						CreatedDate = orderWU[i].CreatedDate,
						Notes = orderWU[i].Notes,
						TripNo = orderWU[i].TripNo,
						TechnicianName = orderWU[i].TechnicianName,
						DispatcherName = orderWU[i].DispatcherName,
						Status = orderWU[i].StatusName,
						Update = orderWU[i].WorkUpdateName
					};
					int? id = orderWU[i].WorkUpdateId;
					temp.WorkUpdateId = db.Work_Update.Where(x => x.Id == id).FirstOrDefault().Name;
					result.Add(temp);
				}
				JsonSerializerSettings settings = new JsonSerializerSettings();
				settings.ContractResolver = new CustomResolver();
				settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
				settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
				settings.Formatting = Formatting.Indented;

				// Do the serialization and output to the console
				string json = JsonConvert.SerializeObject(result, settings);

				return Json(json, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		[HttpPost]
		public JsonResult GetActionData(int orderId)
		{

			var ass = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();


			ActionDataModel temp = new ActionDataModel();
			if (ass.ActionId != null)
			{
				if (ass.ActionId == 1)
				{
					temp.ActionName = "No Action";
				}
				else if (ass.ActionId == 2)
				{
					temp.ActionName = "Received";
				}
				else if (ass.ActionId == 3)
				{
					temp.ActionName = "Started";
				}
				else if (ass.ActionId == 4)
				{
					temp.ActionName = "Finished";
				}
				temp.ActionDate = ass.ActionDate.ToString();
				temp.Latitude = Convert.ToDecimal(ass.Lat);
				temp.Longitude = Convert.ToDecimal(ass.Long);
			}

			var json = JsonConvert.SerializeObject(temp, Formatting.Indented,
					new JsonSerializerSettings
					{
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore
					});
			return Json(json, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region printout
		[HttpPost]
		public PartialViewResult PrintOut()
		{
			PrintOutViewModel tempPrintOut = new PrintOutViewModel();
			var Formans = db.Formen.Where(x => x.DispatcherId == user.Id).ToList();
			var formanIds = Formans.Select(x => x.Id);
			tempPrintOut.Technicians = db.Technicians.Where(x => formanIds.Contains(x.FormanId) && x.IsDelete != true).ToList();
			tempPrintOut.Statuses = db.OrderStatus.ToList();
			tempPrintOut.Formans = Formans;

			return PartialView("PrintOutPartial", tempPrintOut);
		}

		[HttpPost]
		public JsonResult GetOrdersPrintOut(List<int> statusIds, List<int> formanIds, List<PrintTechModel> techIds, bool includeUnassign, bool includeInprogress, string orderNo)
		{
			try
			{


				List<Forman> formans = new List<Forman>();
				List<AssignedOrder> assigendOrders = new List<AssignedOrder>();
				List<PrintViewModel> PrintData = new List<PrintViewModel>();
				if (formanIds != null && statusIds != null)
				{
					for (int i = 0; i < formanIds.Count; i++)
					{
						if (techIds != null && techIds[i].techIds != null)
						{
							for (int k = 0; k < techIds[i].techIds.Count; k++)
							{
								int techId = techIds[i].techIds[k];
								var technician = db.Technicians.Where(x => x.Id == techId && x.IsDelete != true).FirstOrDefault();
								var techOrders = technician.AssignedOrders.ToList();
								foreach (var assign in techOrders)
								{
									PrintViewModel tempPrintData = new PrintViewModel();
									var order = db.Orders.Where(x => x.Id == assign.OrderId).FirstOrDefault();
									for (int j = 0; j < statusIds.Count; j++)
									{
										if (order.StatusId == statusIds[j])
										{
											tempPrintData.AssigendTechnician = technician.Name;
											tempPrintData.FormanName = technician.Forman.Name;
											string DispatcherID = technician.DispatcherId;
											tempPrintData.DispatcherName = db.AspNetUsers.Where(x => x.Id == DispatcherID).FirstOrDefault().UserName;
											var address = order.Address;
											var contract = order.Customer.Contracts.FirstOrDefault();
											tempPrintData.OrderId = order.Id;
											tempPrintData.OrderNo = order.OrderNo;
											tempPrintData.OrderType = order.OrderType.Name;
											tempPrintData.CreationDate = (DateTime)order.CreatedDate;
											tempPrintData.CustomerName = order.Customer.Name;
											tempPrintData.CustomerTel = order.Customer.PhoneOne;
											tempPrintData.AddressNote = address.AddressNote;
											tempPrintData.PACI = address.PACI;
											if (address.Governorate != null)
											{
												tempPrintData.Gov = address.Governorate.Name;
											}
											if (address.StreetId != null)
											{
												var street = db.Street_Jaddah.Where(x => x.Id == address.StreetId).FirstOrDefault();
												if (street != null)
												{
													tempPrintData.Street = street.Name;
												}
											}
											if (address.BlockId != null)
											{
												var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
												if (block != null)
												{
													tempPrintData.block = block.Name;
												}
											}

											tempPrintData.Area = address.Area.Name;
											tempPrintData.House_Kasima = address.House_Kasima;
											tempPrintData.AppartmentNo = address.AppartmentNo;
											tempPrintData.Floor = address.Floor;
											tempPrintData.Functional_Location = address.Functional_Location;
											if (contract != null)
											{
												tempPrintData.ContractCode = contract.ContractType.Code;
												tempPrintData.ContractType = contract.ContractType.Name;
												tempPrintData.ExpirationDate = contract.ExpirationDate;
											}
											tempPrintData.CompanyCode = order.CompanyCode.Name;
										}
									}

									if (tempPrintData.OrderNo != null)
									{
										PrintData.Add(tempPrintData);
									}
								}

							}
						}
						else
						{
							int formanId = formanIds[i];
							var forman = db.Formen.Where(x => x.Id == formanId).FirstOrDefault();
							assigendOrders = forman.AssignedOrders.ToList();
							foreach (var assign in assigendOrders)
							{
								PrintViewModel tempPrintData = new PrintViewModel();
								var order = db.Orders.Where(x => x.Id == assign.OrderId).FirstOrDefault();
								for (int j = 0; j < statusIds.Count; j++)
								{
									if (order.StatusId == statusIds[j])
									{
										string technicianName = "";
										if (assign.TechnicianId != null)
										{
											technicianName = db.Technicians.Where(x => x.Id == assign.TechnicianId && x.IsDelete != true).FirstOrDefault().Name;
										}
										tempPrintData.AssigendTechnician = technicianName;
										tempPrintData.FormanName = forman.Name;
										string DispatcherID = forman.DispatcherId;
										tempPrintData.DispatcherName = db.AspNetUsers.Where(x => x.Id == DispatcherID).FirstOrDefault().UserName;
										var address = order.Address;
										var contract = order.Customer.Contracts.FirstOrDefault();
										tempPrintData.OrderId = order.Id;
										tempPrintData.OrderNo = order.OrderNo;
										tempPrintData.OrderType = order.OrderType.Name;
										tempPrintData.CreationDate = (DateTime)order.CreatedDate;
										tempPrintData.CustomerName = order.Customer.Name;
										tempPrintData.CustomerTel = order.Customer.PhoneOne;
										tempPrintData.AddressNote = address.AddressNote;
										tempPrintData.PACI = address.PACI;
										if (address.Governorate != null)
										{
											tempPrintData.Gov = address.Governorate.Name;
										}
										if (address.StreetId != null)
										{
											var street = db.Street_Jaddah.Where(x => x.Id == address.StreetId).FirstOrDefault();
											if (street != null)
											{
												tempPrintData.Street = street.Name;
											}
										}
										if (address.BlockId != null)
										{
											var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
											if (block != null)
											{
												tempPrintData.block = block.Name;
											}
										}

										tempPrintData.Area = address.Area.Name;
										tempPrintData.House_Kasima = address.House_Kasima;
										tempPrintData.AppartmentNo = address.AppartmentNo;
										tempPrintData.Floor = address.Floor;
										tempPrintData.Functional_Location = address.Functional_Location;
										if (contract != null)
										{
											tempPrintData.ContractCode = contract.ContractType.Code;
											tempPrintData.ContractType = contract.ContractType.Name;
											tempPrintData.ExpirationDate = contract.ExpirationDate;
										}
										tempPrintData.CompanyCode = order.CompanyCode.Name;
									}
								}

								if (tempPrintData.OrderNo != null)
								{
									PrintData.Add(tempPrintData);
								}
							}
						}
					}
				}

				if (includeUnassign == true)
				{
					var orders = db.Orders.Where(x => x.StatusId == 3).ToList();
					List<int?> orderIds = new List<int?>();
					for (int i = 0; i < orders.Count; i++)
					{
						orderIds.Add(orders[i].Id);
					}
					assigendOrders = db.AssignedOrders.Where(x => x.IsClosed == false && x.FormanId == null && x.TechnicianId == null && x.DispatcherId == user.Id && !orderIds.Contains(x.OrderId)).ToList();
					foreach (var assign in assigendOrders)
					{
						PrintViewModel tempPrintData = new PrintViewModel();
						var order = db.Orders.Where(x => x.Id == assign.OrderId).FirstOrDefault();
						string DispatcherID = user.Id;
						tempPrintData.DispatcherName = db.AspNetUsers.Where(x => x.Id == DispatcherID).FirstOrDefault().UserName;
						var address = order.Address;
						var contract = order.Customer.Contracts.FirstOrDefault();
						tempPrintData.OrderId = order.Id;
						tempPrintData.OrderNo = order.OrderNo;
						tempPrintData.CreationDate = (DateTime)order.CreatedDate;
						tempPrintData.CustomerName = order.Customer.Name;
						tempPrintData.CustomerTel = order.Customer.PhoneOne;
						tempPrintData.AddressNote = address.AddressNote;
						tempPrintData.PACI = address.PACI;

						if (order.OrderType != null)
						{
							tempPrintData.OrderType = order.OrderType.Name;
						}

						if (address.Governorate != null)
						{
							tempPrintData.Gov = address.Governorate.Name;
						}
						if (address.StreetId != null)
						{
							var street = db.Street_Jaddah.Where(x => x.Id == address.StreetId).FirstOrDefault();
							if (street != null)
							{
								tempPrintData.Street = street.Name;
							}
						}
						if (address.BlockId != null)
						{
							var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
							if (block != null)
							{
								tempPrintData.block = block.Name;
							}
						}
						if (address.Area != null)
						{
							tempPrintData.Area = address.Area.Name;
						}
						tempPrintData.AppartmentNo = address.AppartmentNo;
						tempPrintData.House_Kasima = address.House_Kasima;
						tempPrintData.Floor = address.Floor;
						tempPrintData.Functional_Location = address.Functional_Location;
						if (contract != null)
						{
							tempPrintData.ContractCode = contract.ContractType.Code;
							tempPrintData.ContractType = contract.ContractType.Name;
							tempPrintData.ExpirationDate = contract.ExpirationDate;
						}
						tempPrintData.CompanyCode = order.CompanyCode.Name;

						PrintData.Add(tempPrintData);
					}
				}

				if (includeInprogress == true)
				{

					var orders = db.Orders.Where(x => x.StatusId == 3).ToList();
					List<int?> orderIds = new List<int?>();
					for (int i = 0; i < orders.Count; i++)
					{
						orderIds.Add(orders[i].Id);
					}
					if (orderIds.Count > 0)
					{

						assigendOrders = db.AssignedOrders.Where(x => x.IsClosed == false && x.FormanId == null && x.TechnicianId == null && x.DispatcherId == user.Id && orderIds.Contains(x.OrderId)).ToList();
						foreach (var assign in assigendOrders)
						{
							PrintViewModel tempPrintData = new PrintViewModel();
							var order = db.Orders.Where(x => x.Id == assign.OrderId).FirstOrDefault();
							string DispatcherID = user.Id;
							tempPrintData.DispatcherName = db.AspNetUsers.Where(x => x.Id == DispatcherID).FirstOrDefault().UserName;
							var address = order.Address;
							var contract = order.Customer.Contracts.FirstOrDefault();
							tempPrintData.OrderId = order.Id;
							tempPrintData.OrderNo = order.OrderNo;
							tempPrintData.CreationDate = (DateTime)order.CreatedDate;
							tempPrintData.CustomerName = order.Customer.Name;
							tempPrintData.CustomerTel = order.Customer.PhoneOne;
							tempPrintData.AddressNote = address.AddressNote;
							tempPrintData.PACI = address.PACI;

							if (order.OrderType != null)
							{
								tempPrintData.OrderType = order.OrderType.Name;
							}

							if (address.Governorate != null)
							{
								tempPrintData.Gov = address.Governorate.Name;
							}
							if (address.StreetId != null)
							{
								var street = db.Street_Jaddah.Where(x => x.Id == address.StreetId).FirstOrDefault();
								if (street != null)
								{
									tempPrintData.Street = street.Name;
								}
							}
							if (address.BlockId != null)
							{
								var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
								if (block != null)
								{
									tempPrintData.block = block.Name;
								}
							}
							if (address.Area != null)
							{
								tempPrintData.Area = address.Area.Name;
							}
							tempPrintData.AppartmentNo = address.AppartmentNo;
							tempPrintData.House_Kasima = address.House_Kasima;
							tempPrintData.Floor = address.Floor;
							tempPrintData.Functional_Location = address.Functional_Location;
							if (contract != null)
							{
								tempPrintData.ContractCode = contract.ContractType.Code;
								tempPrintData.ContractType = contract.ContractType.Name;
								tempPrintData.ExpirationDate = contract.ExpirationDate;
							}
							tempPrintData.CompanyCode = order.CompanyCode.Name;

							PrintData.Add(tempPrintData);
						}
					}
				}

				if (!string.IsNullOrEmpty(orderNo))
				{

					var orders = db.Orders.Where(x => x.OrderNo == orderNo).ToList();
					List<int?> orderIds = new List<int?>();
					for (int i = 0; i < orders.Count; i++)
					{
						orderIds.Add(orders[i].Id);
					}
					if (orderIds.Count > 0)
					{

						assigendOrders = db.AssignedOrders.Where(x => x.IsClosed == false && x.FormanId == null && x.TechnicianId == null && x.DispatcherId == user.Id && orderIds.Contains(x.OrderId)).ToList();
						foreach (var assign in assigendOrders)
						{
							PrintViewModel tempPrintData = new PrintViewModel();
							var order = db.Orders.Where(x => x.Id == assign.OrderId).FirstOrDefault();
							string DispatcherID = user.Id;
							tempPrintData.DispatcherName = db.AspNetUsers.Where(x => x.Id == DispatcherID).FirstOrDefault().UserName;
							var address = order.Address;
							var contract = order.Customer.Contracts.FirstOrDefault();
							tempPrintData.OrderId = order.Id;
							tempPrintData.OrderNo = order.OrderNo;
							tempPrintData.CreationDate = (DateTime)order.CreatedDate;
							tempPrintData.CustomerName = order.Customer.Name;
							tempPrintData.CustomerTel = order.Customer.PhoneOne;
							tempPrintData.AddressNote = address.AddressNote;
							tempPrintData.PACI = address.PACI;

							if (order.OrderType != null)
							{
								tempPrintData.OrderType = order.OrderType.Name;
							}

							if (address.Governorate != null)
							{
								tempPrintData.Gov = address.Governorate.Name;
							}
							if (address.StreetId != null)
							{
								var street = db.Street_Jaddah.Where(x => x.Id == address.StreetId).FirstOrDefault();
								if (street != null)
								{
									tempPrintData.Street = street.Name;
								}
							}
							if (address.BlockId != null)
							{
								var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
								if (block != null)
								{
									tempPrintData.block = block.Name;
								}
							}
							if (address.Area != null)
							{
								tempPrintData.Area = address.Area.Name;
							}
							tempPrintData.AppartmentNo = address.AppartmentNo;
							tempPrintData.House_Kasima = address.House_Kasima;
							tempPrintData.Floor = address.Floor;
							tempPrintData.Functional_Location = address.Functional_Location;
							if (contract != null)
							{
								tempPrintData.ContractCode = contract.ContractType.Code;
								tempPrintData.ContractType = contract.ContractType.Name;
								tempPrintData.ExpirationDate = contract.ExpirationDate;
							}
							tempPrintData.CompanyCode = order.CompanyCode.Name;

							PrintData.Add(tempPrintData);
						}
					}
				}

				for (int i = 0; i < PrintData.Count; i++)
				{
					var id = PrintData[i].OrderId;
					var tempWork = db.OrderWorkUpdates.Where(x => x.OrderId == id).ToList();
					if (tempWork != null && tempWork.Count > 0)
					{
						var workupdate = tempWork.LastOrDefault();
						PrintData[i].StatusUpdate = workupdate.StatusName + " - " + workupdate.WorkUpdateName;
						PrintData[i].TripTech = workupdate.TechnicianName;
						PrintData[i].TripDisp = workupdate.DispatcherName;
						PrintData[i].TripNote = workupdate.Notes;
					}
					else
					{
						var tempAssigner = db.AssignedOrders.Where(x => x.OrderId == id);
						if (tempAssigner != null)
						{
							var techName = tempAssigner.FirstOrDefault();
							if (techName.Technician != null)
							{
								PrintData[i].TripTech = techName.Technician.Name;
							}
						}
					}
				}


				JavaScriptSerializer json = new JavaScriptSerializer();
				json.MaxJsonLength = int.MaxValue;
				var data = json.Serialize(PrintData);

				return Json(data, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region order_assign
		[HttpPost]
		public JsonResult GetAssignedOrder(string orderNo)
		{
			var json = "";

			var order = db.Orders.Where(x => x.OrderNo == orderNo).FirstOrDefault();

			if (order != null)
			{
				var ass = db.AssignedOrders.Where(x => x.OrderId == order.Id).FirstOrDefault();
				if (ass != null)
				{
					if (ass.IsClosed == true)
					{
						json = JsonConvert.SerializeObject("is closed");
					}
					else if (ass.TechnicianId != null)
					{
						json = JsonConvert.SerializeObject(ass.Technician.Name);
					}
					else
					{
						json = JsonConvert.SerializeObject("not assigned");
					}
					return Json(json, JsonRequestBehavior.AllowGet);
				}
			}

			json = JsonConvert.SerializeObject("not found");
			return Json(json, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetTechnicianOrder(int techId)
		{
			var json = "";
			if (techId != 0)
			{
				var assOrders = db.AssignedOrders.Where(x => x.TechnicianId == techId && x.IsClosed == false).Select(x => x.OrderId).ToList();

				var orders = db.Orders.Where(x => assOrders.Contains(x.Id)).ToList();

				var techName = db.Technicians.Where(x => x.Id == techId && x.IsDelete != true).FirstOrDefault().Name;

				if (orders != null && orders.Count > 0)
				{
					var result = new List<BulkModel>();
					for (int i = 0; i < orders.Count; i++)
					{
						var temp = new BulkModel()
						{
							OrderNo = orders[i].OrderNo,
							TechName = techName
						};
						result.Add(temp);
					}
					json = JsonConvert.SerializeObject(result);
					return Json(json, JsonRequestBehavior.AllowGet);
				}

				json = JsonConvert.SerializeObject("not found");
				return Json(json, JsonRequestBehavior.AllowGet);
			}

			json = JsonConvert.SerializeObject("not found");
			return Json(json, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public bool SetAsUnAssigned(int orderId)
		{
			if (orderId > 0)
			{
				var tempAssign = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();

				var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
				OrderNotificationModel notifyThis = new OrderNotificationModel()
				{
					Id = order.Id,
					CreatedDate = order.CreatedDate.ToString(),
					CustomerName = order.Customer.Name,
					CustomerPhone = order.Customer.PhoneOne,
					OrderNo = order.OrderNo,
					Status = order.OrderStatu.Name,
					Problem = order.Problem.Name,
					Area = order.Address.Area.Name,
					PACI = order.Address.PACI,
					Lat = order.Address.Lat,
					Long = order.Address.Long
				};

				//var deviceID = db.Devices.Where(x => x.TechId == tempAssign.TechnicianId).FirstOrDefault();
				//if (deviceID != null)
				//{
				//	var body = string.Format("Order No. {0} is unassigned", order.OrderNo);
				//	FireBase.SendPushNotification(deviceID.DeviceId, "Order Unassigned", body, notifyThis);
				//}

				tempAssign.FormanId = null;
				tempAssign.TechnicianId = null;
				db.SaveChanges();
				if (order.StatusId == 2)
				{
					order.StatusId = 1;
					db.SaveChanges();
					var tempRow = db.RowDatas.Where(x => x.OrderNo == order.OrderNo).FirstOrDefault();
					tempRow.OrderStatus = "Open";
					db.SaveChanges();
				}
				else
				{
					var orderStatus = db.OrderStatus.Where(x => x.Id == order.StatusId).FirstOrDefault().Name;
					var tempRow = db.RowDatas.Where(x => x.OrderNo == order.OrderNo).FirstOrDefault();
					tempRow.OrderStatus = orderStatus;
					db.SaveChanges();
				}
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool AutomationAssignOrderToDispatcher(Order createdOrder, Address address, int orderId)
		{
			List<DispatcherData> finalMatchedDispatchers = new List<DispatcherData>();

			if (address.AreaId != null)
			{
				if (createdOrder.ProblemId != null)
				{
					finalMatchedDispatchers = db.DispatcherDatas.Where(x => x.AreaId == address.AreaId && x.ProblemId == createdOrder.ProblemId && x.DivisionId == createdOrder.DivisionId).ToList();
				}
			}

			var selectedDispatcher = "";
			if (finalMatchedDispatchers.Count > 0)
			{
				if (finalMatchedDispatchers.Count == 1)
				{
					selectedDispatcher = finalMatchedDispatchers[0].DispatcherId;
				}
				else
				{
					var random = new Random();
					int index = random.Next(finalMatchedDispatchers.Count);
					selectedDispatcher = finalMatchedDispatchers[index].DispatcherId;
				}

				var superId = db.SuperDispatchers.Where(x => x.DispatcherId == selectedDispatcher).FirstOrDefault().SuperId;
				AssignedOrder tempDO = new AssignedOrder()
				{
					SupervisorId = superId,
					DispatcherId = selectedDispatcher,
					OrderId = orderId,
					IsClosed = false
				};
				db.AssignedOrders.Add(tempDO);
				db.SaveChanges();
			}
			else
			{
				if (createdOrder.DivisionId != null)
				{

					var random = new Random();

					var supersByDivision = db.SuperDispatchers.Where(x => x.DivisionId == createdOrder.DivisionId).ToList();

					int index = random.Next(supersByDivision.Count);

					var selectedSuper = supersByDivision[index].SuperId;

					AssignedOrder temp = new AssignedOrder()
					{
						SupervisorId = selectedSuper,
						OrderId = orderId,
						IsClosed = false
					};
					db.AssignedOrders.Add(temp);
					db.SaveChanges();
				}
			}
			return true;
		}

		[HttpPost]
		public JsonResult AssignTechnicianToOrder(int technincianId, int orderId)
		{
			if (technincianId > 0 && orderId > 0)
			{
				var techData = db.Technicians.Where(x => x.Id == technincianId && x.IsDelete != true).FirstOrDefault();
				var tempAssign = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
				tempAssign.FormanId = techData.FormanId;
				tempAssign.TechnicianId = technincianId;
				tempAssign.DispatcherId = techData.DispatcherId;
				db.SaveChanges();

				var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
				if (order.StatusId == 1)
				{
					order.StatusId = 2;
					db.SaveChanges();
					var tempRow = db.RowDatas.Where(x => x.OrderNo == order.OrderNo).FirstOrDefault();
					tempRow.OrderStatus = "Dispatched";
					db.SaveChanges();
				}

				var customer = order.Customer;

				var address = db.Addresses.Where(x => x.Id == order.AddressId).FirstOrDefault();
				string block = "";
				if (address.BlockId != null)
				{
					var tempblock = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
					if (tempblock != null)
					{
						block = tempblock.Name;
					}
					//block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault().Name;
				}

				OrderBoardViewModel orderDetail = new OrderBoardViewModel()
				{
					Id = order.Id,
					CreatedDate = order.CreatedDate,
					DateFrom = tempAssign.DateFrom,
					DateTo = tempAssign.DateTo,
					OrderNo = order.OrderNo,
					CustomerNo = customer.CustomerNo,
					CustomerName = customer.Name,
					CustomerPhone = customer.PhoneOne,
					Block = block
				};

				if (address.Area != null)
				{
					orderDetail.Area = address.Area.Name;
				}

				if (order.Problem != null)
				{
					orderDetail.Problem = order.Problem.Name;
					orderDetail.ProblemId = order.ProblemId;
				}
				if (order.OrderType != null)
				{
					orderDetail.Type = order.OrderType.Name;
					orderDetail.TypeId = order.TypeId;
				}


				//OrderNotificationModel notifyThis = new OrderNotificationModel()
				//{
				//	Id = order.Id,
				//	CreatedDate = order.CreatedDate.ToString(),
				//	CustomerName = order.Customer.Name,
				//	CustomerPhone = order.Customer.PhoneOne,
				//	OrderNo = order.OrderNo,
				//	Status = order.OrderStatu.Name,
				//	Problem = order.Problem.Name,
				//	Area = order.Address.Area.Name,
				//	PACI = order.Address.PACI,
				//	Lat = order.Address.Lat,
				//	Long = order.Address.Long
				//};



				//var deviceID = db.Devices.Where(x => x.TechId == tempAssign.TechnicianId).FirstOrDefault();
				//if (deviceID != null)
				//{
				//	var body = string.Format("Order No. {0} assigned to you", order.OrderNo);
				//	FireBase.SendPushNotification(deviceID.DeviceId, "Order Assigned", body, notifyThis);
				//}

				var json = JsonConvert.SerializeObject(orderDetail, Formatting.Indented,
				new JsonSerializerSettings
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore
				});
				return Json(json, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return null;
			}
		}

		public JsonResult GetUnAssignedAjax()
		{
			//var unAssignedOrders = db.AssignedOrders.Where(x => x.DispatcherId == user.Id && x.FormanId == null && x.IsClosed == false).ToList();

			//List<OrderBoardViewModel> unAssignedOrderForBoard = new List<OrderBoardViewModel>();
			//foreach (var order in unAssignedOrders)
			//{
			//	var type = db.Orders.Where(x => x.Id == order.OrderId).FirstOrDefault().OrderType;
			//	var address = order.Order.Address;
			//	var customer = db.Customers.Where(x => x.Id == order.Order.CustomerId).FirstOrDefault();

			//	OrderBoardViewModel tempOrder = new OrderBoardViewModel()
			//	{
			//		OrderNo = order.Order.OrderNo,
			//		CreatedDate = order.Order.CreatedDate,
			//		StatusId = order.Order.StatusId,
			//		DateTo = order.DateTo,
			//		DateFrom = order.DateFrom,
			//		Id = order.Order.Id,
			//		CustomerNo = customer.CustomerNo,
			//		CustomerName = customer.Name,
			//		CustomerPhone = customer.PhoneOne
			//	};

			//	if (address.PACI != null)
			//	{
			//		tempOrder.PACI = address.PACI;
			//	}

			//	if (type != null)
			//	{
			//		tempOrder.Type = type.Name;
			//		tempOrder.TypeId = type.Id;
			//	}
			//	if (order.Order.Problem != null)
			//	{
			//		tempOrder.Problem = order.Order.Problem.Name;
			//	}
			//	if (order.Order.ProblemId != null)
			//	{
			//		tempOrder.ProblemId = order.Order.ProblemId;
			//	}

			//	if (address.Area != null)
			//	{
			//		tempOrder.Area = address.Area.Name;
			//	}

			//	if (address.Governorate != null)
			//	{
			//		tempOrder.Governorate = address.Governorate.Name;
			//	}
			//	if (address.StreetId != null)
			//	{
			//		tempOrder.Street = address.StreetId.ToString();
			//	}
			//	if (address.BlockId != null)
			//	{
			//		var block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
			//		if (block != null)
			//		{
			//			tempOrder.Block = block.Name;
			//		}
			//		else
			//		{
			//			tempOrder.Block = null;
			//		}
			//	}
			//	unAssignedOrderForBoard.Add(tempOrder);
			//}
			List<OrderBoardViewModel> unAssignedOrderForBoard = new List<OrderBoardViewModel>();
			var unAssignedOrders = db.dispatcherUnAssigned(user.Id).ToList();
			for (int i = 0; i < unAssignedOrders.Count; i++)
			{
				OrderBoardViewModel tempOrder = new OrderBoardViewModel()
				{
					OrderNo = unAssignedOrders[i].OrderNo,
					CreatedDate = unAssignedOrders[i].CreatedDate,
					StatusId = unAssignedOrders[i].StatusId,
					DateTo = unAssignedOrders[i].DateTo,
					DateFrom = unAssignedOrders[i].DateFrom,
					Id = unAssignedOrders[i].orderId,
					CustomerNo = unAssignedOrders[i].CustomerNo,
					CustomerName = unAssignedOrders[i].CustomerName,
					CustomerPhone = unAssignedOrders[i].PhoneOne,
					Block = unAssignedOrders[i].Block,
					PACI = unAssignedOrders[i].PACI,
					Type = unAssignedOrders[i].Type,
					TypeId = unAssignedOrders[i].TypeId,
					Problem = unAssignedOrders[i].Problem,
					ProblemId = unAssignedOrders[i].ProblemId,
					Area = unAssignedOrders[i].Area,
					Street = unAssignedOrders[i].StreetId.ToString(),
					Governorate = unAssignedOrders[i].GovName
				};
				unAssignedOrderForBoard.Add(tempOrder);
			}

			var outJson = JsonConvert.SerializeObject(unAssignedOrderForBoard, Formatting.Indented,
					new JsonSerializerSettings
					{
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore
					});
			return Json(outJson, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public bool TransferOrder(string dispatcherId, int orderId)
		{
			var order = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
			order.DispatcherId = dispatcherId;
			db.SaveChanges();
			return true;
		}

		[HttpPost]
		public bool DispatcherMassReassign(int TechnicianId, List<int> Orders)
		{
			if (TechnicianId != 0 && Orders.Count > 0)
			{
				for (int i = 0; i < Orders.Count; i++)
				{
					AssignTechnicianToOrder(TechnicianId, Orders[i]);
				}
				return true;
			}
			return false;
		}

		[HttpPost]
		public bool TransferFromDispatcher(int techId, int action, string option)
		{
			var orders = db.AssignedOrders.Where(x => x.TechnicianId == techId && x.IsClosed == false).ToList();

			if (action == 0)
			{
				for (int i = 0; i < orders.Count; i++)
				{
					orders[i].TechnicianId = null;
					orders[i].FormanId = null;
					db.SaveChanges();
				}
			}
			else if (action == 1)
			{
				var targetTech = Convert.ToInt32(option);
				var formanID = db.Technicians.Where(x => x.Id == targetTech && x.IsDelete != true).FirstOrDefault().FormanId;

				for (int i = 0; i < orders.Count; i++)
				{
					orders[i].FormanId = formanID;
					orders[i].TechnicianId = targetTech;
					db.SaveChanges();
				}
			}
			else if (action == 2)
			{
				var superID = db.SuperDispatchers.Where(x => x.DispatcherId == option).FirstOrDefault().SuperId;
				for (int i = 0; i < orders.Count; i++)
				{
					orders[i].DispatcherId = option;
					orders[i].SupervisorId = superID;
					orders[i].TechnicianId = null;
					orders[i].FormanId = null;
					db.SaveChanges();
				}
			}
			return true;
		}

		[HttpPost]
		public bool BulkAssign(string ordersFrom, string FromTechnician, string orderTo, string orderToTechnician, string orderToDispatcher)
		{
			var orders = new List<AssignedOrder>();
			if (ordersFrom == "all")
			{
				orders = db.AssignedOrders.Where(x => x.DispatcherId == user.Id && x.IsClosed == false).ToList();
			}
			else if (ordersFrom == "un")
			{
				orders = db.AssignedOrders.Where(x => x.DispatcherId == user.Id && x.FormanId == null && x.TechnicianId == null && x.IsClosed == false).ToList();
			}
			else if (ordersFrom == "tech")
			{
				int technicianId = Convert.ToInt32(FromTechnician);
				orders = db.AssignedOrders.Where(x => x.DispatcherId == user.Id && x.TechnicianId == technicianId && x.IsClosed == false).ToList();
			}
			if (orderTo == "un")
			{
				for (int i = 0; i < orders.Count; i++)
				{
					orders[i].FormanId = null;
					orders[i].TechnicianId = null;
					db.SaveChanges();
				}
			}
			else if (orderTo == "tech")
			{
				int technicianId = Convert.ToInt32(orderToTechnician);
				var techData = db.Technicians.Where(x => x.Id == technicianId && x.IsDelete != true).FirstOrDefault();
				var formanID = techData.FormanId;
				if (techData.AvailabilityId == 1)
				{
					for (int i = 0; i < orders.Count; i++)
					{
						var orderid = orders[i].OrderId;
						var order = db.Orders.Where(x => x.Id == orderid).FirstOrDefault();

						var address = db.Addresses.Where(x => x.Id == order.AddressId).FirstOrDefault();
						if (address.PACI != null || (address.AreaId != null && address.GovId != null && address.StreetId != null && address.BlockId != null))
						{
							orders[i].FormanId = formanID;
							orders[i].TechnicianId = technicianId;
							db.SaveChanges();
						}
					}
				}
				else
				{
					return false;
				}
			}
			else if (orderTo == "disp")
			{
				var SuperId = db.SuperDispatchers.Where(x => x.DispatcherId == orderToDispatcher).FirstOrDefault().SuperId;
				for (int i = 0; i < orders.Count; i++)
				{
					orders[i].SupervisorId = SuperId;
					orders[i].DispatcherId = orderToDispatcher;
					orders[i].TechnicianId = null;
					orders[i].FormanId = null;
					db.SaveChanges();
				}
			}
			return true;
		}

		[HttpPost]
		public bool BulkAssignNo(List<string> orderList, string orderTo, string orderToTechnician, string orderToDispatcher)
		{
			if (orderList.Count > 0)
			{
				var orders = new List<AssignedOrder>();
				var ids = new List<int>();
				for (int i = 0; i < orderList.Count; i++)
				{
					var orderNo = orderList[i];
					var temporder = db.Orders.Where(x => x.OrderNo == orderNo).FirstOrDefault();
					if (temporder != null)
					{
						var tempass = db.AssignedOrders.Where(x => x.OrderId == temporder.Id).FirstOrDefault();
						if (tempass != null)
						{
							orders.Add(tempass);
						}
					}

				}
				if (orderTo == "un")
				{
					for (int i = 0; i < orders.Count; i++)
					{
						orders[i].FormanId = null;
						orders[i].TechnicianId = null;
						db.SaveChanges();
					}
				}
				else if (orderTo == "tech")
				{
					int technicianId = Convert.ToInt32(orderToTechnician);
					var techData = db.Technicians.Where(x => x.Id == technicianId && x.IsDelete != true).FirstOrDefault();
					var formanID = techData.FormanId;
					if (techData.AvailabilityId == 1)
					{
						for (int i = 0; i < orders.Count; i++)
						{
							var orderid = orders[i].OrderId;
							var order = db.Orders.Where(x => x.Id == orderid).FirstOrDefault();

							var address = db.Addresses.Where(x => x.Id == order.AddressId).FirstOrDefault();
							if (address.PACI != null || (address.AreaId != null && address.GovId != null && address.StreetId != null && address.BlockId != null))
							{
								orders[i].FormanId = formanID;
								orders[i].TechnicianId = technicianId;
								db.SaveChanges();
							}
						}
					}
					else
					{
						return false;
					}
				}
				else if (orderTo == "disp")
				{
					var SuperId = db.SuperDispatchers.Where(x => x.DispatcherId == orderToDispatcher).FirstOrDefault().SuperId;
					for (int i = 0; i < orders.Count; i++)
					{
						orders[i].SupervisorId = SuperId;
						orders[i].DispatcherId = orderToDispatcher;
						orders[i].TechnicianId = null;
						orders[i].FormanId = null;
						db.SaveChanges();
					}
				}
				return true;
			}
			return false;
		}

		[HttpGet]
		public JsonResult GetAssignedOrders()
		{
			var formans = db.Formen.Where(x => x.DispatcherId == user.Id).ToList();
			var technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();

			List<FormanBoardViewModel> outFormans = new List<FormanBoardViewModel>();
			foreach (var item in formans)
			{
				var tempTechnicians = technicians.Where(x => x.FormanId == item.Id).ToList();
				List<TechnicianBoardViewModel> tech = new List<TechnicianBoardViewModel>();
				foreach (var techItem in tempTechnicians)
				{
					List<OrderBoardViewModel> orders = new List<OrderBoardViewModel>();
					var tempout = db.getAssignedOrdersForTechs(techItem.Id.ToString()).ToList();
					for (int i = 0; i < tempout.Count; i++)
					{
						OrderBoardViewModel orderDetail = new OrderBoardViewModel()
						{
							Id = tempout[i].orderId,
							CreatedDate = tempout[i].CreatedDate,
							DateFrom = tempout[i].DateFrom,
							DateTo = tempout[i].DateTo,
							OrderNo = tempout[i].OrderNo,
							CustomerName = tempout[i].CustomerName,
							Area = tempout[i].Area,
							Block = tempout[i].Block,
							Problem = tempout[i].Problem,
							ProblemId = tempout[i].ProblemId,
							Type = tempout[i].Type,
							TypeId = tempout[i].TypeId
						};
						orders.Add(orderDetail);
					}
					//foreach (var order in techItem.AssignedOrders)
					//{
					//	var tempOrder = db.Orders.Where(x => x.Id == order.OrderId).FirstOrDefault();
					//	var orderDates = db.AssignedOrders.Where(x => x.OrderId == order.OrderId && x.IsClosed == false).FirstOrDefault();
					//	var customer = db.Customers.Where(x => x.Id == order.Order.CustomerId).FirstOrDefault();
					//	var address = db.Addresses.Where(x => x.Id == order.Order.AddressId).FirstOrDefault();
					//	string block = "";
					//	if (address.BlockId != null)
					//	{
					//		var tempBlock = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault();
					//		if (tempBlock != null)
					//		{
					//			block = tempBlock.Name;
					//		}
					//	}
					//	if (orderDates != null)
					//	{
					//		OrderBoardViewModel orderDetail = new OrderBoardViewModel()
					//		{
					//			Id = tempOrder.Id,
					//			CreatedDate = tempOrder.CreatedDate,
					//			DateFrom = orderDates.DateFrom,
					//			DateTo = orderDates.DateTo,
					//			OrderNo = tempOrder.OrderNo,
					//			CustomerName = customer.Name,
					//			Area = address.Area.Name,
					//			Block = block
					//		};
					//		//if (orderDates.ActionId == 1)
					//		//{
					//		//	orderDetail.ActionName = "No Action";
					//		//}
					//		//else if (orderDates.ActionId == 2)
					//		//{
					//		//	orderDetail.ActionName = "Received";
					//		//}
					//		//else if (orderDates.ActionId == 3)
					//		//{
					//		//	orderDetail.ActionName = "Started";
					//		//}
					//		//else if (orderDates.ActionId == 4)
					//		//{
					//		//	orderDetail.ActionName = "Finished";
					//		//}
					//		//else
					//		//{
					//		//	orderDetail.ActionName = "No Action";
					//		//}

					//		if (tempOrder.Problem != null)
					//		{
					//			orderDetail.Problem = tempOrder.Problem.Name;
					//			orderDetail.ProblemId = tempOrder.ProblemId;
					//		}
					//		if (tempOrder.OrderType != null)
					//		{
					//			orderDetail.Type = tempOrder.OrderType.Name;
					//			orderDetail.TypeId = tempOrder.TypeId;
					//		}

					//		orders.Add(orderDetail);
					//	}

					//}

					int counts = 0;
					var compeletedOrders = db.AssignedOrders.Where(x => x.TechnicianId == techItem.Id && x.IsClosed == true).ToList();
					for (int i = 0; i < compeletedOrders.Count(); i++)
					{
						if (compeletedOrders[i].ActionDate != null)
						{
							if (DateTime.Parse(compeletedOrders[i].ActionDate.ToString()).Date == DateTime.Now.Date)
							{
								counts++;
							}
						}
					}
					TechnicianBoardViewModel tempTech = new TechnicianBoardViewModel()
					{
						Id = techItem.Id,
						Name = techItem.Name,
						OrderCounts = techItem.AssignedOrders.Count,
						Orders = orders,
						CompeletedCount = counts
					};
					var availabilityObj = db.TV_Availability.Where(x => x.Id == techItem.AvailabilityId).FirstOrDefault();
					if (availabilityObj != null)
					{
						tempTech.Availability = availabilityObj.Name;
					}
					tech.Add(tempTech);
				}

				FormanBoardViewModel temp = new FormanBoardViewModel()
				{
					AllOrderCount = tempTechnicians.Count,
					Name = item.Name,
					Id = item.Id,
					Technicians = tech
				};
				outFormans.Add(temp);
			}

			var json = JsonConvert.SerializeObject(outFormans, Formatting.Indented,
				new JsonSerializerSettings
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore
				});
			return Json(json, JsonRequestBehavior.AllowGet);
		}

		public ActionResult BulkAssign()
		{
			var technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();
			ViewBag.Technicians = technicians;
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			List<DisparcheViewModel> dispatcherModel = new List<DisparcheViewModel>();
			foreach (var item in dispatchers)
			{
				DisparcheViewModel temp = new DisparcheViewModel()
				{
					id = item.Id,
					Name = item.UserName
				};
				dispatcherModel.Add(temp);
			}
			ViewBag.Dispatchers = dispatcherModel;
			var availability = db.TV_Availability.ToList();
			ViewBag.Availability = availability;

			return View();
		}
		#endregion

		#region time_visit
		[HttpPost]
		public JsonResult GetTimeVisit(int orderId)
		{
			var orderWU = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();

			JsonSerializerSettings settings = new JsonSerializerSettings();
			settings.ContractResolver = new CustomResolver2();
			settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
			settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
			settings.Formatting = Formatting.Indented;

			// Do the serialization and output to the console
			string json = JsonConvert.SerializeObject(orderWU, settings);

			return Json(json, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public bool SetVisitTime(int orderId, DateTime dateFrom, DateTime dateTo)
		{
			if (orderId > 0 && dateFrom != null && dateTo != null)
			{
				var tempAssign = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
				tempAssign.DateFrom = dateFrom;
				tempAssign.DateTo = dateTo;
				db.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}
		#endregion

		#region notification
		public JsonResult GetNotification()
		{
			List<OrderBoardViewModel> notifiedOrder = new List<OrderBoardViewModel>();
			var period = Convert.ToInt32(ConfigurationManager.AppSettings["NotificationPeriod"]);
			var ComparedDate = DateTime.Now.AddHours(period);
			var orders = db.AssignedOrders.Where(x => x.DispatcherId == user.Id && x.IsClosed == false).ToList();

			foreach (var order in orders)
			{
				if (order.DateFrom != null)
				{
					var DateFromMinus = Convert.ToDateTime(order.DateFrom).AddHours(-period);
					//TimeSpan diff = (TimeSpan)(ComparedDate - order.DateFrom);
					//double hours = diff.TotalHours;


					if ((DateFromMinus < DateTime.Now) && (Convert.ToDateTime(order.DateFrom) > DateTime.Now))
					{
						var tempOrder = db.Orders.Where(x => x.Id == order.OrderId).FirstOrDefault();
						OrderBoardViewModel orderDetail = new OrderBoardViewModel()
						{
							Id = tempOrder.Id,
							OrderNo = tempOrder.OrderNo,
							CreatedDate = tempOrder.CreatedDate,
							Problem = tempOrder.Problem.Name,
							ProblemId = tempOrder.ProblemId,
							Type = tempOrder.OrderType.Name,
							TypeId = tempOrder.TypeId,
							DateFrom = order.DateFrom,
							DateTo = order.DateTo
						};
						notifiedOrder.Add(orderDetail);
					}
				}
			}
			var outJson = JsonConvert.SerializeObject(notifiedOrder, Formatting.Indented,
					new JsonSerializerSettings
					{
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore
					});
			return Json(outJson, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region change_availability
		[HttpPost]
		public bool ChangeAvailability(int technicianId, int availabilityId)
		{
			var technician = db.Technicians.Where(x => x.Id == technicianId && x.IsDelete != true).FirstOrDefault();
			technician.AvailabilityId = availabilityId;
			db.SaveChanges();
			return true;
		}
		#endregion

		#region report
		public ActionResult Reports()
		{
			ViewBag.Technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();
			ViewBag.Dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.Status = db.OrderStatus.ToList();
			ViewBag.Problems = db.Problems.ToList();
			ViewBag.Types = db.OrderTypes.ToList();
			ViewBag.Divisions = db.Divisions.ToList();
			ViewBag.CompanyCodes = db.CompanyCodes.ToList();
			return View();
		}

		[HttpPost]
		public JsonResult Reports(List<string> status, List<string> problems, List<string> technicians, List<string> dispatchers, List<string> types, List<string> divisions, List<string> company, DateTime dateFrom, DateTime dateTo, bool isTrip)
		{
			var nw = DateTime.Now;
			var userValues = db.ReportFillters.Where(x => x.UserId == user.Id);
			db.ReportFillters.RemoveRange(userValues);
			db.SaveChanges();

			if (status != null && status.Count > 0)
			{
				for (int i = 0; i < status.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Status",
						UserId = user.Id,
						value = status[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempStatus = db.OrderStatus.ToList();
				for (int i = 0; i < tempStatus.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Status",
						UserId = user.Id,
						value = tempStatus[i].Name
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (problems != null && problems.Count > 0)
			{
				for (int i = 0; i < problems.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Problem",
						UserId = user.Id,
						value = problems[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempProblems = db.Problems.ToList();
				for (int i = 0; i < tempProblems.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Problem",
						UserId = user.Id,
						value = tempProblems[i].Name
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}


			if (technicians != null && technicians.Count > 0)
			{
				for (int i = 0; i < technicians.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Technician",
						UserId = user.Id,
						value = technicians[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				//var tempTechnicians = db.Technicians.Where(x => x.DispatcherId == user.Id).ToList();
				//for (int i = 0; i < tempTechnicians.Count; i++)
				//{
				//	ReportFillter rf = new ReportFillter()
				//	{
				//		CreatedDate = nw,
				//		Type = "Technician",
				//		UserId = user.Id,
				//		value = tempTechnicians[i].Name
				//	};
				//	db.ReportFillters.Add(rf);
				//	db.SaveChanges();
				//}
			}

			if (dispatchers != null && dispatchers.Count > 0)
			{
				for (int i = 0; i < dispatchers.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Dispatcher",
						UserId = user.Id,
						value = dispatchers[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempDispatchers = db.AspNetUsers.ToList();
				for (int i = 0; i < tempDispatchers.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Dispatcher",
						UserId = user.Id,
						value = tempDispatchers[i].UserName
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}


			if (types != null && types.Count > 0)
			{
				for (int i = 0; i < types.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Type",
						UserId = user.Id,
						value = types[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempTypes = db.OrderTypes.ToList();
				for (int i = 0; i < tempTypes.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Type",
						UserId = user.Id,
						value = tempTypes[i].Name
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (divisions != null && divisions.Count > 0)
			{
				for (int i = 0; i < divisions.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Division",
						UserId = user.Id,
						value = divisions[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempDivisions = db.Divisions.ToList();
				for (int i = 0; i < tempDivisions.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Division",
						UserId = user.Id,
						value = tempDivisions[i].Name
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}

			if (company != null && company.Count > 0)
			{
				for (int i = 0; i < company.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Company",
						UserId = user.Id,
						value = company[i]
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}
			else
			{
				var tempCompany = db.CompanyCodes.ToList();
				for (int i = 0; i < tempCompany.Count; i++)
				{
					ReportFillter rf = new ReportFillter()
					{
						CreatedDate = nw,
						Type = "Company",
						UserId = user.Id,
						value = tempCompany[i].Name
					};
					db.ReportFillters.Add(rf);
					db.SaveChanges();
				}
			}

			ReportFillter rfDateTo = new ReportFillter()
			{
				CreatedDate = nw,
				Type = "DateTo",
				UserId = user.Id,
				value = dateTo.ToString()
			};
			db.ReportFillters.Add(rfDateTo);
			db.SaveChanges();
			ReportFillter rfDateFrom = new ReportFillter()
			{
				CreatedDate = nw,
				Type = "DateFrom",
				UserId = user.Id,
				value = dateFrom.ToString()
			};
			db.ReportFillters.Add(rfDateFrom);
			db.SaveChanges();

			if (isTrip == true)
			{
				ReportFillter rf = new ReportFillter()
				{
					CreatedDate = nw,
					Type = "IsTrip",
					UserId = user.Id,
					value = isTrip.ToString()
				};
				db.ReportFillters.Add(rf);
				db.SaveChanges();
			}
			else
			{
				ReportFillter rf = new ReportFillter()
				{
					CreatedDate = nw,
					Type = "IsTrip",
					UserId = user.Id,
					value = "False"
				};
				db.ReportFillters.Add(rf);
				db.SaveChanges();
			}

			PrintModel outOrders = GetReportForPage(1, isTrip);

			return Json(outOrders, JsonRequestBehavior.AllowGet);

		}

		private PrintModel GetReportForPage(int pageNo, bool isTrip)
		{
			int startNo = (pageNo * 10) - 10;
			int endNo = startNo + 10;
			PrintModel outer = new PrintModel();
			List<PrintViewModel> outOrders = new List<PrintViewModel>();
			if (isTrip == true)
			{
				ObjectResult<int?> ReportCount = db.ReportCountTrip(user.Id);
				var count = ReportCount.ToList().FirstOrDefault();
				if (count != null || count != 0)
				{
					outer.TotalCount = count;
				}
				else
				{
					outer.TotalCount = 0;
				}
				List<GenerateReportTripPaging_Result> orders = db.GenerateReportTripPaging(user.Id, startNo, endNo).ToList();

				for (int i = 0; i < orders.Count; i++)
				{
					PrintViewModel temp = new PrintViewModel();
					temp.DivisionName = orders[i].DivisionName;
					temp.CompanyCode = orders[i].CompanyCode;
					temp.OrderNo = orders[i].OrderNo;
					temp.OrderCreationDate = orders[i].OrderCreationDate.ToString();
					temp.OrderType = orders[i].OrderType;
					temp.Problem = orders[i].Problem;
					temp.Status = orders[i].Status;
					temp.CustomerName = orders[i].CustomerName;
					temp.CustomerTel = orders[i].CustomerTel;
					temp.PACI = orders[i].PACI;
					temp.Area = orders[i].Area;
					temp.DispatcherName = orders[i].DispatcherName;
					temp.SuperName = orders[i].SuperName;
					temp.Gov = orders[i].Gov;
					temp.block = orders[i].block;
					temp.Street = orders[i].Street;
					temp.TripDisp = orders[i].DispatcherName;
					temp.TripTech = orders[i].techName;
					temp.TripNote = orders[i].TripNote;
					temp.TripNo = orders[i].TripNo;
					temp.TripStatusName = orders[i].TripStatusName;
					temp.TripDate = orders[i].TripDate.ToString();
					temp.TripUpdateName = orders[i].TripUpdateName;
					//temp.ContractNo = orders[i].ContractNo;
					//temp.ContractType = orders[i].ContractType;
					//temp.CreatedDateString = orders[i].CreatedDateString.ToString();
					//temp.ExpirationDateString = orders[i].ExpirationDateString.ToString();
					outOrders.Add(temp);
				}
			}
			else
			{
				ObjectResult<int?> ReportCount = db.ReportCountNoTrip(user.Id);
				var count = ReportCount.ToList().FirstOrDefault();

				if (count != null || count != 0)
				{
					outer.TotalCount = count;
				}
				else
				{
					outer.TotalCount = 0;
				}
				List<GenerateReportNoTripPaging_Result> orders = db.GenerateReportNoTripPaging(user.Id, startNo, endNo).ToList();

				for (int i = 0; i < orders.Count; i++)
				{
					PrintViewModel temp = new PrintViewModel();
					temp.DivisionName = orders[i].DivisionName;
					temp.CompanyCode = orders[i].CompanyCode;
					temp.OrderNo = orders[i].OrderNo;
					temp.OrderCreationDate = orders[i].OrderCreationDate.ToString();
					temp.OrderType = orders[i].OrderType;
					temp.Problem = orders[i].Problem;
					temp.Status = orders[i].Status;
					temp.CustomerName = orders[i].CustomerName;
					temp.CustomerTel = orders[i].CustomerTel;
					temp.PACI = orders[i].PACI;
					temp.Area = orders[i].Area;
					temp.DispatcherName = orders[i].DispatcherName;
					temp.SuperName = orders[i].SuperName;
					temp.Gov = orders[i].Gov;
					temp.block = orders[i].block;
					temp.Street = orders[i].Street;
					temp.TripDisp = orders[i].DispatcherName;
					temp.TripTech = orders[i].techName;
					//temp.ContractNo = orders[i].ContractNo;
					//temp.ContractType = orders[i].ContractType;
					//temp.CreatedDateString = orders[i].CreatedDateString.ToString();
					//temp.ExpirationDateString = orders[i].ExpirationDateString.ToString();
					outOrders.Add(temp);
				}
			}
			outer.Orders = outOrders;


			return outer;
		}

		private List<PrintViewModel> GenerateReport(bool isTrip)
		{
			List<PrintViewModel> outOrders = new List<PrintViewModel>();
			if (isTrip == true)
			{

				List<DownloadReportTrip_Result> orders = db.DownloadReportTrip(user.Id).ToList();

				for (int i = 0; i < orders.Count; i++)
				{
					PrintViewModel temp = new PrintViewModel();
					temp.DivisionName = orders[i].DivisionName;
					temp.CompanyCode = orders[i].CompanyCode;
					temp.OrderNo = orders[i].OrderNo;
					temp.OrderCreationDate = orders[i].OrderCreationDate.ToString();
					temp.OrderType = orders[i].OrderType;
					temp.Problem = orders[i].Problem;
					temp.Status = orders[i].Status;
					temp.CustomerName = orders[i].CustomerName;
					temp.CustomerTel = orders[i].CustomerTel;
					temp.PACI = orders[i].PACI;
					temp.Area = orders[i].Area;
					temp.DispatcherName = orders[i].DispatcherName;
					temp.SuperName = orders[i].SuperName;
					temp.Gov = orders[i].Gov;
					temp.block = orders[i].block;
					temp.Street = orders[i].Street;
					temp.TripDisp = orders[i].DispatcherName;
					temp.TripTech = orders[i].techName;
					temp.TripNote = orders[i].TripNote;
					temp.TripNo = orders[i].TripNo;
					temp.TripStatusName = orders[i].TripStatusName;
					temp.TripDate = orders[i].TripDate.ToString();
					temp.TripUpdateName = orders[i].TripUpdateName;
					//temp.ContractNo = orders[i].ContractNo;
					//temp.ContractType = orders[i].ContractType;
					//temp.CreatedDateString = orders[i].CreatedDateString.ToString();
					//temp.ExpirationDateString = orders[i].ExpirationDateString.ToString();
					outOrders.Add(temp);
				}
			}
			else
			{

				List<DownloadReportNoTrip_Result> orders = db.DownloadReportNoTrip(user.Id).ToList();

				for (int i = 0; i < orders.Count; i++)
				{
					PrintViewModel temp = new PrintViewModel();
					temp.DivisionName = orders[i].DivisionName;
					temp.CompanyCode = orders[i].CompanyCode;
					temp.OrderNo = orders[i].OrderNo;
					temp.OrderCreationDate = orders[i].OrderCreationDate.ToString();
					temp.OrderType = orders[i].OrderType;
					temp.Problem = orders[i].Problem;
					temp.Status = orders[i].Status;
					temp.CustomerName = orders[i].CustomerName;
					temp.CustomerTel = orders[i].CustomerTel;
					temp.PACI = orders[i].PACI;
					temp.Area = orders[i].Area;
					temp.DispatcherName = orders[i].DispatcherName;
					temp.SuperName = orders[i].SuperName;
					temp.Gov = orders[i].Gov;
					temp.block = orders[i].block;
					temp.Street = orders[i].Street;
					temp.TripDisp = orders[i].DispatcherName;
					temp.TripTech = orders[i].techName;
					//temp.ContractNo = orders[i].ContractNo;
					//temp.ContractType = orders[i].ContractType;
					//temp.CreatedDateString = orders[i].CreatedDateString.ToString();
					//temp.ExpirationDateString = orders[i].ExpirationDateString.ToString();
					outOrders.Add(temp);
				}
			}
			return outOrders;
		}

		[HttpPost]
		public JsonResult GetReportPageData(int pageNo, bool isTrip)
		{
			var outOrders = GetReportForPage(pageNo, isTrip);
			return Json(outOrders, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetReportData(List<int> ids, bool isTrip)
		{
			return DashboardHelper.GetOrdersAccumlated(ids, isTrip);
		}

		[HttpGet]
		public ActionResult ExportToExcel(bool isTrip)
		{
			var gv = new GridView();
			gv.DataSource = GenerateReport(isTrip);
			gv.DataBind();
			Response.ClearContent();
			Response.Buffer = true;
			Response.AddHeader("content-disposition", "attachment; filename=ExportedExcel.xls");
			Response.ContentType = "application/ms-excel";
			Response.Charset = "";
			Response.ContentEncoding = System.Text.Encoding.Unicode;
			Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

			StringWriter objStringWriter = new StringWriter();
			HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
			gv.RenderControl(objHtmlTextWriter);
			Response.Output.Write(objStringWriter.ToString());
			Response.Flush();
			Response.End();
			return View("Index");
		}
		#endregion

		#region chart_board
		public ActionResult ChartBoard()
		{
			ViewBag.Technicians = db.Technicians.Where(x => x.DispatcherId == user.Id && x.IsDelete != true).ToList();
			ViewBag.Dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.Status = db.OrderStatus.ToList();
			ViewBag.Problems = db.Problems.ToList();
			ViewBag.Types = db.OrderTypes.ToList();
			ViewBag.Divisions = db.Divisions.ToList();
			ViewBag.CompanyCodes = db.CompanyCodes.ToList();
			return View();
		}

		[HttpPost]
		public JsonResult Charts(List<string> divisions)
		{
			List<int> orderIds = new List<int>();
			List<int?> orderDivisionIds = new List<int?>();

			List<ChartViewModel> toReport = new List<ChartViewModel>();
			List<generateChartData_Result> chartResult = new List<generateChartData_Result>();
			if (divisions.Count == 1)
			{
				chartResult = db.generateChartData(divisions[0], "").ToList();
			}
			else
			{
				chartResult = db.generateChartData(divisions[0], divisions[1]).ToList();
			}

			for (int i = 0; i < chartResult.Count; i++)
			{
				ChartViewModel tempResult = new ChartViewModel()
				{
					Area = chartResult[i].areaName,
					CreationDate = chartResult[i].CreatedDate.ToString(),
					DivisionName = chartResult[i].divisionName,
					OrderNo = chartResult[i].OrderNo,
					OrderType = chartResult[i].orderType,
					TechnicianName = chartResult[i].technicianName
				};
				toReport.Add(tempResult);
			}

			var json = JsonConvert.SerializeObject(toReport, Formatting.Indented,
					new JsonSerializerSettings
					{
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore
					});
			return Json(json, JsonRequestBehavior.AllowGet);

		}
		#endregion
	}
}