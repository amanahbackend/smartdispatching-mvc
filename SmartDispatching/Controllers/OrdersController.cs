﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class OrdersController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: Orders
        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.CompanyCode).Include(o => o.Customer).Include(o => o.Division).Include(o => o.OrderPriority).Include(o => o.OrderStatu).Include(o => o.OrderType).Include(o => o.Problem);
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.CompanyCodeId = new SelectList(db.CompanyCodes, "Id", "Name");
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name");
            ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name");
            ViewBag.PriorityId = new SelectList(db.OrderPriorities, "Id", "Name");
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name");
            ViewBag.TypeId = new SelectList(db.OrderTypes, "Id", "Name");
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TypeId,CompanyCodeId,DivisionId,PriorityId,StatusId,ProblemId,CustomerId,CreatedDate,Notes_ICAgent")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyCodeId = new SelectList(db.CompanyCodes, "Id", "Name", order.CompanyCodeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", order.CustomerId);
            ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", order.DivisionId);
            ViewBag.PriorityId = new SelectList(db.OrderPriorities, "Id", "Name", order.PriorityId);
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", order.StatusId);
            ViewBag.TypeId = new SelectList(db.OrderTypes, "Id", "Name", order.TypeId);
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", order.ProblemId);
            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyCodeId = new SelectList(db.CompanyCodes, "Id", "Name", order.CompanyCodeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", order.CustomerId);
            ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", order.DivisionId);
            ViewBag.PriorityId = new SelectList(db.OrderPriorities, "Id", "Name", order.PriorityId);
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", order.StatusId);
            ViewBag.TypeId = new SelectList(db.OrderTypes, "Id", "Name", order.TypeId);
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", order.ProblemId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TypeId,CompanyCodeId,DivisionId,PriorityId,StatusId,ProblemId,CustomerId,CreatedDate,Notes_ICAgent")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyCodeId = new SelectList(db.CompanyCodes, "Id", "Name", order.CompanyCodeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "Name", order.CustomerId);
            ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", order.DivisionId);
            ViewBag.PriorityId = new SelectList(db.OrderPriorities, "Id", "Name", order.PriorityId);
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", order.StatusId);
            ViewBag.TypeId = new SelectList(db.OrderTypes, "Id", "Name", order.TypeId);
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", order.ProblemId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
