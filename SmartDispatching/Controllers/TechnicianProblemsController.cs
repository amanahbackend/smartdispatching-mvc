﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace SmartDispatching.Controllers
{
    public class TechnicianProblemsController : HandlErrorController
	{
        private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());


		// GET: TechnicianProblems
		public ActionResult Index()
        {
            var technicianProblems = db.TechnicianProblems.Include(t => t.Problem).Include(t => t.Technician).Where(x=>x.Technician.SuperDispatcherId == user.Id);
            return View(technicianProblems.ToList());
        }

        // GET: TechnicianProblems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TechnicianProblem technicianProblem = db.TechnicianProblems.Find(id);
            if (technicianProblem == null)
            {
                return HttpNotFound();
            }
            return View(technicianProblem);
        }

        // GET: TechnicianProblems/Create
        public ActionResult Create()
        {
			var technicians = db.Technicians.Where(x => x.SuperDispatcherId == user.Id && x.IsDelete != true);

            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name");
            ViewBag.TechnicianId = new SelectList(technicians, "Id", "Name");
            return View();
        }

        // POST: TechnicianProblems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProblemId,TechnicianId")] TechnicianProblem technicianProblem)
        {
            if (ModelState.IsValid)
            {
                db.TechnicianProblems.Add(technicianProblem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", technicianProblem.ProblemId);
			var techs = db.Technicians.Where(x => x.IsDelete != true);
            ViewBag.TechnicianId = new SelectList(techs, "Id", "Name", technicianProblem.TechnicianId);
            return View(technicianProblem);
        }

        // GET: TechnicianProblems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TechnicianProblem technicianProblem = db.TechnicianProblems.Find(id);
            if (technicianProblem == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", technicianProblem.ProblemId);
			var techs = db.Technicians.Where(x => x.IsDelete != true);
			ViewBag.TechnicianId = new SelectList(techs, "Id", "Name", technicianProblem.TechnicianId);
            return View(technicianProblem);
        }

        // POST: TechnicianProblems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProblemId,TechnicianId")] TechnicianProblem technicianProblem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(technicianProblem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", technicianProblem.ProblemId);
			var techs = db.Technicians.Where(x => x.IsDelete != true);
			ViewBag.TechnicianId = new SelectList(techs, "Id", "Name", technicianProblem.TechnicianId);
            return View(technicianProblem);
        }

        // GET: TechnicianProblems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TechnicianProblem technicianProblem = db.TechnicianProblems.Find(id);
            if (technicianProblem == null)
            {
                return HttpNotFound();
            }
            return View(technicianProblem);
        }

        // POST: TechnicianProblems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TechnicianProblem technicianProblem = db.TechnicianProblems.Find(id);
            db.TechnicianProblems.Remove(technicianProblem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
