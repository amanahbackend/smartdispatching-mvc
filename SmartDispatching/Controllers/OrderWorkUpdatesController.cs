﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class OrderWorkUpdatesController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: OrderWorkUpdates
        public ActionResult Index()
        {
            var orderWorkUpdates = db.OrderWorkUpdates.Include(o => o.Order).Include(o => o.Work_Update);
            return View(orderWorkUpdates.ToList());
        }

        // GET: OrderWorkUpdates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderWorkUpdate orderWorkUpdate = db.OrderWorkUpdates.Find(id);
            if (orderWorkUpdate == null)
            {
                return HttpNotFound();
            }
            return View(orderWorkUpdate);
        }

        // GET: OrderWorkUpdates/Create
        public ActionResult Create()
        {
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent");
            ViewBag.WorkUpdateId = new SelectList(db.Work_Update, "Id", "Name");
            return View();
        }

        // POST: OrderWorkUpdates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrderId,TripNo,WorkUpdateId,CreatedDate,Notes")] OrderWorkUpdate orderWorkUpdate)
        {
            if (ModelState.IsValid)
            {
                db.OrderWorkUpdates.Add(orderWorkUpdate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent", orderWorkUpdate.OrderId);
            ViewBag.WorkUpdateId = new SelectList(db.Work_Update, "Id", "Name", orderWorkUpdate.WorkUpdateId);
            return View(orderWorkUpdate);
        }

        // GET: OrderWorkUpdates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderWorkUpdate orderWorkUpdate = db.OrderWorkUpdates.Find(id);
            if (orderWorkUpdate == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent", orderWorkUpdate.OrderId);
            ViewBag.WorkUpdateId = new SelectList(db.Work_Update, "Id", "Name", orderWorkUpdate.WorkUpdateId);
            return View(orderWorkUpdate);
        }

        // POST: OrderWorkUpdates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrderId,TripNo,WorkUpdateId,CreatedDate,Notes")] OrderWorkUpdate orderWorkUpdate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderWorkUpdate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent", orderWorkUpdate.OrderId);
            ViewBag.WorkUpdateId = new SelectList(db.Work_Update, "Id", "Name", orderWorkUpdate.WorkUpdateId);
            return View(orderWorkUpdate);
        }

        // GET: OrderWorkUpdates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderWorkUpdate orderWorkUpdate = db.OrderWorkUpdates.Find(id);
            if (orderWorkUpdate == null)
            {
                return HttpNotFound();
            }
            return View(orderWorkUpdate);
        }

        // POST: OrderWorkUpdates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderWorkUpdate orderWorkUpdate = db.OrderWorkUpdates.Find(id);
            db.OrderWorkUpdates.Remove(orderWorkUpdate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
