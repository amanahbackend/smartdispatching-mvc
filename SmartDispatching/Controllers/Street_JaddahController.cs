﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class Street_JaddahController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: Street_Jaddah
        public ActionResult Index()
        {
            return View(db.Street_Jaddah.ToList());
        }

        // GET: Street_Jaddah/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Street_Jaddah street_Jaddah = db.Street_Jaddah.Find(id);
            if (street_Jaddah == null)
            {
                return HttpNotFound();
            }
            return View(street_Jaddah);
        }

        // GET: Street_Jaddah/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Street_Jaddah/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Street_Jaddah street_Jaddah)
        {
            if (ModelState.IsValid)
            {
                db.Street_Jaddah.Add(street_Jaddah);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(street_Jaddah);
        }

        // GET: Street_Jaddah/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Street_Jaddah street_Jaddah = db.Street_Jaddah.Find(id);
            if (street_Jaddah == null)
            {
                return HttpNotFound();
            }
            return View(street_Jaddah);
        }

        // POST: Street_Jaddah/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Street_Jaddah street_Jaddah)
        {
            if (ModelState.IsValid)
            {
                db.Entry(street_Jaddah).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(street_Jaddah);
        }

        // GET: Street_Jaddah/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Street_Jaddah street_Jaddah = db.Street_Jaddah.Find(id);
            if (street_Jaddah == null)
            {
                return HttpNotFound();
            }
            return View(street_Jaddah);
        }

        // POST: Street_Jaddah/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Street_Jaddah street_Jaddah = db.Street_Jaddah.Find(id);
            db.Street_Jaddah.Remove(street_Jaddah);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
