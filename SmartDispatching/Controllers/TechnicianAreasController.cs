﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace SmartDispatching.Controllers
{
    public class TechnicianAreasController : HandlErrorController
	{
        private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: TechnicianAreas
		public ActionResult Index()
        {
            var technicianAreas = db.TechnicianAreas.Include(t => t.SAPArea).Include(t => t.Technician).Where(x=>x.Technician.SuperDispatcherId == user.Id);
            return View(technicianAreas.ToList());
        }

        // GET: TechnicianAreas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TechnicianArea technicianArea = db.TechnicianAreas.Find(id);
            if (technicianArea == null)
            {
                return HttpNotFound();
            }
            return View(technicianArea);
        }

        // GET: TechnicianAreas/Create
        public ActionResult Create()
        {

			var technicians = db.Technicians.Where(x => x.SuperDispatcherId == user.Id && x.IsDelete != true);

			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name");
            ViewBag.TechnicianId = new SelectList(technicians, "Id", "Name");
            return View();
        }

        // POST: TechnicianAreas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AreaId,TechnicianId")] TechnicianArea technicianArea)
        {
            if (ModelState.IsValid)
            {
                db.TechnicianAreas.Add(technicianArea);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", technicianArea.AreaId);
			var techs = db.Technicians.Where(x => x.IsDelete != true);
            ViewBag.TechnicianId = new SelectList(techs, "Id", "Name", technicianArea.TechnicianId);
            return View(technicianArea);
        }

        // GET: TechnicianAreas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TechnicianArea technicianArea = db.TechnicianAreas.Find(id);
            if (technicianArea == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", technicianArea.AreaId);
			var techs = db.Technicians.Where(x => x.IsDelete != true);
            ViewBag.TechnicianId = new SelectList(techs, "Id", "Name", technicianArea.TechnicianId);
            return View(technicianArea);
        }

        // POST: TechnicianAreas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AreaId,TechnicianId")] TechnicianArea technicianArea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(technicianArea).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", technicianArea.AreaId);
			var techs = db.Technicians.Where(x => x.IsDelete != true);
			ViewBag.TechnicianId = new SelectList(techs, "Id", "Name", technicianArea.TechnicianId);
            return View(technicianArea);
        }

        // GET: TechnicianAreas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TechnicianArea technicianArea = db.TechnicianAreas.Find(id);
            if (technicianArea == null)
            {
                return HttpNotFound();
            }
            return View(technicianArea);
        }

        // POST: TechnicianAreas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TechnicianArea technicianArea = db.TechnicianAreas.Find(id);
            db.TechnicianAreas.Remove(technicianArea);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
