﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartDispatching.Controllers
{
	public class ErrorController : HandlErrorController
	{

		public ViewResult Index()
		{
			return View();
		}
		public ViewResult NotFound()
		{
			return View("NotFound");
		}
		public ActionResult HttpError500()
		{
			return View();
		}

		public ActionResult HttpError404()
		{
			return View();
		}
	}
}