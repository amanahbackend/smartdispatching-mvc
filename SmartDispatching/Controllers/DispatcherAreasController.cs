﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using System.Runtime.Serialization.Json;

namespace SmartDispatching.Controllers
{
	public class DispatcherAreasController : HandlErrorController
	{
		private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: DispatcherAreas
		public ActionResult Index()
		{
			var dispatcherAreas = db.DispatcherAreas.Include(d => d.SAPArea).Include(d => d.AspNetUser);

			//ApplicationDbContext contextdb = new ApplicationDbContext();
			//var targetDispatcher = db.SuperDispatchers.Where(x => x.SuperId == user.Id).ToList();
			//List<DispatcherArea> outDispatcherArea = new List<DispatcherArea>();
			//var dispatcherAreas = db.DispatcherAreas.Include(d => d.Area).Include(d => d.AspNetUser);
			//for (int i = 0; i < targetDispatcher.Count; i++)
			//{
			//	foreach (var item in dispatcherAreas)
			//	{
			//		if (item.DispatcherId == targetDispatcher[i].DispatcherId)
			//		{
			//			var tempDispatcherArea = db.DispatcherAreas.Include(d => d.Area).Include(d => d.AspNetUser).Where(x => x.DispatcherId == item.DispatcherId).FirstOrDefault();
			//			outDispatcherArea.Add(tempDispatcherArea);
			//		}
			//	}
			//}


			return View(dispatcherAreas);
		}

		// GET: DispatcherAreas/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherArea dispatcherArea = db.DispatcherAreas.Find(id);
			if (dispatcherArea == null)
			{
				return HttpNotFound();
			}
			return View(dispatcherArea);
		}

		// GET: DispatcherAreas/Create
		public ActionResult Create()
		{

			//var areas = GetAreasFromServices();
			//var areasData = areas.features;

			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name");

			var targetDispatcher = db.SuperDispatchers.Where(x => x.SuperId == user.Id);
			List<AspNetUser> users = new List<AspNetUser>();

			foreach (var item in targetDispatcher)
			{
				var user = db.AspNetUsers.Where(x => x.Id == item.DispatcherId).FirstOrDefault();
				users.Add(user);
			}

			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName");
			return View();
		}


		public AreaPACIModel GetAreasFromServices()
		{
			string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
	Request.ApplicationPath.TrimEnd('/') + "/";
			var URL = baseUrl + "/proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/AddressSearch/MapServer/2/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=Nhood_No%2CNeighborhoodEnglish&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson";
			WebRequest request = WebRequest.Create(URL);
			WebResponse response = request.GetResponse();
			Stream dataStream = response.GetResponseStream();
			// Open the stream using a StreamReader for easy access.  
			StreamReader reader = new StreamReader(dataStream);
			// Read the content.  
			string responseFromServer = reader.ReadToEnd();
			//JavaScriptSerializer oJS = new JavaScriptSerializer();
			//AreaPACIModel oRootObject = new AreaPACIModel();
			//oRootObject = oJS.Deserialize<AreaPACIModel>(responseFromServer);

			//var serializer = new JavaScriptSerializer();
			//var deserializedResult = serializer.Deserialize<AreaPACIModel>(responseFromServer);

			AreaPACIModel deserializedUser = new AreaPACIModel();
			MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(responseFromServer));
			DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedUser.GetType());
			deserializedUser = ser.ReadObject(ms) as AreaPACIModel;
			ms.Close();

			return deserializedUser;
		}

		// POST: DispatcherAreas/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,DispatcherId,AreaId")] DispatcherArea dispatcherArea)
		{
			bool isComplete = true;
			bool isUpdated = false;

			if (ModelState.IsValid)
			{
				var dispatcherDivisonId = db.SuperDispatchers.Where(x => x.DispatcherId == dispatcherArea.DispatcherId).FirstOrDefault().DivisionId;
				List<DispatcherCombination> dcList = new List<DispatcherCombination>();

				var updateAreas = db.DispatcherCombinations.Where(x => x.AreaId == 0 && x.DispatcherId == dispatcherArea.DispatcherId).ToList();
				if (updateAreas != null && updateAreas.Count > 0)
				{
					for (int j = 0; j < updateAreas.Count; j++)
					{
						var obj = updateAreas[j];
						obj.AreaId = dispatcherArea.AreaId;
						obj.CombinationCode = int.Parse(dispatcherArea.AreaId.ToString() + obj.ProblemId.ToString() + obj.DivisionId.ToString());
						dcList.Add(obj);
					}
					isUpdated = true;
				}
				else
				{
					var dispatcherProblems = db.DispatcherProblems.Where(x => x.DispatcherId == dispatcherArea.DispatcherId).ToList();

					if (dispatcherProblems != null && dispatcherProblems.Count > 0)
					{
						for (int i = 0; i < dispatcherProblems.Count; i++)
						{
							DispatcherCombination dc = new DispatcherCombination()
							{
								DispatcherId = dispatcherArea.DispatcherId,
								AreaId = dispatcherArea.AreaId,
								DivisionId = dispatcherDivisonId,
								ProblemId = dispatcherProblems[i].ProblemId
							};
							dc.CombinationCode = int.Parse(dc.AreaId.ToString() + dc.ProblemId.ToString() + dc.DivisionId.ToString());
							dcList.Add(dc);
						}
					}
					else
					{
						DispatcherCombination dc = new DispatcherCombination()
						{
							DispatcherId = dispatcherArea.DispatcherId,
							AreaId = dispatcherArea.AreaId,
							DivisionId = dispatcherDivisonId,
							ProblemId = 0
						};
						dc.CombinationCode = int.Parse(dc.AreaId.ToString() + dc.ProblemId.ToString() + dc.DivisionId.ToString());
						db.DispatcherCombinations.Add(dc);
						db.SaveChanges();
						isComplete = true;
					}
				}

				if (dcList.Count > 0 )
				{
					for (int i = 0; i < dcList.Count; i++)
					{
						int? combCode = dcList[i].CombinationCode;
						var isFound = db.DispatcherCombinations.Where(x => x.CombinationCode == combCode).ToList();
						if (isFound != null && isFound.Count > 0)
						{
							isComplete = false;
							break;
						}
					}
				}
				
				if (isComplete == true)
				{
					db.DispatcherCombinations.AddRange(dcList);
					db.SaveChanges();
					if (isUpdated == true )
					{
						var removeAreas = db.DispatcherCombinations.Where(x => x.AreaId == 0 && x.DispatcherId == dispatcherArea.DispatcherId).ToList();
						db.DispatcherCombinations.RemoveRange(removeAreas);
						db.SaveChanges();
					}
					db.DispatcherAreas.Add(dispatcherArea);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
			}

			var targetDispatcher = db.SuperDispatchers.Where(x => x.SuperId == user.Id);
			List<AspNetUser> users = new List<AspNetUser>();

			foreach (var item in targetDispatcher)
			{
				var user = db.AspNetUsers.Where(x => x.Id == item.DispatcherId).FirstOrDefault();
				users.Add(user);
			}

			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName");
			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", dispatcherArea.AreaId);
			ModelState.AddModelError("", "Can not assign this area for this dispatcher!");

			return View(dispatcherArea);
		}

		// GET: DispatcherAreas/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherArea dispatcherArea = db.DispatcherAreas.Find(id);
			if (dispatcherArea == null)
			{
				return HttpNotFound();
			}
			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", dispatcherArea.AreaId);
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherArea.DispatcherId);
			return View(dispatcherArea);
		}

		// POST: DispatcherAreas/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,DispatcherId,AreaId")] DispatcherArea dispatcherArea)
		{
			if (ModelState.IsValid)
			{
				List<DispatcherCombination> dcList = new List<DispatcherCombination>();

				var areaId = db.DispatcherAreas.Where(x => x.Id == dispatcherArea.Id).FirstOrDefault().AreaId;
				var dispatcherAreas = db.DispatcherCombinations.Where(x => x.AreaId == areaId && x.DispatcherId == dispatcherArea.DispatcherId).ToList();

				for (int i = 0; i < dispatcherAreas.Count; i++)
				{
					var obj = dispatcherAreas[i];
					obj.AreaId = dispatcherArea.AreaId;
					obj.CombinationCode = int.Parse(dispatcherArea.AreaId.ToString() + obj.ProblemId.ToString() + obj.DivisionId.ToString());
					dcList.Add(obj);
				}
				bool isAllow = true;
				for (int i = 0; i < dcList.Count; i++)
				{
					var comb = dcList[i].CombinationCode;
					var combinations = db.DispatcherCombinations.Where(x => x.CombinationCode == comb).ToList();
					if (combinations != null && combinations.Count > 0)
					{
						isAllow = false;
						break;
					}
				}
				if (isAllow == true)
				{
					if (dcList != null && dcList.Count > 0)
					{
						db.DispatcherCombinations.RemoveRange(dispatcherAreas);
						db.SaveChanges();
						db.DispatcherCombinations.AddRange(dcList);
						db.SaveChanges();
					}
					var area = db.DispatcherAreas.Where(x => x.Id == dispatcherArea.Id).FirstOrDefault();
					area.AreaId = dispatcherArea.AreaId;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				else
				{
					ModelState.AddModelError("", "Can not assign this area for this dispatcher!");
				}
			}
			
			ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", dispatcherArea.AreaId);
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherArea.DispatcherId);
			return View(dispatcherArea);
		}

		// GET: DispatcherAreas/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherArea dispatcherArea = db.DispatcherAreas.Find(id);
			if (dispatcherArea == null)
			{
				return HttpNotFound();
			}
			return View(dispatcherArea);
		}

		// POST: DispatcherAreas/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			DispatcherArea dispatcherArea = db.DispatcherAreas.Find(id);
			db.DispatcherAreas.Remove(dispatcherArea);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
