﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class CancellationController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: Cancellation
        public ActionResult Index()
        {
            return View(db.Cancellation_Reasons.ToList());
        }

        // GET: Cancellation/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancellation_Reasons cancellation_Reasons = db.Cancellation_Reasons.Find(id);
            if (cancellation_Reasons == null)
            {
                return HttpNotFound();
            }
            return View(cancellation_Reasons);
        }

        // GET: Cancellation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cancellation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Cancellation_Reasons cancellation_Reasons)
        {
            if (ModelState.IsValid)
            {
                db.Cancellation_Reasons.Add(cancellation_Reasons);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cancellation_Reasons);
        }

        // GET: Cancellation/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancellation_Reasons cancellation_Reasons = db.Cancellation_Reasons.Find(id);
            if (cancellation_Reasons == null)
            {
                return HttpNotFound();
            }
            return View(cancellation_Reasons);
        }

        // POST: Cancellation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Cancellation_Reasons cancellation_Reasons)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cancellation_Reasons).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cancellation_Reasons);
        }

        // GET: Cancellation/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancellation_Reasons cancellation_Reasons = db.Cancellation_Reasons.Find(id);
            if (cancellation_Reasons == null)
            {
                return HttpNotFound();
            }
            return View(cancellation_Reasons);
        }

        // POST: Cancellation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cancellation_Reasons cancellation_Reasons = db.Cancellation_Reasons.Find(id);
            db.Cancellation_Reasons.Remove(cancellation_Reasons);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
