﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class CompanyCodesController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: CompanyCodes
        public ActionResult Index()
        {
            return View(db.CompanyCodes.ToList());
        }

        // GET: CompanyCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyCode companyCode = db.CompanyCodes.Find(id);
            if (companyCode == null)
            {
                return HttpNotFound();
            }
            return View(companyCode);
        }

        // GET: CompanyCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CompanyCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] CompanyCode companyCode)
        {
            if (ModelState.IsValid)
            {
                db.CompanyCodes.Add(companyCode);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(companyCode);
        }

        // GET: CompanyCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyCode companyCode = db.CompanyCodes.Find(id);
            if (companyCode == null)
            {
                return HttpNotFound();
            }
            return View(companyCode);
        }

        // POST: CompanyCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] CompanyCode companyCode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(companyCode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(companyCode);
        }

        // GET: CompanyCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyCode companyCode = db.CompanyCodes.Find(id);
            if (companyCode == null)
            {
                return HttpNotFound();
            }
            return View(companyCode);
        }

        // POST: CompanyCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CompanyCode companyCode = db.CompanyCodes.Find(id);
            db.CompanyCodes.Remove(companyCode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
