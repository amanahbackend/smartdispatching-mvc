﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class WorkUpdateController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: WorkUpdate
        public ActionResult Index()
        {
            var work_Update = db.Work_Update.Include(w => w.OrderStatu);
            return View(work_Update.ToList());
        }

        // GET: WorkUpdate/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work_Update work_Update = db.Work_Update.Find(id);
            if (work_Update == null)
            {
                return HttpNotFound();
            }
            return View(work_Update);
        }

        // GET: WorkUpdate/Create
        public ActionResult Create()
        {
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name");
            return View();
        }

        // POST: WorkUpdate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,StatusId")] Work_Update work_Update)
        {
            if (ModelState.IsValid)
            {
                db.Work_Update.Add(work_Update);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", work_Update.StatusId);
            return View(work_Update);
        }

        // GET: WorkUpdate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work_Update work_Update = db.Work_Update.Find(id);
            if (work_Update == null)
            {
                return HttpNotFound();
            }
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", work_Update.StatusId);
            return View(work_Update);
        }

        // POST: WorkUpdate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,StatusId")] Work_Update work_Update)
        {
            if (ModelState.IsValid)
            {
                db.Entry(work_Update).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StatusId = new SelectList(db.OrderStatus, "Id", "Name", work_Update.StatusId);
            return View(work_Update);
        }

        // GET: WorkUpdate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work_Update work_Update = db.Work_Update.Find(id);
            if (work_Update == null)
            {
                return HttpNotFound();
            }
            return View(work_Update);
        }

        // POST: WorkUpdate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Work_Update work_Update = db.Work_Update.Find(id);
            db.Work_Update.Remove(work_Update);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
