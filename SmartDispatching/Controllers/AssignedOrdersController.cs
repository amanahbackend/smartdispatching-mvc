﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class AssignedOrdersController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: AssignedOrders
        public ActionResult Index()
        {
            var assignedOrders = db.AssignedOrders.Include(a => a.Forman).Include(a => a.Order).Include(a => a.Technician);
            return View(assignedOrders.ToList());
        }

        // GET: AssignedOrders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssignedOrder assignedOrder = db.AssignedOrders.Find(id);
            if (assignedOrder == null)
            {
                return HttpNotFound();
            }
            return View(assignedOrder);
        }

        // GET: AssignedOrders/Create
        public ActionResult Create()
        {
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name");
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent");
            ViewBag.TechnicianId = new SelectList(db.Technicians, "Id", "Name");
            return View();
        }

        // POST: AssignedOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrderId,TechnicianId,FormanId,DispatcherId")] AssignedOrder assignedOrder)
        {
            if (ModelState.IsValid)
            {
                db.AssignedOrders.Add(assignedOrder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", assignedOrder.FormanId);
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent", assignedOrder.OrderId);
            ViewBag.TechnicianId = new SelectList(db.Technicians, "Id", "Name", assignedOrder.TechnicianId);
            return View(assignedOrder);
        }

        // GET: AssignedOrders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssignedOrder assignedOrder = db.AssignedOrders.Find(id);
            if (assignedOrder == null)
            {
                return HttpNotFound();
            }
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", assignedOrder.FormanId);
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent", assignedOrder.OrderId);
            ViewBag.TechnicianId = new SelectList(db.Technicians, "Id", "Name", assignedOrder.TechnicianId);
            return View(assignedOrder);
        }

        // POST: AssignedOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrderId,TechnicianId,FormanId,DispatcherId")] AssignedOrder assignedOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assignedOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", assignedOrder.FormanId);
            ViewBag.OrderId = new SelectList(db.Orders, "Id", "Notes_ICAgent", assignedOrder.OrderId);
            ViewBag.TechnicianId = new SelectList(db.Technicians, "Id", "Name", assignedOrder.TechnicianId);
            return View(assignedOrder);
        }

        // GET: AssignedOrders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssignedOrder assignedOrder = db.AssignedOrders.Find(id);
            if (assignedOrder == null)
            {
                return HttpNotFound();
            }
            return View(assignedOrder);
        }

        // POST: AssignedOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssignedOrder assignedOrder = db.AssignedOrders.Find(id);
            db.AssignedOrders.Remove(assignedOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
