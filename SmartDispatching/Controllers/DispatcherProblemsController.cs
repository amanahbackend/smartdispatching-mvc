﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace SmartDispatching.Controllers
{
	public class DispatcherProblemsController : HandlErrorController
	{
		private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: DispatcherProblems
		public ActionResult Index()
		{
			var dispatcherProblems = db.DispatcherProblems.Include(d => d.AspNetUser).Include(d => d.Problem);
			return View(dispatcherProblems.ToList());
		}

		// GET: DispatcherProblems/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherProblem dispatcherProblem = db.DispatcherProblems.Find(id);
			if (dispatcherProblem == null)
			{
				return HttpNotFound();
			}
			return View(dispatcherProblem);
		}

		// GET: DispatcherProblems/Create
		public ActionResult Create()
		{
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName");
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name");
			return View();
		}

		// POST: DispatcherProblems/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,DispatcherId,ProblemId")] DispatcherProblem dispatcherProblem)
		{
			bool isComplete = true;
			bool isUpdated = false;
			if (ModelState.IsValid)
			{
				var dispatcherDivisonId = db.SuperDispatchers.Where(x => x.DispatcherId == dispatcherProblem.DispatcherId).FirstOrDefault().DivisionId;
				List<DispatcherCombination> dcList = new List<DispatcherCombination>();

				var updateProblems = db.DispatcherCombinations.Where(x => x.ProblemId == 0 && x.DispatcherId == dispatcherProblem.DispatcherId).ToList();
				if (updateProblems != null && updateProblems.Count > 0)
				{
					for (int j = 0; j < updateProblems.Count; j++)
					{
						var obj = updateProblems[j];
						obj.ProblemId = dispatcherProblem.ProblemId;
						obj.CombinationCode = int.Parse(obj.AreaId.ToString() + dispatcherProblem.ProblemId.ToString() + obj.DivisionId.ToString());
						dcList.Add(obj);
					}
					isUpdated = true;
				}
				else
				{
					var dispatcherAreas = db.DispatcherAreas.Where(x => x.DispatcherId == dispatcherProblem.DispatcherId).ToList();

					if (dispatcherAreas != null && dispatcherAreas.Count > 0)
					{
						for (int i = 0; i < dispatcherAreas.Count; i++)
						{
							DispatcherCombination dc = new DispatcherCombination()
							{
								DispatcherId = dispatcherProblem.DispatcherId,
								AreaId = dispatcherAreas[i].AreaId,
								DivisionId = dispatcherDivisonId,
								ProblemId = dispatcherProblem.ProblemId
							};
							dc.CombinationCode = int.Parse(dc.AreaId.ToString() + dc.ProblemId.ToString() + dc.DivisionId.ToString());
							dcList.Add(dc);
						}
					}
					else
					{
						DispatcherCombination dc = new DispatcherCombination()
						{
							DispatcherId = dispatcherProblem.DispatcherId,
							AreaId = 0,
							DivisionId = dispatcherDivisonId,
							ProblemId = dispatcherProblem.ProblemId
						};
						dc.CombinationCode = int.Parse(dc.AreaId.ToString() + dc.ProblemId.ToString() + dc.DivisionId.ToString());
						db.DispatcherCombinations.Add(dc);
						db.SaveChanges();
						isComplete = true;
					}
				}

				if (dcList.Count > 0)
				{
					for (int i = 0; i < dcList.Count; i++)
					{
						int? combCode = dcList[i].CombinationCode;
						var isFound = db.DispatcherCombinations.Where(x => x.CombinationCode == combCode).ToList();
						if (isFound != null && isFound.Count > 0)
						{
							isComplete = false;
							break;
						}
					}
				}

				if (isComplete == true)
				{
					db.DispatcherCombinations.AddRange(dcList);
					db.SaveChanges();
					if (isUpdated == true)
					{
						var removeAreas = db.DispatcherCombinations.Where(x => x.ProblemId == 0 && x.DispatcherId == dispatcherProblem.DispatcherId).ToList();
						db.DispatcherCombinations.RemoveRange(removeAreas);
						db.SaveChanges();
					}
					db.DispatcherProblems.Add(dispatcherProblem);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				else
				{
					ModelState.AddModelError("", "Can not assign this problem for this dispatcher!");
				}
			}
			var targetDispatcher = db.SuperDispatchers.Where(x => x.SuperId == user.Id);
			List<AspNetUser> users = new List<AspNetUser>();

			foreach (var item in targetDispatcher)
			{
				var user = db.AspNetUsers.Where(x => x.Id == item.DispatcherId).FirstOrDefault();
				users.Add(user);
			}

			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherProblem.DispatcherId);
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", dispatcherProblem.ProblemId);
			return View(dispatcherProblem);
		}

		// GET: DispatcherProblems/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherProblem dispatcherProblem = db.DispatcherProblems.Find(id);
			if (dispatcherProblem == null)
			{
				return HttpNotFound();
			}
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherProblem.DispatcherId);
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", dispatcherProblem.ProblemId);
			return View(dispatcherProblem);
		}

		// POST: DispatcherProblems/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,DispatcherId,ProblemId")] DispatcherProblem dispatcherProblem)
		{
			if (ModelState.IsValid)
			{
				List<DispatcherCombination> dcList = new List<DispatcherCombination>();

				var problemId = db.DispatcherProblems.Where(x => x.Id == dispatcherProblem.Id).FirstOrDefault().ProblemId;
				var dispatcherProblems = db.DispatcherCombinations.Where(x => x.ProblemId == problemId && x.DispatcherId == dispatcherProblem.DispatcherId).ToList();

				for (int i = 0; i < dispatcherProblems.Count; i++)
				{
					var obj = dispatcherProblems[i];
					obj.ProblemId = dispatcherProblem.ProblemId;
					obj.CombinationCode = int.Parse(obj.AreaId.ToString() + dispatcherProblem.ProblemId.ToString() + obj.DivisionId.ToString());
					dcList.Add(obj);
				}
				bool isAllow = true;
				for (int i = 0; i < dcList.Count; i++)
				{
					var comb = dcList[i].CombinationCode;
					var combinations = db.DispatcherCombinations.Where(x => x.CombinationCode == comb).ToList();
					if (combinations != null && combinations.Count > 0)
					{
						isAllow = false;
						break;
					}
				}
				if (isAllow == true)
				{
					if (dcList != null && dcList.Count > 0)
					{
						db.DispatcherCombinations.RemoveRange(dispatcherProblems);
						db.SaveChanges();
						db.DispatcherCombinations.AddRange(dcList);
						db.SaveChanges();
					}
					var problem = db.DispatcherProblems.Where(x => x.Id == dispatcherProblem.Id).FirstOrDefault();
					problem.ProblemId = dispatcherProblem.ProblemId;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				else
				{
					ModelState.AddModelError("", "Can not assign this problem for this dispatcher!");
				}
			}
			var users = db.AspNetUsers.Where(x => x.RoleId == 5).ToList();
			ViewBag.DispatcherId = new SelectList(users, "Id", "UserName", dispatcherProblem.DispatcherId);
			ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", dispatcherProblem.ProblemId);
			return View(dispatcherProblem);
		}

		// GET: DispatcherProblems/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			DispatcherProblem dispatcherProblem = db.DispatcherProblems.Find(id);
			if (dispatcherProblem == null)
			{
				return HttpNotFound();
			}
			return View(dispatcherProblem);
		}

		// POST: DispatcherProblems/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			DispatcherProblem dispatcherProblem = db.DispatcherProblems.Find(id);
			db.DispatcherProblems.Remove(dispatcherProblem);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
