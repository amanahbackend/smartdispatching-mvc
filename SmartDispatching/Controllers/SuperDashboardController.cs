﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartDispatching.Controllers
{
	[Authorize]
	public class SuperDashboardController : HandlErrorController
	{
		Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: SuperDashboard
		public ActionResult Index()
		{
			List<OrderBoardViewModel> unAssignedOrderForBoard = new List<OrderBoardViewModel>();
			var unAssignedOrders = db.supervisorUnAssigned(user.Id).ToList();
			for (int i = 0; i < unAssignedOrders.Count; i++)
			{
				OrderBoardViewModel tempOrder = new OrderBoardViewModel()
				{
					PACI = unAssignedOrders[i].PACI,
					CreatedDate = unAssignedOrders[i].CreatedDate,
					DateTo = unAssignedOrders[i].DateTo,
					DateFrom = unAssignedOrders[i].DateFrom,
					Id = unAssignedOrders[i].orderId,
					ProblemId = unAssignedOrders[i].ProblemId,
					StatusId = unAssignedOrders[i].StatusId,
					OrderNo = unAssignedOrders[i].OrderNo,
					Type = unAssignedOrders[i].Type,
					TypeId = unAssignedOrders[i].TypeId,
					Problem = unAssignedOrders[i].Problem,
					Area = unAssignedOrders[i].Area
				};
				unAssignedOrderForBoard.Add(tempOrder);
			}

			ViewBag.UnAssignedOrders = unAssignedOrderForBoard;

			/// Fill Dispatchers Board
			var dispatchers = db.SuperDispatchers.Where(x => x.SuperId == user.Id).ToList();
			List<DispatcherBoardViewModel> outDispatchers = new List<DispatcherBoardViewModel>();

			foreach (var dispatcher in dispatchers)
			{
				DispatcherBoardViewModel tempDispatcher = new DispatcherBoardViewModel();
				var dispatcherName = db.AspNetUsers.Where(x => x.Id == dispatcher.DispatcherId).FirstOrDefault().UserName;

				tempDispatcher.Id = dispatcher.DispatcherId;
				tempDispatcher.Name = dispatcherName;

				var formans = db.Formen.Where(x => x.DispatcherId == dispatcher.DispatcherId).ToList();
				var technicians = db.Technicians.Where(x => x.DispatcherId == dispatcher.DispatcherId && x.IsDelete != true).ToList();

				List<FormanBoardViewModel> outFormans = new List<FormanBoardViewModel>();
				foreach (var item in formans)
				{
					var tempTechnicians = technicians.Where(x => x.FormanId == item.Id).ToList();
					List<TechnicianBoardViewModel> tech = new List<TechnicianBoardViewModel>();
					foreach (var techItem in tempTechnicians)
					{
						List<OrderBoardViewModel> orders = new List<OrderBoardViewModel>();
						var tempout = db.getAssignedOrdersForTechs(techItem.Id.ToString()).ToList();
						for (int i = 0; i < tempout.Count; i++)
						{
							OrderBoardViewModel orderDetail = new OrderBoardViewModel()
							{
								Id = tempout[i].orderId,
								CreatedDate = tempout[i].CreatedDate,
								DateFrom = tempout[i].DateFrom,
								DateTo = tempout[i].DateTo,
								OrderNo = tempout[i].OrderNo,
								CustomerName = tempout[i].CustomerName,
								Area = tempout[i].Area,
								Block = tempout[i].Block,
								Problem = tempout[i].Problem,
								ProblemId = tempout[i].ProblemId,
								Type = tempout[i].Type,
								TypeId = tempout[i].TypeId
							};
							orders.Add(orderDetail);
						}

						TechnicianBoardViewModel tempTech = new TechnicianBoardViewModel()
						{
							//Availability = techItem.TV_Availability.Name,
							Id = techItem.Id,
							Name = techItem.Name,
							OrderCounts = techItem.AssignedOrders.Count,
							Orders = orders
						};
						tech.Add(tempTech);
					}

					FormanBoardViewModel temp = new FormanBoardViewModel()
					{
						AllOrderCount = tempTechnicians.Count,
						Name = item.Name,
						Id = item.Id,
						Technicians = tech
					};
					outFormans.Add(temp);
				}
				tempDispatcher.Formans = outFormans;

				var dispatcherUnassigendOrders = db.AssignedOrders.Where(x => x.SupervisorId == user.Id && x.DispatcherId == dispatcher.DispatcherId && x.TechnicianId == null && x.IsClosed == false).ToList();

				List<OrderBoardViewModel> dispatcherUnassigendOrdersList = new List<OrderBoardViewModel>();
				foreach (var order in dispatcherUnassigendOrders)
				{
					var type = db.Orders.Where(x => x.Id == order.OrderId).FirstOrDefault().OrderType;
					var address = order.Order.Address;

					OrderBoardViewModel tempOrder = new OrderBoardViewModel()
					{
						PACI = address.PACI,
						CreatedDate = order.Order.CreatedDate,
						DateTo = order.DateTo,
						DateFrom = order.DateFrom,
						Id = order.Order.Id,
						StatusId = order.Order.StatusId,
						OrderNo = order.Order.OrderNo
					};

					if (order.Order.Problem != null)
					{
						tempOrder.Problem = order.Order.Problem.Name;
						tempOrder.ProblemId = order.Order.ProblemId;
					}

					if (type != null)
					{
						tempOrder.Type = type.Name;
						tempOrder.TypeId = type.Id;
					}
					if (address.Area != null)
					{
						tempOrder.Area = address.Area.Name;
					}
					dispatcherUnassigendOrdersList.Add(tempOrder);
				}
				tempDispatcher.UnAssignedOrders = dispatcherUnassigendOrdersList;

				outDispatchers.Add(tempDispatcher);
			}

			ViewBag.Dispatchers = JsonConvert.SerializeObject(outDispatchers, Formatting.Indented,
					new JsonSerializerSettings
					{
						ReferenceLoopHandling = ReferenceLoopHandling.Ignore
					});
			return View();
		}

		public ActionResult ForemanIndex()
		{
			List<FormanBoardViewModel> outFormans = new List<FormanBoardViewModel>();

			Forman forman = db.Formen.Where(x => x.ForemanUserId == user.Id).FirstOrDefault();

			if (forman != null)
			{
				var technicians = db.Technicians.Where(x => x.FormanId == forman.Id && x.IsDelete != true).ToList();

				var tempTechnicians = technicians.Where(x => x.FormanId == forman.Id).ToList();
				List<TechnicianBoardViewModel> tech = new List<TechnicianBoardViewModel>();
				foreach (var techItem in tempTechnicians)
				{
					List<OrderBoardViewModel> orders = new List<OrderBoardViewModel>();
					var tempout = db.getAssignedOrdersForTechs(techItem.Id.ToString()).ToList();
					for (int i = 0; i < tempout.Count; i++)
					{
						OrderBoardViewModel orderDetail = new OrderBoardViewModel()
						{
							Id = tempout[i].orderId,
							CreatedDate = tempout[i].CreatedDate,
							DateFrom = tempout[i].DateFrom,
							DateTo = tempout[i].DateTo,
							OrderNo = tempout[i].OrderNo,
							CustomerName = tempout[i].CustomerName,
							Area = tempout[i].Area,
							Block = tempout[i].Block,
							Problem = tempout[i].Problem,
							ProblemId = tempout[i].ProblemId,
							Type = tempout[i].Type,
							TypeId = tempout[i].TypeId
						};
						orders.Add(orderDetail);
					}
					int counts = 0;
					var compeletedOrders = db.AssignedOrders.Where(x => x.TechnicianId == techItem.Id && x.IsClosed == true).ToList();
					for (int i = 0; i < compeletedOrders.Count(); i++)
					{
						if (compeletedOrders[i].ActionDate != null)
						{
							if (DateTime.Parse(compeletedOrders[i].ActionDate.ToString()).Date == DateTime.Now.Date)
							{
								counts++;
							}
						}
					}
					TechnicianBoardViewModel tempTech = new TechnicianBoardViewModel()
					{
						Id = techItem.Id,
						Name = techItem.Name,
						OrderCounts = techItem.AssignedOrders.Count,
						Orders = orders,
						CompeletedCount = counts
					};
					var availabilityObj = db.TV_Availability.Where(x => x.Id == techItem.AvailabilityId).FirstOrDefault();
					if (availabilityObj != null)
					{
						tempTech.Availability = availabilityObj.Name;
					}
					tech.Add(tempTech);
				}

				FormanBoardViewModel temp = new FormanBoardViewModel()
				{
					AllOrderCount = tempTechnicians.Count,
					Name = forman.Name,
					Id = forman.Id,
					Technicians = tech
				};
				outFormans.Add(temp);


				ViewBag.Formans = JsonConvert.SerializeObject(outFormans, Formatting.Indented,
						new JsonSerializerSettings
						{
							ReferenceLoopHandling = ReferenceLoopHandling.Ignore
						});
			}
			return View();
		}

		[HttpPost]
		public bool SetAsUnAssigned(int orderId)
		{
			if (orderId > 0)
			{
				var tempAssign = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
				tempAssign.FormanId = null;
				tempAssign.TechnicianId = null;
				tempAssign.DispatcherId = null;
				db.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}

		[HttpPost]
		public bool AssignDispatcherToOrder(string dispatcherId, int orderId)
		{
			if (dispatcherId != null && orderId > 0)
			{
				var tempAssign = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
				tempAssign.DispatcherId = dispatcherId;
				db.SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}

		[HttpGet]
		public JsonResult GetOrderData(int id)
		{
			var tempOrder = db.Orders.Where(x => x.Id == id).FirstOrDefault();
			var address = tempOrder.Address;
			var type = db.Orders.Where(x => x.Id == tempOrder.Id).FirstOrDefault().OrderType;

			string block = "";
			if (address.BlockId != null)
			{
				block = db.Blocks.Where(x => x.Id == address.BlockId).FirstOrDefault().Name;
			}

			OrderBoardViewModel tempOut = new OrderBoardViewModel()
			{
				OrderNo = tempOrder.OrderNo,
				CreatedDateString = Convert.ToDateTime(tempOrder.CreatedDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
				StatusId = tempOrder.StatusId,
				Status = tempOrder.OrderStatu.Name,
				DateTo = tempOrder.AssignedOrders.FirstOrDefault().DateTo,
				DateFrom = tempOrder.AssignedOrders.FirstOrDefault().DateFrom,
				Id = tempOrder.Id,
				CustomerNo = tempOrder.Customer.CustomerNo,
				CustomerName = tempOrder.Customer.Name,
				CustomerPhone = tempOrder.Customer.PhoneOne,
				Block = block
			};


			if (address.PACI != null)
			{
				tempOut.PACI = address.PACI;
			}

			if (type != null)
			{
				tempOut.Type = type.Name;
				tempOut.TypeId = type.Id;
			}
			if (tempOrder.Problem != null)
			{
				tempOut.Problem = tempOrder.Problem.Name;
			}
			if (tempOrder.ProblemId != null)
			{
				tempOut.ProblemId = tempOrder.ProblemId;
			}

			if (address.Area != null)
			{
				tempOut.Area = address.Area.Name;
			}

			if (address.Governorate != null)
			{
				tempOut.Governorate = address.Governorate.Name;
			}
			if (address.StreetId != null)
			{
				var street = db.Street_Jaddah.Where(x => x.Id == address.StreetId).FirstOrDefault().Name;
				tempOut.Street = street;
			}

			return Json(tempOut, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetUnassignedOrders()
		{
			List<OrderBoardViewModel> unAssignedOrderForBoard = new List<OrderBoardViewModel>();
			var unAssignedOrders = db.supervisorUnAssigned(user.Id).ToList();
			for (int i = 0; i < unAssignedOrders.Count; i++)
			{
				OrderBoardViewModel tempOrder = new OrderBoardViewModel()
				{
					PACI = unAssignedOrders[i].PACI,
					CreatedDate = unAssignedOrders[i].CreatedDate,
					DateTo = unAssignedOrders[i].DateTo,
					DateFrom = unAssignedOrders[i].DateFrom,
					Id = unAssignedOrders[i].orderId,
					ProblemId = unAssignedOrders[i].ProblemId,
					StatusId = unAssignedOrders[i].StatusId,
					OrderNo = unAssignedOrders[i].OrderNo,
					Type = unAssignedOrders[i].Type,
					TypeId = unAssignedOrders[i].TypeId,
					Problem = unAssignedOrders[i].Problem,
					Area = unAssignedOrders[i].Area
				};
				unAssignedOrderForBoard.Add(tempOrder);
			}
			var json = JsonConvert.SerializeObject(unAssignedOrderForBoard, Formatting.Indented,
				new JsonSerializerSettings
				{
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore
				});
			return Json(json, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetAssignedOrders()
		{
			var dispatchers = db.SuperDispatchers.Where(x => x.SuperId == user.Id).ToList();
			List<DispatcherBoardViewModel> outDispatchers = new List<DispatcherBoardViewModel>();

			foreach (var dispatcher in dispatchers)
			{
				DispatcherBoardViewModel tempDispatcher = new DispatcherBoardViewModel();
				var dispatcherName = db.AspNetUsers.Where(x => x.Id == dispatcher.DispatcherId).FirstOrDefault().UserName;

				tempDispatcher.Id = dispatcher.DispatcherId;
				tempDispatcher.Name = dispatcherName;

				var formans = db.Formen.Where(x => x.DispatcherId == dispatcher.DispatcherId).ToList();
				var technicians = db.Technicians.Where(x => x.DispatcherId == dispatcher.DispatcherId && x.IsDelete != true).ToList();

				List<FormanBoardViewModel> outFormans = new List<FormanBoardViewModel>();
				foreach (var item in formans)
				{
					var tempTechnicians = technicians.Where(x => x.FormanId == item.Id).ToList();
					List<TechnicianBoardViewModel> tech = new List<TechnicianBoardViewModel>();
					foreach (var techItem in tempTechnicians)
					{
						List<OrderBoardViewModel> orders = new List<OrderBoardViewModel>();
						var tempout = db.getAssignedOrdersForTechs(techItem.Id.ToString()).ToList();
						for (int i = 0; i < tempout.Count; i++)
						{
							OrderBoardViewModel orderDetail = new OrderBoardViewModel()
							{
								Id = tempout[i].orderId,
								CreatedDate = tempout[i].CreatedDate,
								DateFrom = tempout[i].DateFrom,
								DateTo = tempout[i].DateTo,
								OrderNo = tempout[i].OrderNo,
								CustomerName = tempout[i].CustomerName,
								Area = tempout[i].Area,
								Block = tempout[i].Block,
								Problem = tempout[i].Problem,
								ProblemId = tempout[i].ProblemId,
								Type = tempout[i].Type,
								TypeId = tempout[i].TypeId
							};
							orders.Add(orderDetail);
						}

						TechnicianBoardViewModel tempTech = new TechnicianBoardViewModel()
						{
							//Availability = techItem.TV_Availability.Name,
							Id = techItem.Id,
							Name = techItem.Name,
							OrderCounts = techItem.AssignedOrders.Count,
							Orders = orders
						};
						tech.Add(tempTech);
					}

					FormanBoardViewModel temp = new FormanBoardViewModel()
					{
						AllOrderCount = tempTechnicians.Count,
						Name = item.Name,
						Id = item.Id,
						Technicians = tech
					};
					outFormans.Add(temp);
				}
				tempDispatcher.Formans = outFormans;

				var dispatcherUnassigendOrders = db.AssignedOrders.Where(x => x.SupervisorId == user.Id && x.DispatcherId == dispatcher.DispatcherId && x.TechnicianId == null && x.IsClosed == false).ToList();

				List<OrderBoardViewModel> dispatcherUnassigendOrdersList = new List<OrderBoardViewModel>();
				foreach (var order in dispatcherUnassigendOrders)
				{
					var type = db.Orders.Where(x => x.Id == order.OrderId).FirstOrDefault().OrderType;
					var address = order.Order.Address;

					OrderBoardViewModel tempOrder = new OrderBoardViewModel()
					{
						PACI = address.PACI,
						CreatedDate = order.Order.CreatedDate,
						DateTo = order.DateTo,
						DateFrom = order.DateFrom,
						Id = order.Order.Id,
						StatusId = order.Order.StatusId,
						OrderNo = order.Order.OrderNo
					};

					if (order.Order.Problem != null)
					{
						tempOrder.Problem = order.Order.Problem.Name;
						tempOrder.ProblemId = order.Order.ProblemId;
					}

					if (type != null)
					{
						tempOrder.Type = type.Name;
						tempOrder.TypeId = type.Id;
					}
					if (address.Area != null)
					{
						tempOrder.Area = address.Area.Name;
					}
					dispatcherUnassigendOrdersList.Add(tempOrder);
				}
				tempDispatcher.UnAssignedOrders = dispatcherUnassigendOrdersList;

				outDispatchers.Add(tempDispatcher);
			}

			var json = JsonConvert.SerializeObject(outDispatchers, Formatting.Indented,
						new JsonSerializerSettings
						{
							ReferenceLoopHandling = ReferenceLoopHandling.Ignore
						});

			return Json(json, JsonRequestBehavior.AllowGet);
		}
	}
}