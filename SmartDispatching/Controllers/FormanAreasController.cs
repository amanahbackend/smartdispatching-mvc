﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace SmartDispatching.Controllers
{
    public class FormanAreasController : HandlErrorController
	{
        private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: FormanAreas
		public ActionResult Index()
        {
            var formanAreas = db.FormanAreas.Include(f => f.SAPArea).Include(f => f.Forman).Where(x=>x.Forman.SuperDispatcherId == user.Id);
            return View(formanAreas.ToList());
        }

        // GET: FormanAreas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormanArea formanArea = db.FormanAreas.Find(id);
            if (formanArea == null)
            {
                return HttpNotFound();
            }
            return View(formanArea);
        }

        // GET: FormanAreas/Create
        public ActionResult Create()
        {
			var formans = db.Formen.Where(x => x.SuperDispatcherId == user.Id);

            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name");
            ViewBag.FormanId = new SelectList(formans, "Id", "Name");
            return View();
        }

        // POST: FormanAreas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AreaId,FormanId")] FormanArea formanArea)
        {
            if (ModelState.IsValid)
            {
                db.FormanAreas.Add(formanArea);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", formanArea.AreaId);
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", formanArea.FormanId);
            return View(formanArea);
        }

        // GET: FormanAreas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormanArea formanArea = db.FormanAreas.Find(id);
            if (formanArea == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", formanArea.AreaId);
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", formanArea.FormanId);
            return View(formanArea);
        }

        // POST: FormanAreas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AreaId,FormanId")] FormanArea formanArea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formanArea).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaId = new SelectList(db.SAPAreas, "Id", "Name", formanArea.AreaId);
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", formanArea.FormanId);
            return View(formanArea);
        }

        // GET: FormanAreas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormanArea formanArea = db.FormanAreas.Find(id);
            if (formanArea == null)
            {
                return HttpNotFound();
            }
            return View(formanArea);
        }

        // POST: FormanAreas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FormanArea formanArea = db.FormanAreas.Find(id);
            db.FormanAreas.Remove(formanArea);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
