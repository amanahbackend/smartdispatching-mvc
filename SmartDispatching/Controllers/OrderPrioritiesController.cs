﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class OrderPrioritiesController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: OrderPriorities
        public ActionResult Index()
        {
            return View(db.OrderPriorities.ToList());
        }

        // GET: OrderPriorities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderPriority orderPriority = db.OrderPriorities.Find(id);
            if (orderPriority == null)
            {
                return HttpNotFound();
            }
            return View(orderPriority);
        }

        // GET: OrderPriorities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderPriorities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] OrderPriority orderPriority)
        {
            if (ModelState.IsValid)
            {
                db.OrderPriorities.Add(orderPriority);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(orderPriority);
        }

        // GET: OrderPriorities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderPriority orderPriority = db.OrderPriorities.Find(id);
            if (orderPriority == null)
            {
                return HttpNotFound();
            }
            return View(orderPriority);
        }

        // POST: OrderPriorities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] OrderPriority orderPriority)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderPriority).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(orderPriority);
        }

        // GET: OrderPriorities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderPriority orderPriority = db.OrderPriorities.Find(id);
            if (orderPriority == null)
            {
                return HttpNotFound();
            }
            return View(orderPriority);
        }

        // POST: OrderPriorities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderPriority orderPriority = db.OrderPriorities.Find(id);
            db.OrderPriorities.Remove(orderPriority);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
