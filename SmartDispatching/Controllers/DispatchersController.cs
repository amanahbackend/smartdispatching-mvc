﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
	public class DispatchersController : HandlErrorController
	{
		private ApplicationUserManager _userManager;
		private ApplicationDbContext db = new ApplicationDbContext();
		//ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}

		// GET: Users
		public ActionResult Index()
		{
			Dispatching context = new Dispatching();
			ApplicationDbContext contextdb = new ApplicationDbContext();
			var userId = User.Identity.GetUserId();
			var targetDispatcher = context.SuperDispatchers.Where(x => x.SuperId == userId).ToList();
			List < ApplicationUser > dispatchers = new List<ApplicationUser>();
			  
			for (int i = 0; i < targetDispatcher.Count; i++)
			{
				var dispatcherId = targetDispatcher[i].DispatcherId;
				var dispatcher = UserManager.Users.Where(x => x.Id == dispatcherId).FirstOrDefault();
				dispatchers.Add(dispatcher);
			}

			return View(dispatchers);
		}

		// GET: Users/Details/5
		public ActionResult Details(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var applicationUser = db.Users.Find(id);
			var roleId = applicationUser.Roles.Where(x => x.UserId == applicationUser.Id).FirstOrDefault().RoleId;
			var role = db.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
			RegisterViewModel user = new RegisterViewModel()
			{
				UserName = applicationUser.UserName,
				Email = applicationUser.Email,
				RoleId = role
			};
			if (applicationUser == null)
			{
				return HttpNotFound();
			}
			return View(user);
		}

		// GET: Users/Create
		public ActionResult Create()
		{
			ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name");
			return View();
		}

		//
		// POST: /Account/Register
		//[HttpPost]
		//[AllowAnonymous]
		//[ValidateAntiForgeryToken]
		//public async Task<ActionResult> Register(RegisterViewModel model)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
		//		var result = await UserManager.CreateAsync(user, model.Password);
		//		if (result.Succeeded)
		//		{
		//			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
		//			string roleName = roleManager.Roles.Where(x => x.Id == model.RoleId).FirstOrDefault().Name;
		//			await UserManager.AddToRoleAsync(user.Id, roleName);
		//			await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
		//			// For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
		//			// Send an email with this link
		//			// string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
		//			// var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
		//			// await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

		//			return RedirectToAction("Index", "Home");
		//		}
		//		AddErrors(result);
		//	}

		//	// If we got this far, something failed, redisplay form
		//	return View(model);
		//}

		// POST: Users/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(RegisterViewModel model)
		{
			var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
			var result = await UserManager.CreateAsync(user, model.Password);
			if (result.Succeeded)
			{
				var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
				string roleName = roleManager.Roles.Where(x => x.Id == model.RoleId).FirstOrDefault().Name;
				await UserManager.AddToRoleAsync(user.Id, roleName);

				Dispatching context = new Dispatching();
				var newUser = context.AspNetUsers.Where(x => x.UserName == model.UserName && x.Email == model.Email).FirstOrDefault();
				newUser.RoleId = Convert.ToInt32(model.RoleId);
				context.SaveChanges();

				var userId = User.Identity.GetUserId();

				SuperDispatcher temp = new SuperDispatcher()
				{
					DispatcherId = newUser.Id,
					SuperId = userId
				};
				context.SuperDispatchers.Add(temp);
				context.SaveChanges();

				return View("Index", db.Users.ToList());
			}
			//db.Users.Add(applicationUser);
			//            db.SaveChanges();
			return View(model);
		}

		// GET: Users/Edit/5
		public ActionResult Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser applicationUser = db.Users.Find(id);
			var roleId = applicationUser.Roles.Where(x => x.UserId == applicationUser.Id).FirstOrDefault().RoleId;
			var role = db.Roles.Where(x => x.Id == roleId).FirstOrDefault().Name;
			RegisterViewModel user = new RegisterViewModel()
			{
				UserName = applicationUser.UserName,
				Email = applicationUser.Email,
				RoleId = role
			};
			SelectListItem selected = new SelectListItem()
			{
				Selected = true,
				Text = role,
				Value = roleId
			};
			if (applicationUser == null)
			{
				return HttpNotFound();
			}
			ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", selected);
			return View(user);
		}

		// POST: Users/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
		{
			if (ModelState.IsValid)
			{
				db.Entry(applicationUser).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(applicationUser);
		}

		// GET: Users/Delete/5
		public ActionResult Delete(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser applicationUser = db.Users.Find(id);
			if (applicationUser == null)
			{
				return HttpNotFound();
			}
			return View(applicationUser);
		}

		// POST: Users/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(string id)
		{
			Dispatching dbcontext = new Dispatching();
			var superDispatcher = dbcontext.SuperDispatchers.Where(x => x.DispatcherId == id).FirstOrDefault();
			dbcontext.SuperDispatchers.Remove(superDispatcher);
			dbcontext.SaveChanges();

			ApplicationUser applicationUser = db.Users.Find(id);
			db.Users.Remove(applicationUser);
			db.SaveChanges();

			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
