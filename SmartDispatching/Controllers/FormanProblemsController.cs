﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace SmartDispatching.Controllers
{
    public class FormanProblemsController : HandlErrorController
	{
        private Dispatching db = new Dispatching();
		ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		// GET: FormanProblems
		public ActionResult Index()
        {
            var formanProblems = db.FormanProblems.Include(f => f.Forman).Include(f => f.Problem).Where(x=>x.Forman.SuperDispatcherId == user.Id);
            return View(formanProblems.ToList());
        }

        // GET: FormanProblems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormanProblem formanProblem = db.FormanProblems.Find(id);
            if (formanProblem == null)
            {
                return HttpNotFound();
            }
            return View(formanProblem);
        }

        // GET: FormanProblems/Create
        public ActionResult Create()
        {
			var formans = db.Formen.Where(x => x.SuperDispatcherId == user.Id);

			ViewBag.FormanId = new SelectList(formans, "Id", "Name");
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name");
            return View();
        }

        // POST: FormanProblems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProblemId,FormanId")] FormanProblem formanProblem)
        {
            if (ModelState.IsValid)
            {
                db.FormanProblems.Add(formanProblem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", formanProblem.FormanId);
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", formanProblem.ProblemId);
            return View(formanProblem);
        }

        // GET: FormanProblems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormanProblem formanProblem = db.FormanProblems.Find(id);
            if (formanProblem == null)
            {
                return HttpNotFound();
            }
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", formanProblem.FormanId);
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", formanProblem.ProblemId);
            return View(formanProblem);
        }

        // POST: FormanProblems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProblemId,FormanId")] FormanProblem formanProblem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formanProblem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FormanId = new SelectList(db.Formen, "Id", "Name", formanProblem.FormanId);
            ViewBag.ProblemId = new SelectList(db.Problems, "Id", "Name", formanProblem.ProblemId);
            return View(formanProblem);
        }

        // GET: FormanProblems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormanProblem formanProblem = db.FormanProblems.Find(id);
            if (formanProblem == null)
            {
                return HttpNotFound();
            }
            return View(formanProblem);
        }

        // POST: FormanProblems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FormanProblem formanProblem = db.FormanProblems.Find(id);
            db.FormanProblems.Remove(formanProblem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
