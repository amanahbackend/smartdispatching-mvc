﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
	public class Super_R_DispatchersController : HandlErrorController
	{
		private Dispatching db = new Dispatching();

		// GET: Super_R_Dispatchers
		public ActionResult Index()
		{
			var superDispatchers = db.SuperDispatchers.ToList();
			var users = db.AspNetUsers.ToList();
			foreach (var item in superDispatchers)
			{
				foreach (var user in users)
				{
					if (user.Id == item.DispatcherId)
					{
						item.DispatcherId = user.UserName;
					}
					if (user.Id == item.SuperId)
					{
						item.SuperId = user.UserName;
					}
				}
			}
			return View(superDispatchers);
		}

		// GET: Super_R_Dispatchers/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			SuperDispatcher superDispatcher = db.SuperDispatchers.Find(id);
			if (superDispatcher == null)
			{
				return HttpNotFound();
			}
			return View(superDispatcher);
		}

		// GET: Super_R_Dispatchers/Create
		public ActionResult Create()
		{
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5);
			var supers = db.AspNetUsers.Where(x => x.RoleId == 4);

			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName");
			ViewBag.SuperId = new SelectList(supers, "Id", "UserName");
			return View();
		}

		// POST: Super_R_Dispatchers/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,SuperId,DispatcherId")] SuperDispatcher superDispatcher)
		{
			if (ModelState.IsValid)
			{
				var isFound = db.SuperDispatchers.Where(x => x.DispatcherId == superDispatcher.DispatcherId).FirstOrDefault();
				if (isFound == null)
				{
					var firstFound = db.SuperDispatchers.Where(x => x.DispatcherId == null && x.SuperId == superDispatcher.SuperId).FirstOrDefault();
					if (firstFound == null)
					{
						var divisionId = db.SuperDispatchers.Where(x => x.SuperId == superDispatcher.SuperId).FirstOrDefault().DivisionId;
						superDispatcher.DivisionId = divisionId;
						db.SuperDispatchers.Add(superDispatcher);
						db.SaveChanges();
					}
					else
					{
						firstFound.DispatcherId = superDispatcher.DispatcherId;
						db.SaveChanges();
					}

					return RedirectToAction("Index");
				}
				else
				{
					ModelState.AddModelError("", "This dispatcher assigned to supervisor before!");
				}
			}
			var dispatchers = db.AspNetUsers.Where(x => x.RoleId == 5);
			var supers = db.AspNetUsers.Where(x => x.RoleId == 4);

			ViewBag.DispatcherId = new SelectList(dispatchers, "Id", "UserName", superDispatcher.DispatcherId);
			ViewBag.SuperId = new SelectList(supers, "Id", "UserName", superDispatcher.SuperId);
			return View(superDispatcher);
		}

		// GET: Super_R_Dispatchers/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			SuperDispatcher superDispatcher = db.SuperDispatchers.Find(id);
			if (superDispatcher == null)
			{
				return HttpNotFound();
			}
			return View(superDispatcher);
		}

		// POST: Super_R_Dispatchers/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,SuperId,DispatcherId")] SuperDispatcher superDispatcher)
		{
			if (ModelState.IsValid)
			{
				db.Entry(superDispatcher).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(superDispatcher);
		}

		public ActionResult ChangeSuperDivison(string id)
		{
			var supers = db.AspNetUsers.Where(x => x.RoleId == 4);
			var superDivison = db.SuperDispatchers.Where(x => x.SuperId == id).FirstOrDefault().DivisionId;
			ViewBag.SuperId = new SelectList(supers, "Id", "UserName", id);
			ViewBag.DivisionId = new SelectList(db.Divisions, "Id", "Name", superDivison);

			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ChangeSuperDivison(SuperDispatcher superDispatcher)
		{
			var supers = db.SuperDispatchers.Where(x => x.SuperId == superDispatcher.SuperId).ToList();
			for (int i = 0; i < supers.Count; i++)
			{
				supers[i].DivisionId = superDispatcher.DivisionId;
				db.SaveChanges();
			}
			return RedirectToAction("Index", "Users");
		}

		// GET: Super_R_Dispatchers/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			SuperDispatcher superDispatcher = db.SuperDispatchers.Find(id);
			if (superDispatcher == null)
			{
				return HttpNotFound();
			}
			return View(superDispatcher);
		}

		// POST: Super_R_Dispatchers/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			SuperDispatcher superDispatcher = db.SuperDispatchers.Find(id);
			db.SuperDispatchers.Remove(superDispatcher);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
