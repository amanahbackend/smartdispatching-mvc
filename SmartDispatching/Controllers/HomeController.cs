﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartDispatching.Controllers
{
    public class HomeController : HandlErrorController
	{
		//ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

		public ActionResult Index()
		{
            if (User.IsInRole("Dispatcher"))
            {
                return RedirectToAction("Index", "Dashboard");
            }
            else if (User.IsInRole("Supervisor"))
            {
                return RedirectToAction("Index", "SuperDashboard");
            }
            else if (User.IsInRole("Manager"))
            {
                return RedirectToAction("Index", "Manager");
            }
            else if (User.IsInRole("IT"))
            {
                return RedirectToAction("Index", "ITManagement");
            }
            else if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Users");
            }
            return RedirectToAction("Login", "Account");
        }

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}