﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SmartDispatching.DB.DataBase;

namespace SmartDispatching.Controllers
{
    public class TVAvailabilityController : HandlErrorController
	{
        private Dispatching db = new Dispatching();

        // GET: TVAvailability
        public ActionResult Index()
        {
            return View(db.TV_Availability.ToList());
        }

        // GET: TVAvailability/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TV_Availability tV_Availability = db.TV_Availability.Find(id);
            if (tV_Availability == null)
            {
                return HttpNotFound();
            }
            return View(tV_Availability);
        }

        // GET: TVAvailability/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TVAvailability/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] TV_Availability tV_Availability)
        {
            if (ModelState.IsValid)
            {
                db.TV_Availability.Add(tV_Availability);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tV_Availability);
        }

        // GET: TVAvailability/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TV_Availability tV_Availability = db.TV_Availability.Find(id);
            if (tV_Availability == null)
            {
                return HttpNotFound();
            }
            return View(tV_Availability);
        }

        // POST: TVAvailability/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] TV_Availability tV_Availability)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tV_Availability).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tV_Availability);
        }

        // GET: TVAvailability/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TV_Availability tV_Availability = db.TV_Availability.Find(id);
            if (tV_Availability == null)
            {
                return HttpNotFound();
            }
            return View(tV_Availability);
        }

        // POST: TVAvailability/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TV_Availability tV_Availability = db.TV_Availability.Find(id);
            db.TV_Availability.Remove(tV_Availability);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
