﻿using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SmartDispatching.Controllers
{
	public class SuperManagementController : HandlErrorController
	{
		// GET: SuperManagement
		public ActionResult Index()
		{
			return View();
		}

		// GET: SuperManagement/Details/5
		public ActionResult NotificationTime()
		{
			Dispatching db = new Dispatching();
			var notificationObj = db.NotificationTimes.FirstOrDefault();
			ViewBag.Time = notificationObj.Time;
			return View();
		}

		// GET: SuperManagement/Create
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult NotificationTime(NotificationTimeModel nTime, FormCollection collection)
		{
			Dispatching db = new Dispatching();
			var notificationObj = db.NotificationTimes.FirstOrDefault();
			notificationObj.Time = nTime.Time;
			db.SaveChanges();
			//var configuration = WebConfigurationManager.OpenWebConfiguration("~");
			//var section = (AppSettingsSection)configuration.GetSection("appSettings");
			//section.Settings["NotificationPeriod"].Value = nTime.Time;
			//configuration.Save();
			return RedirectToAction("Index");
		}

		// POST: SuperManagement/Create
		[HttpPost]
		public ActionResult Create(FormCollection collection)
		{
			try
			{
				// TODO: Add insert logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		// GET: SuperManagement/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: SuperManagement/Edit/5
		[HttpPost]
		public ActionResult Edit(int id, FormCollection collection)
		{
			try
			{
				// TODO: Add update logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		// GET: SuperManagement/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: SuperManagement/Delete/5
		[HttpPost]
		public ActionResult Delete(int id, FormCollection collection)
		{
			try
			{
				// TODO: Add delete logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
	}
}
