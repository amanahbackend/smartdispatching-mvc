﻿using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Transactions;

namespace SmartDispatching.Helper
{
	public static class Tasker
	{
		private static PACIModel GetLocationByPACI(string PACI)
		{
			try
			{
				var baseUrl = ConfigurationManager.AppSettings["BaseURL"];
				var URL = baseUrl + "/proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/Hosted/PACIGeocoder/FeatureServer/0/query?where=civilid+%3D+%27" + PACI + "%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&gdbVersion=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=&resultOffset=&resultRecordCount=&f=pjson";
				WebRequest request = WebRequest.Create(URL);
				request.Timeout = 5000;
				WebResponse response = request.GetResponse();
				Stream dataStream = response.GetResponseStream();
				// Open the stream using a StreamReader for easy access.  
				StreamReader reader = new StreamReader(dataStream);
				// Read the content.  
				string responseFromServer = reader.ReadToEnd();
				JavaScriptSerializer oJS = new JavaScriptSerializer();
				PACIModel oRootObject = new PACIModel();
				oRootObject = oJS.Deserialize<PACIModel>(responseFromServer);

				return oRootObject;
			}
			catch
			{
				return null;
			}
		}
		public static bool AutomationAssignOrderToDispatcher(Order createdOrder, RowData address, int orderId)
		{
			try
			{
				using (Dispatching db = new Dispatching())
				{
					List<DispatcherData> finalMatchedDispatchers = new List<DispatcherData>();

					if (address.AreaCode != null || address.AreaCode != 0)
					{
						if (createdOrder.ProblemId != null)
						{
							finalMatchedDispatchers = db.DispatcherDatas.Where(x => x.AreaId == address.AreaCode && x.ProblemId == createdOrder.ProblemId && x.DivisionId == createdOrder.DivisionId).ToList();
						}
					}
					var selectedDispatcher = "";
					if (finalMatchedDispatchers.Count > 0)
					{
						if (finalMatchedDispatchers.Count == 1)
						{
							selectedDispatcher = finalMatchedDispatchers[0].DispatcherId;
						}
						else
						{
							var random = new Random();
							int index = random.Next(finalMatchedDispatchers.Count);
							selectedDispatcher = finalMatchedDispatchers[index].DispatcherId;
						}

						var superId = db.SuperDispatchers.Where(x => x.DispatcherId == selectedDispatcher).FirstOrDefault().SuperId;
						AssignedOrder tempDO = new AssignedOrder()
						{
							SupervisorId = superId,
							DispatcherId = selectedDispatcher,
							OrderId = orderId,
							IsClosed = false
						};
						db.AssignedOrders.Add(tempDO);
						db.SaveChanges();
					}
					else
					{
						if (createdOrder.DivisionId != null)
						{
							var random = new Random();

							var supersByDivision = db.SuperDispatchers.Where(x => x.DivisionId == createdOrder.DivisionId).ToList();

							int index = random.Next(supersByDivision.Count);

							if (supersByDivision.Count > 0)
							{
								var selectedSuper = supersByDivision[index].SuperId;

								AssignedOrder temp = new AssignedOrder()
								{
									SupervisorId = selectedSuper,
									OrderId = orderId,
									IsClosed = false
								};
								db.AssignedOrders.Add(temp);
								db.SaveChanges();
							}
						}
					}
					return true;
				}
			}
			catch (DbEntityValidationException e)
			{
				foreach (var eve in e.EntityValidationErrors)
				{
					Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
						eve.Entry.Entity.GetType().Name, eve.Entry.State);
					foreach (var ve in eve.ValidationErrors)
					{
						Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
							ve.PropertyName, ve.ErrorMessage);
					}
				}
				return false;
			}
		}


		public static void MoveFile(string fileName, StreamReader reader)
		{
			var targetFolder = ConfigurationManager.AppSettings["TargetFolder"];
			var destFolder = ConfigurationManager.AppSettings["DestinationFolder"];

			//string fileName = "FinalUploadFile.xls";
			//string sourcePath = @"C:\Users\ahmon\Desktop";
			//string targetPath = @"C:\Users\ahmon\Desktop\archive";

			// Use Path class to manipulate file and directory paths.
			string sourceFile = System.IO.Path.Combine(targetFolder, fileName);
			string destFile = System.IO.Path.Combine(destFolder, fileName);
			try
			{
				reader.Dispose();
				File.Move(sourceFile, destFile);
			}
			catch
			{
				reader.Dispose();
			}
		}

		public static IEnumerable<TSource> DistinctBy<TSource, TKey>
	(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			HashSet<TKey> seenKeys = new HashSet<TKey>();
			foreach (TSource element in source)
			{
				if (seenKeys.Add(keySelector(element)))
				{
					yield return element;
				}
			}
		}

		public static void CSVMainFunction()
		{
			try
			{
				var targerFolder = ConfigurationManager.AppSettings["TargetFolder"];

				string[] files = Directory.GetFiles(targerFolder, "*.csv");

				if (files.Length > 0)
				{

					for (int i = 0; i < files.Length; i++)
						files[i] = Path.GetFileName(files[i]);

					//string[] files = System.IO.Directory.GetFiles(targerFolder, "*.xls");
					for (int j = 0; j < files.Length; j++)
					{
						var fileName = System.IO.Path.Combine(targerFolder, files[j]);

						var reader = new StreamReader(File.OpenRead(fileName));
						List<OrderViewModel> orderList = new List<OrderViewModel>();
						List<RowData> tempRowList = new List<RowData>();
						bool isFirst = true;
						while (!reader.EndOfStream)
						{
							var line = reader.ReadLine();
							var values = line.Split(',');

							if (isFirst == false)
							{
								if (values != null && values.Length > 0)
								{

									try
									{

										DateTime? cDate = null;
										//	DateTime? rDate = null;
										if (values[5] != "")
										{
											var date = values[5].ToString();
											int len = date.Length;
											cDate = DateTime.ParseExact(values[5].ToString(), "dd.MM.yyyy HH:mm:ss", null);
										}

										OrderViewModel temp = new OrderViewModel();
										temp.OrderNo = values[0].ToString();
										if (values[1] != "")
										{
											temp.OrderTypeCode = values[1].ToString();
										}
										if (values[2] != "")
										{
											temp.OrderTypeName = values[2].ToString();
										}
										if (values[4] != "")
										{
											temp.CompanyCodeName = values[4].ToString();
										}
										temp.OrderDate = cDate;
										if (values[6] != "")
										{
											temp.DivisionCode = Convert.ToInt32(values[6]);
											temp.DivisionName = values[7].ToString();
										}
										if (values[8] != "")
										{
											temp.PriorityId = Convert.ToInt32(values[8]);
										}
										if (values[9] != "")
										{
											temp.OrderPriorityDescription = values[9].ToString();
										}
										if (values[10] != "")
										{
											temp.ProblemCode = values[10].ToString();
										}
										if (values[11] != "")
										{
											temp.ProblemDescription = values[11].ToString();
										}
										if (values[19] != "")
										{
											temp.OrderStatusDescription = values[19];
										}
										if (values[20] != "")
										{
											temp.Notes_ICAgent = values[20].ToString();
										}
										if (values[21] != "")
										{
											temp.ContractNo = values[21].ToString();
										}
										if (values[22] != "")
										{
											temp.ContractTypeCode = values[22].ToString();
										}
										if (values[23] != "")
										{
											temp.ContractTypeName = values[23].ToString();
										}
										if (values[24] != "")
										{
											DateTime cCDate = DateTime.ParseExact(values[24], "dd.MM.yyyy", null);
											temp.CreatedDate = cCDate;
										}
										if (values[25] != "")
										{
											DateTime cCDate = DateTime.ParseExact(values[25].ToString(), "dd.MM.yyyy", null);
											temp.ExpirationDate = cCDate;
										}
										if (values[26] != "")
										{
											temp.Functional_Location = values[26].ToString();
										}
										if (values[28] != "")
										{
											temp.CustomerNo = values[28].ToString();
										}
										if (values[29] != "")
										{
											temp.Name = values[29].ToString();
										}
										if (values[30] != "")
										{
											temp.PhoneOne = values[30].ToString();
										}
										if (values[31] != "")
										{
											temp.PhoneTwo = values[31].ToString();
										}
										if (values[32] != "")
										{
											temp.PACI = values[32].ToString();
										}
										if (values[34] != "")
										{
											try
											{
												temp.AreaIdFile = Convert.ToInt32(values[34]);
											}
											catch
											{

											}
										}
										if (values[35] != "")
										{
											temp.AreaDescription = values[35].ToString();
										}
										if (values[36] != "")
										{
											try
											{
												temp.BlockName = values[36].ToString();
											}
											catch
											{
												temp.BlockName = null;
											}
										}
										if (values[38] != "")
										{
											temp.StreetName = values[38].ToString();
										}
										if (values[39] != "")
										{
											temp.House_Kasima = values[39].ToString();
										}
										if (values[40] != "")
										{
											temp.Floor = values[40].ToString();
										}
										if (values[41] != "")
										{
											temp.AppartmentNo = values[41].ToString();
										}

										orderList.Add(temp);

										RowData tempRow = new RowData();

										tempRow.CustomerNo = temp.CustomerNo;
										tempRow.CustomerName = temp.Name;
										tempRow.PhoneOne = temp.PhoneOne;
										tempRow.PhoneTwo = temp.PhoneTwo;
										tempRow.ContractNo = temp.ContractNo;
										tempRow.ContractType = temp.ContractTypeCode;
										tempRow.ContractTypeDescription = temp.ContractTypeName;
										tempRow.ContractDate = temp.CreatedDate;
										tempRow.ContactExpiraion = temp.ExpirationDate;
										tempRow.FunctionalLocation = temp.Functional_Location;
										tempRow.PACI = temp.PACI;
										tempRow.Governorate = temp.GovId;
										tempRow.AreaCode = temp.AreaIdFile;
										tempRow.AreaDescription = temp.AreaDescription;
										tempRow.Block = temp.BlockName;
										tempRow.House = temp.House_Kasima;
										tempRow.Floor = temp.Floor;
										tempRow.AppartmentNo = temp.AppartmentCode;
										tempRow.AddressNote = temp.AddressNote;
										tempRow.OrderNo = temp.OrderNo;
										tempRow.OrderType = temp.OrderTypeCode;
										tempRow.OrderTypeDescription = temp.OrderTypeName;
										tempRow.CompanyCode = temp.CompanyCodeName;
										if (cDate != null)
										{
											tempRow.OrderDate = cDate;
										}
										tempRow.Division = temp.DivisionCode;
										tempRow.DivisionDescription = temp.DivisionName;
										tempRow.OrderPriority = temp.PriorityId;
										tempRow.OrderPriorityDescription = temp.OrderPriorityDescription;
										tempRow.Problem = temp.ProblemCode;
										tempRow.ProblemDescription = temp.ProblemDescription;
										tempRow.OrderNoteAgent = temp.Notes_ICAgent;
										tempRow.OrderStatus = "Open";
										tempRow.Street = temp.StreetName;
										tempRowList.Add(tempRow);
									}
									catch (DbEntityValidationException e)
									{
										foreach (var eve in e.EntityValidationErrors)
										{
											Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
												eve.Entry.Entity.GetType().Name, eve.Entry.State);
											foreach (var ve in eve.ValidationErrors)
											{
												Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
													ve.PropertyName, ve.ErrorMessage);
											}
										}
									}
								}
							}
							else
							{
								isFirst = false;
							}
						}
						var distinctRows = orderList.DistinctBy(x => x.OrderNo).ToList();
						var distinctRowsTemp = tempRowList.DistinctBy(x => x.OrderNo).ToList();

						for (int i = 0; i < distinctRows.Count; i++)
						{
							DashboardHelper.LogReader(distinctRows[i].OrderNo, "Temp");
							using (Dispatching db = new Dispatching())
							{
								var temp = distinctRows[i];
								try
								{
									db.RowDatas.Add(distinctRowsTemp[i]);
									db.SaveChanges();
								}
								catch (Exception e)
								{
									Console.WriteLine(e.Message);
								}
								var isFound = db.Orders.Where(x => x.OrderNo == temp.OrderNo).FirstOrDefault();
								if (isFound == null)
								{
									if (temp.Name != null && temp.PhoneOne != null && temp.AreaIdFile != 0)
									{
										Customer tempCustomer = new Customer()
										{
											CustomerNo = temp.CustomerNo,
											Name = temp.Name,
											PhoneOne = temp.PhoneOne,
											PhoneTwo = temp.PhoneTwo
										};

										try
										{
											db.Customers.Add(tempCustomer);
											db.SaveChanges();
										}
										catch (DbEntityValidationException e)
										{
											foreach (var eve in e.EntityValidationErrors)
											{
												Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
													eve.Entry.Entity.GetType().Name, eve.Entry.State);
												foreach (var ve in eve.ValidationErrors)
												{
													Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
														ve.PropertyName, ve.ErrorMessage);
												}
											}
										}


										var customerId = db.Customers.Where(x => x.PhoneOne == temp.PhoneOne && x.Name == temp.Name).FirstOrDefault().Id;
										if (temp.ContractTypeName != null && temp.CreatedDate != null && temp.ExpirationDate != null)
										{
											var tCode = temp.ContractTypeCode;
											var contractType = db.ContractTypes.Where(x => x.Code == tCode).FirstOrDefault();
											Contract tempContract = new Contract()
											{
												ContractNo = temp.ContractNo,
												CreatedDate = temp.CreatedDate,
												ExpirationDate = temp.ExpirationDate,
												CustomerId = customerId
											};
											if (contractType != null)
											{
												tempContract.TypeId = contractType.Id;
											}

											try
											{
												db.Contracts.Add(tempContract);
												db.SaveChanges();
											}
											catch (DbEntityValidationException e)
											{
												foreach (var eve in e.EntityValidationErrors)
												{
													Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
														eve.Entry.Entity.GetType().Name, eve.Entry.State);
													foreach (var ve in eve.ValidationErrors)
													{
														Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
															ve.PropertyName, ve.ErrorMessage);
													}
												}
											}
										}

										Address tempAddress = new Address()
										{
											AddressNote = temp.AddressNote,
											AppartmentNo = temp.AppartmentNo,
											Functional_Location = temp.Functional_Location,
											CustomerId = customerId
										};

										if (temp.PACI != null)
										{
											PACIModel tempPaci = GetLocationByPACI(temp.PACI);
											if (tempPaci != null && tempPaci.features != null && tempPaci.features.Count > 0)
											{
												tempAddress.PACI = tempPaci.features[0].attributes.civilid;
												temp.GovName = tempPaci.features[0].attributes.governorateenglish;
												temp.BlockName = tempPaci.features[0].attributes.blockenglish;
												temp.StreetName = tempPaci.features[0].attributes.streetenglish;
												var areaid = tempPaci.features[0].attributes.neighborhoodid;
												var areaname = tempPaci.features[0].attributes.neighborhoodenglish;
												tempAddress.AppartmentNo = tempPaci.features[0].attributes.houseenglish;
												tempAddress.Floor = tempPaci.features[0].attributes.floor_no;
												tempAddress.House_Kasima = tempPaci.features[0].attributes.houseenglish;

												int? areaID;
												if (areaid != null)
												{
													int id = Convert.ToInt32(areaid);
													var areaTemp = db.Areas.Where(x => x.OriginalId == id).FirstOrDefault();
													if (areaTemp != null)
													{
														areaID = areaTemp.Id;
														tempAddress.AreaId = areaID;
													}
													else
													{
														Area tempArea = new Area()
														{
															Name = areaname,
															OriginalId = id
														};
														db.Areas.Add(tempArea);
														db.SaveChanges();
														tempAddress.AreaId = tempArea.Id;
													}
												}

												if (temp.GovName != null)
												{
													var name = temp.GovName;
													var govTemp = db.Governorates.Where(x => x.Name == name).FirstOrDefault();
													if (govTemp != null)
													{
														tempAddress.GovId = govTemp.Id;
													}
													else
													{
														Governorate tempGov = new Governorate()
														{
															Name = temp.GovName
														};
														db.Governorates.Add(tempGov);
														db.SaveChanges();
														tempAddress.GovId = tempGov.Id;
													}
												}
												else if (temp.GovId != null)
												{
													var id = temp.GovId;
													var govTemp = db.Governorates.Where(x => x.Id == id).FirstOrDefault();
													if (govTemp != null)
													{
														tempAddress.GovId = govTemp.Id;
													}
													else
													{
														Governorate tempGov = new Governorate()
														{
															Name = temp.GovName
														};
														db.Governorates.Add(tempGov);
														db.SaveChanges();
														tempAddress.GovId = tempGov.Id;
													}
												}

												if (temp.BlockName != null)
												{
													var name = temp.BlockName;
													var blockTemp = db.Blocks.Where(x => x.Name == name).FirstOrDefault();
													if (blockTemp != null)
													{
														tempAddress.BlockId = blockTemp.Id;
													}
													else
													{
														Block tempBlock = new Block()
														{
															Name = temp.BlockName,
															BlockNo = temp.BlockName
														};
														db.Blocks.Add(tempBlock);
														db.SaveChanges();
														tempAddress.BlockId = tempBlock.Id;
													}
												}
												else if (temp.BlockId > 0)
												{
													var id = temp.BlockId;
													var blockTemp = db.Blocks.Where(x => x.Name == id.ToString()).FirstOrDefault();
													if (blockTemp != null)
													{
														tempAddress.BlockId = blockTemp.Id;
													}
													else
													{
														Block tempBlock = new Block()
														{
															Name = temp.BlockName,
															BlockNo = temp.BlockName
														};
														db.Blocks.Add(tempBlock);
														db.SaveChanges();
														tempAddress.BlockId = temp.Id;
													}
												}

												if (temp.StreetName != null)
												{
													var name = temp.StreetName;
													var streetTemp = db.Street_Jaddah.Where(x => x.Name == name).FirstOrDefault();
													if (streetTemp != null)
													{
														tempAddress.StreetId = streetTemp.Id;
													}
													else
													{
														Street_Jaddah tempStreet = new Street_Jaddah()
														{
															Name = temp.StreetName
														};
														db.Street_Jaddah.Add(tempStreet);
														db.SaveChanges();
														tempAddress.StreetId = tempStreet.Id;
													}
												}

												tempAddress.Lat = tempPaci.features[0].attributes.lat;
												tempAddress.Long = tempPaci.features[0].attributes.lon;
											}
											else
											{

												//int? areaID;
												//if (temp.AreaIdFile != null)
												//{
												//	var areaTemp = db.Areas.Where(x => x.OriginalId == temp.AreaIdFile).FirstOrDefault();
												//	if (areaTemp != null)
												//	{
												//		areaID = areaTemp.Id;
												//		tempAddress.AreaId = areaID;
												//	}
												//}

												//tempAddress.BlockId = temp.BlockId;
												//tempAddress.GovId = temp.GovId;
												//tempAddress.Floor = temp.Floor;
												////tempAddress.PACI = temp.PACI;
												//tempAddress.StreetId = temp.StreetId;
												//tempAddress.House_Kasima = temp.House_Kasima;
											}
										}
										else
										{

											//int? areaID;
											//if (temp.AreaIdFile != null)
											//{
											//	var areaTemp = db.Areas.Where(x => x.OriginalId == temp.AreaIdFile).FirstOrDefault();
											//	if (areaTemp != null)
											//	{
											//		areaID = areaTemp.Id;
											//		tempAddress.AreaId = areaID;
											//	}
											//}
											//tempAddress.BlockId = temp.BlockId;
											//tempAddress.GovId = temp.GovId;
											//tempAddress.Floor = temp.Floor;
											//tempAddress.PACI = temp.PACI;
											//tempAddress.StreetId = temp.StreetId;
											//tempAddress.House_Kasima = temp.House_Kasima;

										}

										//if (tempAddress.PACI != null && tempAddress.PACI != "0" && tempAddress.PACI != "null")
										//{
										//	PACIModel tempPaci = GetLocationByPACI(tempAddress.PACI);
										//	if (tempPaci != null && tempPaci.features != null && tempPaci.features.Count > 0)
										//	{
										//		tempAddress.Lat = tempPaci.features[0].attributes.lat;
										//		tempAddress.Long = tempPaci.features[0].attributes.lon;
										//	}
										//}
										//int? areaID;
										//if (temp.AreaIdFile != null)
										//{
										//	var areaTemp = db.Areas.Where(x => x.OriginalId == temp.AreaIdFile).FirstOrDefault();
										//	if (areaTemp != null)
										//	{
										//		areaID = areaTemp.Id;
										//		tempAddress.AreaId = areaID;
										//	}
										//}


										try
										{
											db.Addresses.Add(tempAddress);
											db.SaveChanges();
										}
										catch (DbEntityValidationException e)
										{
											foreach (var eve in e.EntityValidationErrors)
											{
												Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
													eve.Entry.Entity.GetType().Name, eve.Entry.State);
												foreach (var ve in eve.ValidationErrors)
												{
													Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
														ve.PropertyName, ve.ErrorMessage);
												}
											}
										}

										var createdDate = DateTime.Now;
										int? problemId = null;
										if (temp.ProblemCode != null)
										{
											var problem = db.Problems.Where(x => x.Code == temp.ProblemCode).FirstOrDefault();
											if (problem != null)
											{
												problemId = problem.Id;
											}
										}
										int? orderTypeId = null;
										if (temp.OrderTypeCode != null)
										{
											var tempType = db.OrderTypes.Where(x => x.Code == temp.OrderTypeCode).FirstOrDefault();
											if (tempType != null)
											{
												orderTypeId = tempType.Id;
											}
										}
										int? companyCodeId = null;
										if (temp.CompanyCodeName != null)
										{
											var tempComp = db.CompanyCodes.Where(x => x.Name == temp.CompanyCodeName).FirstOrDefault();
											if (tempComp != null)
											{
												companyCodeId = tempComp.Id;
											}
										}
										int? divisionId = null;
										if (temp.DivisionName != null)
										{
											var division = db.Divisions.Where(x => x.Name == temp.DivisionName).FirstOrDefault();
											if (division != null)
											{
												divisionId = division.Id;
											}
										}

										int? priorityId = null;
										if (temp.PriorityId != null)
										{
											var id = Convert.ToInt32(temp.PriorityId);
											var tempPriority = db.OrderPriorities.Where(x => x.Id == id).FirstOrDefault();
											if (tempPriority != null)
											{
												priorityId = tempPriority.Id;
											}
										}

										//if (temp.OrderStatusDescription != "" )
										//{
										//	var tempStatus = db.OrderStatus.Where(x => x.Name == temp.OrderStatusDescription).FirstOrDefault();
										//	if (tempStatus != null)
										//	{
										//		temp.StatusId = tempStatus.Id;
										//	}
										//}

										Order tempOrder = new Order()
										{
											OrderNo = temp.OrderNo,
											CustomerId = customerId,
											Notes_ICAgent = temp.Notes_ICAgent,
											ProblemId = problemId,
											StatusId = 1,
											AddressId = tempAddress.Id
										};
										if (temp.OrderDate != null)
										{
											tempOrder.CreatedDate = temp.OrderDate;
										}

										if (priorityId != null)
										{
											tempOrder.PriorityId = priorityId;
										}

										if (divisionId != null)
										{
											tempOrder.DivisionId = Convert.ToInt32(divisionId);
										}
										if (companyCodeId != null)
										{
											tempOrder.CompanyCodeId = Convert.ToInt32(companyCodeId);
										}
										if (orderTypeId != null)
										{
											tempOrder.TypeId = Convert.ToInt32(orderTypeId);
										}
										try
										{
											db.Orders.Add(tempOrder);
											db.SaveChanges();
										}
										catch (DbEntityValidationException e)
										{
											foreach (var eve in e.EntityValidationErrors)
											{
												Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
													eve.Entry.Entity.GetType().Name, eve.Entry.State);
												foreach (var ve in eve.ValidationErrors)
												{
													Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
														ve.PropertyName, ve.ErrorMessage);
												}
											}
										}
										DashboardHelper.LogReader(tempOrder.OrderNo, "Order");
										AutomationAssignOrderToDispatcher(tempOrder, tempRowList[i], tempOrder.Id);
									}
								}
							}
						}

						Console.WriteLine(string.Format("End of file {0}", files[j]));
						reader.Dispose();
						MoveFile(files[j], reader);
						Console.WriteLine(" \r \n Hi  \r \n  Task Finish press any key to close. \r \n Thanks");
					}
				}
			}
			catch (Exception ex)
			{

			}
		}
	}
}