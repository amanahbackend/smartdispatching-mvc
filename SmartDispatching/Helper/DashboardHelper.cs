﻿using SmartDispatching.DB.DataBase;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartDispatching.Helper
{
	public static class DashboardHelper
	{
		private static Dispatching db = new Dispatching();

		public static JsonResult GetOrdersAccumlated(List<int> OrderIDs, bool isTrip)
		{

			if (OrderIDs != null && OrderIDs.Count > 0)
			{
				var outOrders = db.Orders.Where(x => OrderIDs.Contains(x.Id)).ToList();
				List<PrintViewModel> toReport = new List<PrintViewModel>();

				for (int i = 0; i < outOrders.Count; i++)
				{
					var orderId = outOrders[i].Id;
					var assigner = db.AssignedOrders.Where(x => x.OrderId == orderId).FirstOrDefault();
					var dispatcherName = "";
					if (assigner.DispatcherId != null)
					{
						var tempdisp = db.AspNetUsers.Where(x => x.Id == assigner.DispatcherId).FirstOrDefault();
						if (tempdisp != null)
						{
							dispatcherName = tempdisp.UserName;
						}
					}
					var superName = "";
					var tempSuper = db.AspNetUsers.Where(x => x.Id == assigner.SupervisorId).FirstOrDefault();
					if (tempSuper != null)
					{
						superName = tempSuper.UserName;
					}

					var technicianName = "";
					if (assigner.TechnicianId != null)
					{
						if (assigner.Technician != null)
						{
							technicianName = assigner.Technician.Name;
						}
					}
					var orderOutTemp = new PrintViewModel()
					{
						OrderNo = outOrders[i].OrderNo,
						DispatcherName = dispatcherName,
						TripTech = technicianName,
						SuperName = superName
					};
					if (outOrders[i].Address != null && outOrders[i].Address.PACI != null)
					{
						orderOutTemp.PACI = outOrders[i].Address.PACI;
					}
					if (outOrders[i].CreatedDate != null)
					{
						orderOutTemp.OrderCreationDate = outOrders[i].CreatedDate.ToString();
					}

					if (outOrders[i].DivisionId != null)
					{
						if (outOrders[i].Division != null)
						{
							orderOutTemp.DivisionName = outOrders[i].Division.Name;
						}
					}
					if (outOrders[i].CompanyCodeId != null)
					{
						if (outOrders[i].CompanyCode != null)
						{
							orderOutTemp.CompanyCode = outOrders[i].CompanyCode.Name;
						}
					}

					if (outOrders[i].Notes_ICAgent != null)
					{
						orderOutTemp.ICNote = outOrders[i].Notes_ICAgent;
					}

					if (outOrders[i].Address != null)
					{
						if (outOrders[i].Address.Area != null)
						{
							orderOutTemp.Area = outOrders[i].Address.Area.Name;
						}
					}
					if (outOrders[i].Problem != null)
					{
						if (outOrders[i].Problem != null)
						{
							orderOutTemp.Problem = outOrders[i].Problem.Name;
						}
					}
					if (outOrders[i].OrderStatu != null)
					{
						if (outOrders[i].OrderStatu != null)
						{
							orderOutTemp.Status = outOrders[i].OrderStatu.Name;
						}
					}
					if (outOrders[i].OrderType != null)
					{
						if (outOrders[i].OrderType != null)
						{
							orderOutTemp.OrderType = outOrders[i].OrderType.Name;
						}
					}
					if (outOrders[i].Customer != null)
					{
						orderOutTemp.CustomerName = outOrders[i].Customer.Name;
						orderOutTemp.CustomerTel = outOrders[i].Customer.PhoneOne;
					}
					//if (outOrders[i].Address.Area != null)
					//{
					//	orderOutTemp.Area = outOrders[i].Address.Area.Name;
					//}
					if (outOrders[i].Address != null)
					{
						if (outOrders[i].Address.Governorate != null)
						{
							orderOutTemp.Gov = outOrders[i].Address.Governorate.Name;
						}
					}
					if (outOrders[i].Address != null)
					{
						if (outOrders[i].Address.BlockId != null)
						{
							orderOutTemp.block = outOrders[i].Address.BlockId.ToString();
						}
					}
					if (outOrders[i].Address != null)
					{
						if (outOrders[i].Address.StreetId != null)
						{
							var id = outOrders[i].Address.StreetId;
							var tempStreet = db.Street_Jaddah.Where(x => x.Id == id).FirstOrDefault();
							if (tempStreet != null)
							{
								orderOutTemp.Street = tempStreet.Name;
							}
						}
					}
					if (outOrders[i].Customer != null && outOrders[i].Customer.Contracts != null)
					{
						if (outOrders[i].Customer.Contracts.Count > 0)
						{
							var contract = outOrders[i].Customer.Contracts.FirstOrDefault();
							orderOutTemp.ContractNo = contract.ContractNo;
							orderOutTemp.ContractType = contract.ContractType.Name;
							if (contract.ExpirationDate != null)
							{
								orderOutTemp.ExpirationDateString = Convert.ToDateTime(contract.ExpirationDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							}
							if (contract.CreatedDate != null)
							{
								orderOutTemp.CreatedDateString = Convert.ToDateTime(contract.CreatedDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							}
						}
					}
					if (isTrip == true)
					{
						if (outOrders[i].OrderWorkUpdates != null && outOrders[i].OrderWorkUpdates.Count > 0)
						{
							var workupdates = outOrders[i].OrderWorkUpdates.ToList();
							List<PrintViewModel> test = new List<PrintViewModel>();
							for (int j = 0; j < workupdates.Count; j++)
							{
								var WU = workupdates[j];

								PrintViewModel temp = new PrintViewModel();
								temp.DivisionName = outOrders[i].Division.Name;
								temp.CompanyCode = outOrders[i].CompanyCode.Name;
								temp.OrderNo = orderOutTemp.OrderNo;
								temp.OrderCreationDate = orderOutTemp.OrderCreationDate;
								temp.OrderType = orderOutTemp.OrderType;
								temp.Problem = orderOutTemp.Problem;
								temp.Status = orderOutTemp.Status;
								temp.CustomerName = orderOutTemp.CustomerName;
								temp.CustomerTel = orderOutTemp.CustomerTel;
								temp.PACI = orderOutTemp.PACI;
								temp.Area = orderOutTemp.Area;
								temp.TripNo = orderOutTemp.TripNo;
								temp.DispatcherName = orderOutTemp.DispatcherName;
								temp.SuperName = orderOutTemp.SuperName;
								temp.TripNo = orderOutTemp.TripNo;
								temp.Gov = orderOutTemp.Gov;
								temp.block = orderOutTemp.block;
								temp.Street = orderOutTemp.Street;
								temp.TripDisp = WU.DispatcherName;
								temp.TripTech = WU.TechnicianName;
								temp.TripNote = WU.Notes;
								temp.TripNo = WU.TripNo;
								temp.TripStatusName = WU.StatusName;
								temp.TripDate = WU.CreatedDate.ToString();
								temp.TripUpdateName = WU.WorkUpdateName;
								temp.ContractNo = orderOutTemp.ContractNo;
								temp.ContractType = orderOutTemp.ContractType;
								temp.CreatedDateString = orderOutTemp.CreatedDateString;
								temp.ExpirationDateString = orderOutTemp.ExpirationDateString;
								test.Add(temp);
							}
							toReport.AddRange(test);
						}
						else
						{
							toReport.Add(orderOutTemp);
						}
					}
					else
					{
						if (outOrders[i].OrderWorkUpdates != null && outOrders[i].OrderWorkUpdates.Count > 0)
						{
							var workUpdate = outOrders[i].OrderWorkUpdates.LastOrDefault();
							orderOutTemp.TripDisp = workUpdate.DispatcherName;
							orderOutTemp.TripTech = workUpdate.TechnicianName;
							orderOutTemp.TripNote = workUpdate.Notes;
							orderOutTemp.TripStatusName = workUpdate.StatusName;
							orderOutTemp.TripDate = workUpdate.CreatedDate.ToString();
							orderOutTemp.TripNo = workUpdate.TripNo;
							orderOutTemp.TripUpdateName = workUpdate.WorkUpdateName;
							orderOutTemp.DivisionName = outOrders[i].Division.Name;
							orderOutTemp.CompanyCode = outOrders[i].CompanyCode.Name;
						}
						toReport.Add(orderOutTemp);
					}
				}
				return new JsonResult()
				{
					Data = toReport,
					MaxJsonLength = 999999999,
					JsonRequestBehavior = JsonRequestBehavior.AllowGet
				};
			}
			return null;
		}

		internal static void LogExceptions(HttpRequest request, Exception exception,ApplicationUser user)
		{
			var erroFilePath = ConfigurationManager.AppSettings["ErrorFilePath"];

			if (!File.Exists(erroFilePath))
			{
				// Create a file to write to.
				string createText = Environment.NewLine;
				createText += "Date: " + DateTime.Now + Environment.NewLine;
				if (user != null)
				{
					createText += "UserName: " + user.UserName + Environment.NewLine;
				}
				createText += "URL String: " + request.Url.OriginalString + Environment.NewLine;
				createText += "Is Mobile: " + request.Browser.IsMobileDevice + Environment.NewLine;
				if (exception != null)
				{
					createText += "Target Site: " + exception.TargetSite + Environment.NewLine;
					createText += "Source: " + exception.Source + Environment.NewLine;
					createText += "Message: " + exception.Message + Environment.NewLine;
					createText += "StackTrace: " + exception.StackTrace + Environment.NewLine;
				}
				File.WriteAllText(erroFilePath, createText);
			}
			else
			{
				// This text is always added, making the file longer over time
				// if it is not deleted.
				string createText = Environment.NewLine;
				createText += "Date: " + DateTime.Now + Environment.NewLine;
				if (user != null)
				{
					createText += "UserName: " + user.UserName + Environment.NewLine;
				}
				createText += "URL String: " + request.Url.OriginalString + Environment.NewLine;
				createText += "Is Mobile: " + request.Browser.IsMobileDevice + Environment.NewLine;
				if (exception != null)
				{
					createText += "Target Site: " + exception.TargetSite + Environment.NewLine;
					createText += "Source: " + exception.Source + Environment.NewLine;
					createText += "Message: " + exception.Message + Environment.NewLine;
					createText += "StackTrace: " + exception.StackTrace + Environment.NewLine;
				}
				File.AppendAllText(erroFilePath, createText);
			}
		}

		public static void LogExceptions(HttpRequestBase request, Exception exception, ApplicationUser user)
		{
			var erroFilePath = ConfigurationManager.AppSettings["ErrorFilePath"];

			if (!File.Exists(erroFilePath))
			{
				// Create a file to write to.
				string createText = Environment.NewLine;
				createText += "Date: " + DateTime.Now + Environment.NewLine;
				createText += "UserName: " + user.UserName + Environment.NewLine;
				createText += "URL String: " + request.Url.OriginalString + Environment.NewLine;
				createText += "Is Mobile: " + request.Browser.IsMobileDevice + Environment.NewLine;
				createText += "Target Site: " + exception.TargetSite + Environment.NewLine;
				createText += "Source: " + exception.Source + Environment.NewLine;
				createText += "Message: " + exception.Message + Environment.NewLine;
				createText += "StackTrace: " + exception.StackTrace + Environment.NewLine;
				File.WriteAllText(erroFilePath, createText);
			}
			else
			{
				// This text is always added, making the file longer over time
				// if it is not deleted.
				string createText = Environment.NewLine;
				createText += "Date: " + DateTime.Now + Environment.NewLine;
				createText += "UserName: " + user.UserName + Environment.NewLine;
				createText += "URL String: " + request.Url.OriginalString + Environment.NewLine;
				createText += "Is Mobile: " + request.Browser.IsMobileDevice + Environment.NewLine;
				createText += "Target Site: " + exception.TargetSite + Environment.NewLine;
				createText += "Source: " + exception.Source + Environment.NewLine;
				createText += "Message: " + exception.Message + Environment.NewLine;
				createText += "StackTrace: " + exception.StackTrace + Environment.NewLine;
				File.AppendAllText(erroFilePath, createText);
			}
		}

		public static void LogReader(string orderNo,string type)
		{
			try
			{
				var logFilePath = ConfigurationManager.AppSettings["LogFilePath"];

				if (!File.Exists(logFilePath))
				{
					string createText = type + "," + DateTime.Now + "," + orderNo + Environment.NewLine;
					File.WriteAllText(logFilePath, createText);
				}
				else
				{
					string createText = type + "," + DateTime.Now + "," + orderNo + Environment.NewLine;
					File.AppendAllText(logFilePath, createText);
				}
			}
			catch (Exception ex) { }
		}
	}
	public class Err : HandleErrorAttribute
	{
		public override void OnException(ExceptionContext filterContext)
		{
			Exception ex = filterContext.Exception;
			filterContext.ExceptionHandled = true;
			var model = new HandleErrorInfo(filterContext.Exception, "Error", "HttpError500");

			filterContext.Result = new ViewResult()
			{
				ViewName = "Error1",
				ViewData = new ViewDataDictionary(model)
			};
		}
	}
}