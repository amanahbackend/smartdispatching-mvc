﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SmartDispatching.Helper;
using SmartDispatching.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SmartDispatching
{
	public class MvcApplication : System.Web.HttpApplication
	{
		private static System.Threading.Timer timer;
		private static bool isTimerValid = true;

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			var readReadAfter = ConfigurationManager.AppSettings["AutoReadAfter"];
			var howLongTillTimerFirstGoesInMilliseconds = 1000;
			var intervalBetweenTimerEventsInMilliseconds = Convert.ToInt16(readReadAfter) * 60000;

			timer = new Timer(
				(s) => DoThis(),
				null, // if you need to provide state to the function specify it here
				howLongTillTimerFirstGoesInMilliseconds,
				intervalBetweenTimerEventsInMilliseconds
			);
		}
		private void DoThis()
		{
			if (isTimerValid == true)
			{
				isTimerValid = false;
				Tasker.CSVMainFunction();
				isTimerValid = true;
			}
		}

		protected void Application_End(object sender, EventArgs e)
		{
			if (timer != null)
				timer.Dispose();
		}

		protected void Application_Error(object sender, EventArgs e)
		{
			ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

			var exception = Server.GetLastError();
			var httpException = exception as HttpException;
			var request = Request;
			Response.Clear();
			Server.ClearError();
			Response.TrySkipIisCustomErrors = true;
			var routeData = new RouteData();
			routeData.Values["controller"] = "Error";
			routeData.Values["action"] = "HttpError500";
			routeData.Values["exception"] = exception;
			Response.StatusCode = 500;

			if (httpException != null)
			{
				Response.StatusCode = httpException.GetHttpCode();

				switch (Response.StatusCode)
				{
					case 500:
						routeData.Values["action"] = "HttpError500";
						break;
					case 404:
						routeData.Values["action"] = "HttpError404";
						break;
					default:
						routeData.Values["action"] = "HttpErrorGeneral";
						break;
				}
			}
			HttpContext.Current.Response.ContentType = "text/html";

			IController errorsController = new Controllers.ErrorController();
			var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
			DashboardHelper.LogExceptions(request, exception, user);
			errorsController.Execute(rc);
			Server.ClearError();
		}
	}
}