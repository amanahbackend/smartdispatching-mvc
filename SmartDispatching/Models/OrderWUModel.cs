﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class OrderWUModel
	{
		public int? TripNo { get; set; }
		public string WorkUpdateId { get; set; }
		public Nullable<System.DateTime> CreatedDate { get; set; }
		public string Notes { get; set; }
		public string TechnicianName { get; set; }
		public string DispatcherName { get; set; }
		public string Status { get; set; }
		public string Update { get; set; }
	}
}