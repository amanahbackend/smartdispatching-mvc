﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class OrderBoardViewModel
	{
		public int Id{ get; set; }
		public string OrderNo { get; set; }
		public string ActionName { get; set; }
		public string Area { get; set; }
		public string Governorate { get; set; }
		public string Street { get; set; }
		public string Block { get; set; }
		public string PACI { get; set; }
		public string Problem { get; set; }
		public string ProblemCode { get; set; }
		public int? ProblemId { get; set; }
		public int? StatusId { get; set; }
		public string Status { get; set; }
		public string CustomerName { get; set; }
		public string CustomerPhone { get; set; }
		public string CustomerNo { get; set; }
		public bool IsAssigned { get; set; }
		public int? TypeId { get; set; }
		public string Type { get; set; }
		public decimal Long { get; set; }
		public decimal Lat { get; set; }
		public DateTime? CreatedDate { get; set; }
		public string CreatedDateString { get; set; }
		public DateTime? DateFrom { get; set; }
		public DateTime? DateTo { get; set; }
	}
}