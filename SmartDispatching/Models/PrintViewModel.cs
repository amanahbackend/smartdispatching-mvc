﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class PrintViewModel
	{
		public int OrderId { get; set; }
		public string OrderNo { get; set; }
		public string OrderType { get; set; }
		public string Problem { get; set; }
		public string Status { get; set; }
		public DateTime CreationDate { get; set; }
		public string CustomerName { get; set; }
		public string CustomerTel { get; set; }
		public string AssigendTechnician { get; set; }
		// address details
		public string PACI { get; set; }
		public string Area { get; set; }
		public string Street { get; set; }
		public string Gov { get; set; }
		public string block { get; set; }
		public string Functional_Location { get; set; }
		public string House_Kasima { get; set; }
		public string Floor { get; set; }
		public string AppartmentNo { get; set; }
		public string AddressNote { get; set; }
		public string ContractCode { get; set; }
		public DateTime? ExpirationDate { get; set; }
		public  string ExpirationDateString { get; set; }
		public string CreatedDateString { get; set; }
		public string ContractType { get; set; }
		public string ContractNo { get; set; }
		public string CompanyCode { get; set; }
		public string DispatcherName { get; set; }
		public string SuperName { get; set; }
		public string FormanName { get; set; }
		public string StatusUpdate { get; set; }
		public string TripTech { get; set; }
		public string TripDisp { get; set; }
		public string TripNote { get; set; }
		public int? TripNo { get; set; }
		public string TripUpdateName { get; set; }
		public string TripStatusName { get; set; }
		public string TripDate { get; set; }
		public string DivisionName { get; set; }
		public string OrderCreationDate { get; set; }
		public string ICNote { get; set; }

	}

	public class PrintModel
	{
		public List<PrintViewModel> Orders { get; set; }
		public int?  TotalCount { get; set; }
	}
}