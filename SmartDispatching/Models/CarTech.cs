﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class CarTech
	{
		public string VID { get; set; }
		public string TechName { get; set; }
	}
}