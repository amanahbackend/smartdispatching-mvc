﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class PACIModel
	{
		public List<Feature> features { get; set; }
		public List<Field> fields { get; set; }
		public string geometryType { get; set; }
		public SpatialReference spatialReference { get; set; }
		public string objectIdFieldName { get; set; }
	}
	public class Feature
	{
		public Attribute attributes { get; set; }
		public Geometry geometry { get; set; }
	}
	public class Attribute
	{
		public string objectid { get; set; }
		public string blockarabic { get; set; }
		public string housearabic { get; set; }
		public string parcelarabic { get; set; }
		public string civilid { get; set; }
		public string streetenglish { get; set; }
		public string streetarabic { get; set; }
		public string parcelenglish { get; set; }
		public string houseenglish { get; set; }
		public string neighborhoodarabic { get; set; }
		public string neighborhoodenglish { get; set; }
		public string governoratearabic { get; set; }
		public string governorateenglish { get; set; }
		public string location { get; set; }
		public string blockenglish { get; set; }
		public string unit_no { get; set; }
		public string floor_no { get; set; }
		public decimal? lat { get; set; }
		public decimal? lon { get; set; }
		public string buildingnamearabic { get; set; }
		public string buildingnameenglish { get; set; }
		public string buildingtypearabic { get; set; }
		public string buildingktypeenglish { get; set; }
		public string neighborhoodid { get; set; }
		public string governorateid { get; set; }
		public decimal? ktm_x { get; set; }
		public decimal? ktm_y { get; set; }
		public decimal? utm_x { get; set; }
		public decimal? utm_y { get; set; }
		public decimal? wsphere_x { get; set; }
		public decimal? wsphere_y { get; set; }
	}
	public class Geometry
	{
		public decimal? x { get; set; }
		public decimal? y { get; set; }
	}
	public class Field
	{
		public string name { get; set; }
		public string type { get; set; }
		public string alias { get; set; }
		public string length { get; set; }
	}
	public class SpatialReference
	{
		public string wkid { get; set; }
		public string latestWkid { get; set; }
	}

	// Area
	public class AreaPACIResultModel
	{
		public List<AreaFeature> features { get; set; }
		public List<Field> fields { get; set; }
		public string geometryType { get; set; }
		public SpatialReference spatialReference { get; set; }
		public string objectIdFieldName { get; set; }
	}
	public class AreaFeature
	{
		public AreaAttribute attributes { get; set; }
		public AreaGeometty geometry { get; set; }
	}
	public class AreaAttribute
	{
		public int OBJECTID { get; set; }
		public int Gov_no { get; set; }
		public int Nhood_No { get; set; }
		public string GovernorateArabic { get; set; }
		public string GovernorateEnglish { get; set; }
		public string NeighborhoodArabic { get; set; }
		public string NeighborhoodEnglish { get; set; }
		public decimal? CENTROID_X { get; set; }
		public decimal? CENTROID_Y { get; set; }
	}
	public class AreaGeometty
	{
		public List<List<double>> rings { get; set; }
	}

	// block

	public class BlockAttribute
	{
		public int OBJECTID { get; set; }
		public int Gov_no { get; set; }
		public int Nhood_No { get; set; }
		public int BlockID { get; set; }
		public string GovernorateArabic { get; set; }
		public string GovernorateEnglish { get; set; }
		public string NeighborhoodArabic { get; set; }
		public string NeighborhoodEnglish { get; set; }
		public string BlockArabic { get; set; }
		public string BlockEnglish { get; set; }
		public decimal? CENTROID_X { get; set; }
		public decimal? CENTROID_Y { get; set; }
	}
	public class BlockGeometty
	{
		public List<List<double>> rings { get; set; }
	}
	public class BlockFeature
	{
		public BlockAttribute attributes { get; set; }
		public BlockGeometty geometry { get; set; }
	}
	public class BlockPACIResultModel
	{
		public List<BlockFeature> features { get; set; }
		public List<Field> fields { get; set; }
		public string geometryType { get; set; }
		public SpatialReference spatialReference { get; set; }
		public string objectIdFieldName { get; set; }
	}

	// Gov
	public class GovAttribute
	{
		public int OBJECTID { get; set; }
		public int Gov_no { get; set; }
		public string GovernorateArabic { get; set; }
		public string GovernorateEnglish { get; set; }
		public decimal? CENTROID_X { get; set; }
		public decimal? CENTROID_Y { get; set; }
	}
	public class GovGeometty
	{
		public List<List<double>> rings { get; set; }
	}
	public class GovFeature
	{
		public GovAttribute attributes { get; set; }
		public GovGeometty geometry { get; set; }
	}
	public class GovPACIResultModel
	{
		public List<GovFeature> features { get; set; }
		public List<Field> fields { get; set; }
		public string geometryType { get; set; }
		public SpatialReference spatialReference { get; set; }
		public string objectIdFieldName { get; set; }
	}

	public class StreetAttribute
	{
		public int OBJECTID { get; set; }
		public string GovernorateArabic { get; set; }
		public string GovernorateEnglish { get; set; }
		public string NeighborhoodArabic { get; set; }
		public string NeighborhoodEnglish { get; set; }
		public string BlockArabic { get; set; }
		public string BlockEnglish { get; set; }
		public string StreetArabic { get; set; }
		public string StreetEnglish { get; set; }
		public int? StreetNumber { get; set; }
		public string AlternativeStreetArabic1 { get; set; }
		public string AlternativeStreetEnglish1 { get; set; }
		public string AlternativeStreetArabic2 { get; set; }
		public string AlternativeStreetEnglish2 { get; set; }
		public string AlternativeStreetArabic3 { get; set; }
		public string AlternativeStreetEnglish3 { get; set; }
		public string AlternativeStreetArabic4 { get; set; }
		public string AlternativeStreetEnglish4 { get; set; }
		public string TitleArabic { get; set; }
		public string TitleEnglish { get; set; }
		public string DetailsArabic { get; set; }
		public string DetailsEnglish { get; set; }
		public string Location { get; set; }
		public decimal? CENTROID_X { get; set; }
		public decimal? CENTROID_Y { get; set; }
		public int Nhood_No { get; set; }
		public int Gov_no { get; set; }
	}
	public class FieldAliases
	{
		public string OBJECTID { get; set; }
		public string Gov_no { get; set; }
		public string Nhood_No { get; set; }
		public string StreetNumber { get; set; }
		public string GovernorateArabic { get; set; }
		public string GovernorateEnglish { get; set; }
		public string NeighborhoodArabic { get; set; }
		public string NeighborhoodEnglish { get; set; }
		public string BlockArabic { get; set; }
		public string BlockEnglish { get; set; }
		public string StreetArabic { get; set; }
		public string StreetEnglish { get; set; }
		public string AlternativeStreetArabic1 { get; set; }
		public string AlternativeStreetEnglish1 { get; set; }
		public string AlternativeStreetArabic2 { get; set; }
		public string AlternativeStreetEnglish2 { get; set; }
		public string AlternativeStreetArabic3 { get; set; }
		public string AlternativeStreetEnglish3 { get; set; }
		public string AlternativeStreetArabic4 { get; set; }
		public string AlternativeStreetEnglish4 { get; set; }
		public string TitleArabic { get; set; }
		public string TitleEnglish { get; set; }
		public string DetailsArabic { get; set; }
		public string DetailsEnglish { get; set; }
		public string CENTROID_X { get; set; }
		public string CENTROID_Y { get; set; }
		public string Location { get; set; }
	}
	public class StreetGeometty
	{
		public List<List<double>> paths { get; set; }
	}
	public class StreetFeature
	{
		public StreetAttribute attributes { get; set; }
		//public StreetGeometty geometry { get; set; }
	}
	public class StreetPACIResultModel
	{
		public string displayFieldName { get; set; }
		public FieldAliases fieldAliases { get; set; }
		public List<Field> fields { get; set; }
		public List<StreetFeature> features { get; set; }
	}

	public class VehicleModel
	{
		public VehicleResponse response { get; set; }
		public string objname { get; set; }
		public VehicleStatus status { get; set; }
	}
	public class VehicleResponse
	{
		public List<VehicleObject> vehiclesreg { get; set; }
	}
	public class VehicleObject
	{
		public string VID { get; set; }
		public string Reg { get; set; }
		public string Vtype { get; set; }
	}
	public class VehicleStatus
	{
		public string statuscode { get; set; }
		public string statusdescription { get; set; }
		public string info1 { get; set; }
		public string info2 { get; set; }
	}
}