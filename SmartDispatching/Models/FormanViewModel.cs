﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class FormanViewModel
	{
		[Required]
		public string Name { get; set; }
		[Required]
		[Display(Name = "Dispatcher")]
		public string DispatcherId { get; set; }
		[Display(Name = "Supervisor")]
		public string SuperDispatcherId { get; set; }
		[Required]
		[Display(Name = "Phone No.")]
		public string PhoneNo { get; set; }
		public int Id { get; set; }
	}
}