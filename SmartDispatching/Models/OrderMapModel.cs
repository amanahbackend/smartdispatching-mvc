﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class OrderMapModel
	{
		public string PACI { get; set; }
		public string Area { get; set; }
		public string Problem { get; set; }
		public string ProblemCode { get; set; }
		public string Status { get; set; }
		public string OrderNo { get; set; }
		public string OrderType { get; set; }
		public string CreatedDate { get; set; }
		public decimal? Long { get; set; }
		public decimal? Lat { get; set; }

	}
}