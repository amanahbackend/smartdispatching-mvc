﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class PrintTechModel
	{
		public int formanId{ get; set; }
		public List<int> techIds { get; set; }
	}
}