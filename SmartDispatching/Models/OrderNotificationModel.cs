﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class OrderNotificationModel
	{
			public int Id { get; set; }
			public string PACI { get; set; }
			public string Area { get; set; }
			public string Problem { get; set; }
			public string Status { get; set; }
			public string OrderNo { get; set; }
			public string CustomerName { get; set; }
			public string CustomerPhone { get; set; }
			public string CreatedDate { get; set; }
			public decimal? Long { get; set; }
			public decimal? Lat { get; set; }
	}
}