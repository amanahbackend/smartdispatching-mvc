﻿using SmartDispatching.DB.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class OrderViewModel
	{
		// Address
		[Display(Name = "PACI")]
		public string PACI { get; set; }
		[Display(Name = "Area")]
		public int AreaId { get; set; }
		public int? AreaIdFile { get; set; }
		public string AreaDescription { get; set; }
		[Display(Name = "Customer")]
		public int CustomerId { get; set; }
		[Display(Name = "Street")]
		public Nullable<int> StreetId { get; set; }
		public string StreetName { get; set; }
		[Display(Name = "Governorate")]
		public Nullable<int> GovId { get; set; }
		public string  GovName { get; set; }
		[Display(Name = "Block")]
		public Nullable<int> BlockId { get; set; }
		public string BlockName { get; set; }
		[Display(Name = "Functional location")]
		public string Functional_Location { get; set; }
		[Display(Name = "Building")]
		public string House_Kasima { get; set; }
		[Display(Name = "Floor No.")]
		public string Floor { get; set; }
		[Display(Name = "Appartment No.")]
		public string AppartmentNo { get; set; }
		[Display(Name = "Address note")]
		public string AddressNote { get; set; }

		public virtual Area Area { get; set; }
		public virtual Block Block { get; set; }
		public virtual Governorate Governorate { get; set; }
		public virtual Street_Jaddah Street_Jaddah { get; set; }

		//Customer
		[Display(Name = "Customer No.")]
		public string CustomerNo { get; set; }
		[Display(Name = "Customer name")]
		public string Name { get; set; }
		[Display(Name = "Customer Tel. 1")]
		public string PhoneOne { get; set; }
		[Display(Name = "Customer Tel. 2")]
		public string PhoneTwo { get; set; }

		// Contract
		[Display(Name = "Contract No.")]
		public string ContractNo { get; set; }
		[Display(Name = "Contract type")]
		public int? ContractTypeId { get; set; }
		public string ContractTypeName { get; set; }
		public string ContractTypeCode { get; set; }
		[Display(Name = "Created date")]
		public DateTime CreatedDate { get; set; }
		public string CreatedDateString { get; set; }
		[Display(Name = "Created date")]
		public string StringCreatedDate { get; set; }
		[Display(Name = "Expiration date")]
		public DateTime? ExpirationDate { get; set; }
		public string ExpirationDateString { get; set; }

		public virtual ContractType ContractType { get; set; }

		// Order Info
		public int Id { get; set; }
		[Display(Name = "Order No.")]
		public string OrderNo { get; set; }
		[Display(Name = "Order type")]
		public int OrderTypeId { get; set; }
		[Display(Name = "Order type")]
		public int EditOrderTypeId { get; set; }
		public string OrderTypeName { get; set; }
		public string OrderTypeCode { get; set; }

		[Display(Name = "Company code")]
		public int? CompanyCodeId { get; set; }
		public string CompanyCodeName { get; set; }
		[Display(Name = "Division")]
		public int? DivisionId { get; set; }
		public int? DivisionCode { get; set; }
		public int? AppartmentCode { get; set; }
		public string DivisionName { get; set; }
		[Display(Name = "Priority")]
		public Nullable<int> PriorityId { get; set; }
		public string OrderPriorityDescription { get; set; }
		[Display(Name = "Status")]
		public int? StatusId { get; set; }
		public string OrderStatusDescription { get; set; }
		[Display(Name = "Problem")]
		public int? ProblemId { get; set; }
		public string ProblemCode { get; set; }
		public string ProblemName { get; set; }
		public string ProblemDescription { get; set; }
		[Display(Name = "IC Agent notes")]
		public string Notes_ICAgent { get; set; }
		[Display(Name = "Dispatcher notes")]
		public string Notes_Dispatcher { get; set; }
		[Display(Name = "Cancellation reason")]
		public string Cancellation_Reason { get; set; }
		[Display(Name = "Order date")]
		public DateTime? OrderDate { get; set; }
		public string OrderDateString { get; set; }

		[Display(Name = "SAP PACI")]
		public string SAPPACI { get; set; }
		[Display(Name = "SAP Governorate")]
		public int? SAPGov { get; set; }
		[Display(Name = "SAP Area")]
		public string SAPArea { get; set; }
		[Display(Name = "SAP Block")]
		public string SAPBlock { get; set; }
		[Display(Name = "SAP Street")]
		public string SAPStreet { get; set; }
		[Display(Name = "SAP Building")]
		public string SAPBuilding { get; set; }
		[Display(Name = "SAP Floor")]
		public string SAPFloor { get; set; }
		[Display(Name = "SAP Appartment")]
		public int? SAPAppartment { get; set; }

		public decimal? Long { get; set; }
		public decimal? Lat { get; set; }

		public virtual CompanyCode CompanyCode { get; set; }
		public virtual Division Division { get; set; }
		public virtual OrderPriority OrderPriority { get; set; }
		public virtual OrderStatu OrderStatu { get; set; }
		public virtual OrderType OrderType { get; set; }
		public virtual Problem Problem { get; set; }
	}
}