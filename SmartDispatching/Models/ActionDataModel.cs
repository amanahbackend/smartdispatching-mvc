﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class ActionDataModel
	{
		public string ActionName { get; set; }
		public decimal Longitude { get; set; }
		public decimal Latitude { get; set; }
		public string ActionDate { get; set; }
	}
}