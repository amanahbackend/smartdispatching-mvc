﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class TechnicianOrderModel
	{
		public string Name { get; set; }
		public string OrderNo { get; set; }
		public string OrderProblem { get; set; }
		public string OrderStatus { get; set; }
		public decimal Lat { get; set; }
		public decimal Long { get; set; }
	}
}