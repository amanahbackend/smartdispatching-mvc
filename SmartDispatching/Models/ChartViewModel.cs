﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class ChartViewModel
	{
		public string OrderNo { get; set; }
		public string Area { get; set; }
		public string TechnicianName { get; set; }
		public string DivisionName { get; set; }
		public string CreationDate { get; set; }
		public string OrderType { get; set; }
	}
}