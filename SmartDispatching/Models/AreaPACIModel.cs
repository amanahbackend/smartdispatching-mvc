﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class AreaPACIModel
	{
		public string displayFieldName { get; set; }
		public fieldAliases fieldAliases { get; set; }
		public List<AreaField> fields { get; set; }
		public List<AreaAttributes> features { get; set; }

	}
	public class fieldAliases
	{
		public string Nhood_No { get; set; }
		public string NeighborhoodEnglish { get; set; }
	}
	public class AreaField
	{
		public string name { get; set; }
		public string type { get; set; }
		public string alias { get; set; }
		public int? length { get; set; }

	}
	public class AreaAttributes
	{
		public attribute attribute { get; set; }
	}

	public class attribute
	{
		public string NeighborhoodEnglish { get; set; }
		public int? Nhood_No { get; set; }
	}

}