﻿using SmartDispatching.DB.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class PrintOutViewModel
	{
		[Key]
		public int Id { get; set; }
		public List<OrderStatu> Statuses { get; set; }
		public List<Forman> Formans { get; set; }
		public List<Technician> Technicians { get; set; }
	}
}