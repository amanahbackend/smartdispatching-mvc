﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SmartDispatching.DB.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SmartDispatching.Models
{
	class CustomResolver : DefaultContractResolver
	{
		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			JsonProperty prop = base.CreateProperty(member, memberSerialization);

			if (prop.DeclaringType != typeof(OrderWorkUpdate) &&
				prop.PropertyType.IsClass &&
				prop.PropertyType != typeof(string))
			{
				prop.ShouldSerialize = obj => false;
			}

			return prop;
		}
	}
	class CustomResolver2 : DefaultContractResolver
	{
		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			JsonProperty prop = base.CreateProperty(member, memberSerialization);

			if (prop.DeclaringType != typeof(AssignedOrder) &&
				prop.PropertyType.IsClass &&
				prop.PropertyType != typeof(string))
			{
				prop.ShouldSerialize = obj => false;
			}

			return prop;
		}
	}
}