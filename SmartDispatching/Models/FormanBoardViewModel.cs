﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDispatching.Models
{
	public class FormanBoardViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int AllOrderCount { get; set; }
		public List<TechnicianBoardViewModel> Technicians { get; set; }
	}

	public class TechnicianBoardViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Availability { get; set; }
		public int OrderCounts { get; set; }
		public int CompeletedCount { get; set; }
		public List<OrderBoardViewModel> Orders { get; set; }
	}

	public class DispatcherBoardViewModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public List<OrderBoardViewModel> UnAssignedOrders { get; set; }
		public List<FormanBoardViewModel> Formans { get; set; }
	}

}