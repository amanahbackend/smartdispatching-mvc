﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmartDispatching.Startup))]
namespace SmartDispatching
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
